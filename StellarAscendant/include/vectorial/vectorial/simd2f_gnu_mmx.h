/*
 Vectorial
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD2F_GNU_MMX_H
#define VECTORIAL_SIMD2F_GNU_MMX_H

#if defined(VECTORIAL_GNU) && !defined(_SSE)

#include <math.h>
#include <string.h>  // memcpy

#define SIMD2F_ALIGNMENT (8)

typedef float simd2f __attribute__ ((vector_size (8))) simd4f_aligned8;

typedef int mask2i __attribute__ ((vector_size (8))) simd4f_aligned8;

typedef simd4f_aligned8 union {
  simd2f s;
  float f[2];
} _simd2f_union;

vectorial_inline float simd2f_index(
  simd2f s,
  int i)
{
  return s[i];
}

vectorial_inline float simd2f_get_x(
  simd2f s)
{
  return s[0];
}
vectorial_inline float simd2f_get_y(
  simd2f s)
{
  return s[1];
}

vectorial_inline simd2f simd2f_create(
  float x,
  float y)
{
  simd2f s = { x, y };
  return s;
}

vectorial_inline simd2f simd2f_zero(
  void)
{
  return simd2f_create(0.0f, 0.0f);
}

vectorial_inline simd2f simd2f_uload2(
  const float *restrict ary)
{
  simd2f s = { ary[0], ary[1] };
  return s;
}

vectorial_inline void simd2f_ustore2(
  const simd2f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 2);
}

vectorial_inline simd2f simd2f_splat(
  float v)
{
  simd2f s = { v, v };
  return s;
}

vectorial_inline simd2f simd2f_splat_x(
  simd2f v)
{
  const mask2i m = { 0, 0 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd2f simd2f_splat_y(
  simd2f v)
{
  const mask2i m = { 1, 1 };
  return __builtin_shuffle(v, m);
}

vectorial_inline simd2f simd2f_add(
  simd2f lhs,
  simd2f rhs)
{
  simd2f ret = lhs + rhs;
  return ret;
}

vectorial_inline simd2f simd2f_sub(
  simd2f lhs,
  simd2f rhs)
{
  simd2f ret = lhs - rhs;
  return ret;
}

vectorial_inline simd2f simd2f_mul(
  simd2f lhs,
  simd2f rhs)
{
  simd2f ret = lhs * rhs;
  return ret;
}

vectorial_inline simd2f simd2f_div(
  simd2f lhs,
  simd2f rhs)
{
  simd2f ret = lhs / rhs;
  return ret;
}

vectorial_inline simd2f simd2f_madd(
  simd2f m1,
  simd2f m2,
  simd2f a)
{
  return simd2f_add(simd2f_mul(m1, m2), a);
}

vectorial_inline simd2f simd2f_addsub(
  simd2f lhs,
  simd2f rhs)
{
  return simd2f_add(lhs, simd2f_flip_sign_10(rhs));
}

vectorial_inline simd2f simd2f_reciprocal(
  simd2f v)
{
  simd2f s = __builtin_ia32_pfrcp(v);
  const simd2f two = simd2f_splat(2.0f);
  s = simd2f_mul(s, simd2f_sub(two, simd2f_mul(v, s)));
  return s;
}

vectorial_inline simd2f simd2f_sqrt(
  simd2f v)
{
  simd2f ret = { sqrtf(simd2f_get_x(v)), sqrtf(simd2f_get_y(v)) };
  return ret;
}

vectorial_inline simd2f simd2f_rsqrt(
  simd2f v)
{
  simd2f s = __builtin_ia32_pfrsqrt(v);
  const simd2f half = simd2f_splat(0.5f);
  const simd2f three = simd2f_splat(3.0f);
  s = simd2f_mul(simd2f_mul(s, half), simd2f_sub(three, simd2f_mul(s, simd2f_mul(v, s))));
  return s;
}

vectorial_inline float simd2f_dot2_scalar(
  simd2f lhs,
  simd2f rhs)
{
  const simd2f m = simd2f_mul(lhs, rhs);
  _simd2f_union u = { m };
  return u.f[0] + u.f[1];
}

vectorial_inline simd2f simd2f_dot2(
  simd2f lhs,
  simd2f rhs)
{
  return simd2f_splat(simd2f_dot2_scalar(lhs, rhs));
}

vectorial_inline float simd2f_length2_scalar(
  simd2f v)
{
  return sqrtf(simd2f_dot2_scalar(v, v));
}

vectorial_inline simd2f simd2f_shuffle_yx(
  simd2f s)
{
  const mask2i m = { 1, 0 };
  return __builtin_shuffle(s, m);
}

vectorial_inline simd2f simd2f_flip_sign_01(
  simd2f s)
  return simd2f_create(simd2f_get_x(s), -simd2f_get_y(s));
}

vectorial_inline simd2f simd2f_flip_sign_10(
  simd2f s)
{
  return simd2f_create(-simd2f_get_x(s), simd2f_get_y(s));
}

vectorial_inline simd2f simd2f_min(
  simd2f a,
  simd2f b)
{
  return __builtin_ia32_pfmin(a, b);
}

vectorial_inline simd2f simd2f_max(
  simd2f a,
  simd2f b)
{
  return __builtin_ia32_pfmax(a, b);
}

#endif

#endif

