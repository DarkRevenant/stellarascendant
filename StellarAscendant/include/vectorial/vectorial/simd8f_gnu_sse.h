/*
 Vectorial
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD8F_GNU_SSE_H
#define VECTORIAL_SIMD8F_GNU_SSE_H

#if defined(VECTORIAL_GNU) && !defined(_AVX)

/* This implementation assumes that simd4f_gnu.h is included; it may not work correctly otherwise */

#include <math.h>
#include <string.h>  // memcpy

#define SIMD8F_ALIGNMENT (16)

typedef simd4f_aligned16 struct {
  simd4f s[2];
} simd8f;

typedef simd4f_aligned16 union {
  simd8f s;
  float f[8];
} _simd8f_union;

typedef simd4f_aligned16 union {
  unsigned int ui[8];
  float f[8];
} _simd8f_uif;

vectorial_inline simd4f simd8f_index4(
  simd8f s,
  int i)
{
  _simd8f_union u = { s };
  return simd4f_uload4(&(u.f[i]));
}
vectorial_inline simd2f simd8f_index2(
  simd8f s,
  int i)
{
  _simd8f_union u = { s };
  return simd2f_uload2(&(u.f[i]));
}
vectorial_inline float simd8f_index(
  simd8f s,
  int i)
{
  _simd8f_union u = { s };
  return u.f[i];
}

vectorial_inline float simd8f_get_x(
  simd8f s)
{
  return s.s[0][0];
}
vectorial_inline float simd8f_get_y(
  simd8f s)
{
  return s.s[0][1];
}
vectorial_inline float simd8f_get_z(
  simd8f s)
{
  return s.s[0][2];
}
vectorial_inline float simd8f_get_w(
  simd8f s)
{
  return s.s[0][3];
}
vectorial_inline float simd8f_get_a(
  simd8f s)
{
  return s.s[1][0];
}
vectorial_inline float simd8f_get_b(
  simd8f s)
{
  return s.s[1][1];
}
vectorial_inline float simd8f_get_c(
  simd8f s)
{
  return s.s[1][2];
}
vectorial_inline float simd8f_get_d(
  simd8f s)
{
  return s.s[1][3];
}

vectorial_inline simd8f simd8f_create(
  float x,
  float y,
  float z,
  float w,
  float a,
  float b,
  float c,
  float d)
{
  simd8f s = { { { x, y, z, w }, { a, b, c, d } } };
  return s;
}

vectorial_inline simd8f simd8f_zero(
  void)
{
  return simd8f_create(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
}

vectorial_inline simd8f simd8f_uload8(
  const float *restrict ary)
{
  simd8f s;
  s.s[0] = simd4f_uload4(&(ary[0]));
  s.s[1] = simd4f_uload4(&(ary[4]));
  return s;
}
vectorial_inline simd8f simd8f_uload6(
  const float *restrict ary)
{
  simd8f s;
  s.s[0] = simd4f_uload4(&(ary[0]));
  s.s[1] = simd4f_uload2(&(ary[4]));
  return s;
}
vectorial_inline simd8f simd8f_uload4(
  const float *restrict ary)
{
  simd8f s;
  s.s[0] = simd4f_uload4(&(ary[0]));
  s.s[1] = simd4f_zero();
  return s;
}
vectorial_inline simd8f simd8f_uload3x2(
  const float *restrict ary)
{
  simd8f s;
  s.s[0] = simd4f_uload3(&(ary[0]));
  s.s[1] = simd4f_uload3(&(ary[3]));
  return s;
}
vectorial_inline simd8f simd8f_uload3(
  const float *restrict ary)
{
  simd8f s;
  s.s[0] = simd4f_uload3(&(ary[0]));
  s.s[1] = simd4f_zero();
  return s;
}
vectorial_inline simd8f simd8f_uload2(
  const float *restrict ary)
{
  simd8f s;
  s.s[0] = simd4f_uload2(&(ary[0]));
  s.s[1] = simd4f_zero();
  return s;
}

vectorial_inline void simd8f_ustore8(
  const simd8f val,
  float *restrict ary)
{
  simd4f_ustore4(val.s[0], ary);
  simd4f_ustore4(val.s[1], ary);
}
vectorial_inline void simd8f_ustore6(
  const simd8f val,
  float *restrict ary)
{
  simd4f_ustore4(val.s[0], ary);
  memcpy(ary + 3, &(val.s[1]), sizeof(float) * 2);
}
vectorial_inline void simd8f_ustore4(
  const simd8f val,
  float *restrict ary)
{
  simd4f_ustore4(val.s[0], ary);
}
vectorial_inline void simd8f_ustore3x2(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &(val.s[0]), sizeof(float) * 3);
  memcpy(ary + 4, &(val.s[1]), sizeof(float) * 3);
}
vectorial_inline void simd8f_ustore3(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &(val.s[0]), sizeof(float) * 3);
}
vectorial_inline void simd8f_ustore2(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &(val.s[0]), sizeof(float) * 2);
}

vectorial_inline simd8f simd8f_splat(
  float v)
{
  simd8f s = { { { v, v, v, v }, { v, v, v, v } } };
  return s;
}
vectorial_inline simd8f simd8f_splat_4f(
  float x,
  float y,
  float z,
  float w)
{
  simd8f s = { { { x, y, z, w }, { x, y, z, w } } };
  return s;
}
vectorial_inline simd8f simd8f_splat_2f(
  float x,
  float y)
{
  simd8f s = { { { x, y, x, y }, { x, y, x, y } } };
  return s;
}

vectorial_inline simd8f simd8f_splat_xa(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_splat_x(v.s[0]);
  s.s[1] = simd4f_splat_x(v.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_splat_yb(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_splat_y(v.s[0]);
  s.s[1] = simd4f_splat_y(v.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_splat_zc(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_splat_z(v.s[0]);
  s.s[1] = simd4f_splat_z(v.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_splat_wd(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_splat_w(v.s[0]);
  s.s[1] = simd4f_splat_w(v.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_splat_xzac(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_splat_xz(v.s[0]);
  s.s[1] = simd4f_splat_xz(v.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_splat_ywbd(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_splat_yw(v.s[0]);
  s.s[1] = simd4f_splat_yw(v.s[1]);
  return s;
}

vectorial_inline simd8f simd8f_add(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret;
  ret.s[0] = lhs.s[0] + rhs.s[0];
  ret.s[1] = lhs.s[1] + rhs.s[1];
  return ret;
}

vectorial_inline simd8f simd8f_sub(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret;
  ret.s[0] = lhs.s[0] - rhs.s[0];
  ret.s[1] = lhs.s[1] - rhs.s[1];
  return ret;
}

vectorial_inline simd8f simd8f_mul(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret;
  ret.s[0] = lhs.s[0] * rhs.s[0];
  ret.s[1] = lhs.s[1] * rhs.s[1];
  return ret;
}

vectorial_inline simd8f simd8f_div(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret;
  ret.s[0] = lhs.s[0] / rhs.s[0];
  ret.s[1] = lhs.s[1] / rhs.s[1];
  return ret;
}

vectorial_inline simd8f simd8f_madd(
  simd8f m1,
  simd8f m2,
  simd8f a)
{
  return simd8f_add(simd8f_mul(m1, m2), a);
}

vectorial_inline simd8f simd8f_addsub(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret;
  ret.s[0] = simd2f_addsub(lhs.s[0], rhs.s[0]);
  ret.s[1] = simd2f_addsub(lhs.s[1], rhs.s[1]);
  return ret;
}

vectorial_inline simd8f simd8f_reciprocal(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_reciprocal(v.s[0]);
  s.s[1] = simd4f_reciprocal(v.s[1]);
  return s;
}

vectorial_inline simd8f simd8f_sqrt(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_sqrt(v.s[0]);
  s.s[1] = simd4f_sqrt(v.s[1]);
  return s;
}

vectorial_inline simd8f simd8f_rsqrt(
  simd8f v)
{
  simd8f s;
  s.s[0] = simd4f_rsqrt(v.s[0]);
  s.s[1] = simd4f_rsqrt(v.s[1]);
  return s;
}

vectorial_inline float simd8f_dot4_2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot4_scalar(lhs.s[1], rhs.s[1]);
}
vectorial_inline float simd8f_dot4_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot4_scalar(lhs.s[0], rhs.s[0]);
}
vectorial_inline float simd8f_dot3_2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot3_scalar(lhs.s[1], rhs.s[1]);
}
vectorial_inline float simd8f_dot3_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot3_scalar(lhs.s[0], rhs.s[0]);
}
vectorial_inline float simd8f_dot2_4_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot2_2_scalar(lhs.s[1], rhs.s[1]);
}
vectorial_inline float simd8f_dot2_3_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot2_scalar(lhs.s[1], rhs.s[1]);
}
vectorial_inline float simd8f_dot2_2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot2_2_scalar(lhs.s[0], rhs.s[0]);
}
vectorial_inline float simd8f_dot2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd4f_dot2_scalar(lhs.s[0], rhs.s[0]);
}

vectorial_inline simd8f simd8f_dot4x2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot4(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_dot4(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot4_2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_zero();
  s.s[1] = simd4f_dot4(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot4(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot4(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_zero();
  return s;
}
vectorial_inline simd8f simd8f_dot3x2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot3(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_dot3(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot3_2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_zero();
  s.s[1] = simd4f_dot3(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot3(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot3(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_zero();
  return s;
}
vectorial_inline simd8f simd8f_dot2x4(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot2x2(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_dot2x2(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot2x3(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot2x2(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_dot2(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot2x2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot2x2(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_zero();
  return s;
}
vectorial_inline simd8f simd8f_dot2_4(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_zero();
  s.s[1] = simd4f_dot2_2(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot2_3(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_zero();
  s.s[1] = simd4f_dot2(lhs.s[1], rhs.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_dot2_2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot2_2(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_zero();
  return s;
}
vectorial_inline simd8f simd8f_dot2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f s;
  s.s[0] = simd4f_dot2(lhs.s[0], rhs.s[0]);
  s.s[1] = simd4f_zero();
  return s;
}

vectorial_inline simd8f simd8f_cross3x2(
  simd8f l,
  simd8f r)
{
  simd8f s;
  s.s[0] = simd4f_cross3(l.s[0], r.s[0]);
  s.s[1] = simd4f_cross3(l.s[1], r.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_cross3_2(
  simd8f l,
  simd8f r)
{
  simd8f s;
  s.s[0] = simd4f_zero();
  s.s[1] = simd4f_cross3(l.s[1], r.s[1]);
  return s;
}
vectorial_inline simd8f simd8f_cross3(
  simd8f l,
  simd8f r)
{
  simd8f s;
  s.s[0] = simd4f_cross3(l.s[0], r.s[0]);
  s.s[1] = simd4f_zero();
  return s;
}

vectorial_inline float simd8f_length4_2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot4_2_scalar(v, v));
}
vectorial_inline float simd8f_length4_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot4_scalar(v, v));
}
vectorial_inline float simd8f_length3_2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot3_2_scalar(v, v));
}
vectorial_inline float simd8f_length3_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot3_scalar(v, v));
}
vectorial_inline float simd8f_length2_4_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_4_scalar(v, v));
}
vectorial_inline float simd8f_length2_3_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_3_scalar(v, v));
}
vectorial_inline float simd8f_length2_2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_2_scalar(v, v));
}
vectorial_inline float simd8f_length2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_scalar(v, v));
}

vectorial_inline simd8f simd8f_shuffle_yxwz_x2(
  simd8f s)
{
  simd8f ret;
  ret.s[0] = simd4f_shuffle_yxwz(s.s[0]);
  ret.s[1] = simd4f_shuffle_yxwz(s.s[1]);
  return ret;
}

vectorial_inline simd8f simd8f_zero_w_x2(
  simd8f s)
{
  simd8f ret;
  ret.s[0] = simd4f_zero_w(s.s[0]);
  ret.s[1] = simd4f_zero_w(s.s[1]);
  return ret;
}
vectorial_inline simd8f simd8f_zero_zw_x2(
  simd8f s)
{
  simd8f ret;
  ret.s[0] = simd4f_zero_zw(s.s[0]);
  ret.s[1] = simd4f_zero_zw(s.s[1]);
  return ret;
}

vectorial_inline simd8f simd8f_merge_high_4x2(
  simd8f xyzw,
  simd8f abcd)
{
  simd8f ret;
  ret.s[0] = simd4f_merge_high(xyzw.s[0], abcd.s[0]);
  ret.s[1] = simd4f_merge_high(xyzw.s[1], abcd.s[1]);
  return ret;
}

vectorial_inline simd8f simd8f_flip_sign_0101_x2(
  simd8f s)
{
  simd8f ret;
  ret.s[0] = simd4f_flip_sign_0101(s.s[0]);
  ret.s[1] = simd4f_flip_sign_0101(s.s[1]);
  return ret;
}
vectorial_inline simd8f simd8f_flip_sign_1010_x2(
  simd8f s)
{
  simd8f ret;
  ret.s[0] = simd4f_flip_sign_1010(s.s[0]);
  ret.s[1] = simd4f_flip_sign_1010(s.s[1]);
  return ret;
}

vectorial_inline simd8f simd8f_min(
  simd8f a,
  simd8f b)
{
  simd8f s;
  s.s[0] = simd4f_min(a.s[0], a.s[0]);
  s.s[1] = simd4f_min(a.s[1], a.s[1]);
  return s;
}

vectorial_inline simd8f simd8f_max(
  simd8f a,
  simd8f b)
{
  simd8f s;
  s.s[0] = simd4f_max(a.s[0], a.s[0]);
  s.s[1] = simd4f_max(a.s[1], a.s[1]);
  return s;
}

#endif

#endif

