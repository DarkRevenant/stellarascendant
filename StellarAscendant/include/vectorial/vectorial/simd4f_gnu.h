/*
 Vectorial
 Copyright (c) 2010 Mikko Lehtonen
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD4F_GNU_H
#define VECTORIAL_SIMD4F_GNU_H

#ifdef VECTORIAL_GNU

#include <math.h>
#include <string.h>  // memcpy

#define SIMD4F_ALIGNMENT (16)

typedef float simd4f __attribute__ ((vector_size (16))) simd4f_aligned16;

typedef int mask4i __attribute__ ((vector_size (16))) simd4f_aligned16;

typedef simd4f_aligned16 union {
  simd4f s;
  float f[4];
} _simd4f_union;

typedef simd4f_aligned16 union {
  unsigned int ui[4];
  float f[4];
} _simd4f_uif;

vectorial_inline simd2f simd4f_index2(
  simd4f s,
  int i)
{
  _simd4f_union u = { s };
  return simd2f_uload2(&(u.f[i]));
}
vectorial_inline float simd4f_index(
  simd4f s,
  int i)
{
  return s[i];
}

vectorial_inline float simd4f_get_x(
  simd4f s)
{
  return s[0];
}
vectorial_inline float simd4f_get_y(
  simd4f s)
{
  return s[1];
}
vectorial_inline float simd4f_get_z(
  simd4f s)
{
  return s[2];
}
vectorial_inline float simd4f_get_w(
  simd4f s)
{
  return s[3];
}

vectorial_inline simd4f simd4f_create(
  float x,
  float y,
  float z,
  float w)
{
  simd4f s = { x, y, z, w };
  return s;
}

vectorial_inline simd4f simd4f_zero(
  void)
{
  return simd4f_create(0.0f, 0.0f, 0.0f, 0.0f);
}

vectorial_inline simd4f simd4f_uload4(
  const float *restrict ary)
{
  return __builtin_ia32_loadups(ary);
}
vectorial_inline simd4f simd4f_uload3(
  const float *restrict ary)
{
  simd4f s = { ary[0], ary[1], ary[2] };
  return s;
}
vectorial_inline simd4f simd4f_uload2(
  const float *restrict ary)
{
  simd4f s = { ary[0], ary[1] };
  return s;
}

vectorial_inline void simd4f_ustore4(
  const simd4f val,
  float *restrict ary)
{
  __builtin_ia32_storeups(ary, val);
}
vectorial_inline void simd4f_ustore3(
  const simd4f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 3);
}
vectorial_inline void simd4f_ustore2(
  const simd4f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 2);
}

vectorial_inline simd4f simd4f_splat(
  float v)
{
  simd4f s = { v, v, v, v };
  return s;
}
vectorial_inline simd4f simd4f_splat_2f(
  float x,
  float y)
{
  simd4f s = { x, y, x, y };
  return s;
}

vectorial_inline simd4f simd4f_splat_x(
  simd4f v)
{
  const mask4i m = { 0, 0, 0, 0 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd4f simd4f_splat_y(
  simd4f v)
{
  const mask4i m = { 1, 1, 1, 1 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd4f simd4f_splat_z(
  simd4f v)
{
  const mask4i m = { 2, 2, 2, 2 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd4f simd4f_splat_w(
  simd4f v)
{
  const mask4i m = { 3, 3, 3, 3 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd4f simd4f_splat_xz(
  simd4f v)
{
  const mask4i m = { 0, 0, 2, 2 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd4f simd4f_splat_yw(
  simd4f v)
{
  const mask4i m = { 1, 1, 3, 3 };
  return __builtin_shuffle(v, m);
}

vectorial_inline simd4f simd4f_add(
  simd4f lhs,
  simd4f rhs)
{
  simd4f ret = lhs + rhs;
  return ret;
}

vectorial_inline simd4f simd4f_sub(
  simd4f lhs,
  simd4f rhs)
{
  simd4f ret = lhs - rhs;
  return ret;
}

vectorial_inline simd4f simd4f_mul(
  simd4f lhs,
  simd4f rhs)
{
  simd4f ret = lhs * rhs;
  return ret;
}

vectorial_inline simd4f simd4f_div(
  simd4f lhs,
  simd4f rhs)
{
  simd4f ret = lhs / rhs;
  return ret;
}

vectorial_inline simd4f simd4f_madd(
  simd4f m1,
  simd4f m2,
  simd4f a)
{
  return simd4f_add(simd4f_mul(m1, m2), a);
}

vectorial_inline simd4f simd4f_addsub(
  simd4f lhs,
  simd4f rhs)
{
  return __builtin_ia32_addsubps(lhs, rhs);
}

vectorial_inline simd4f simd4f_reciprocal(
  simd4f v)
{
  simd4f s = __builtin_ia32_rcpps(v);
  const simd4f two = simd4f_splat(2.0f);
  s = simd4f_mul(s, simd4f_sub(two, simd4f_mul(v, s)));
  return s;
}

vectorial_inline simd4f simd4f_sqrt(
  simd4f v)
{
  simd4f s = __builtin_ia32_sqrtps(v);
  const simd4f half = simd4f_splat(0.5f);
  s = simd4f_mul(simd4f_add(simd4f_div(v, s), s), half);
  return s;
}

vectorial_inline simd4f simd4f_rsqrt(
  simd4f v)
{
  simd4f s = __builtin_ia32_rsqrtps(v);
  const simd4f half = simd4f_splat(0.5f);
  const simd4f three = simd4f_splat(3.0f);
  s = simd4f_mul(simd4f_mul(s, half), simd4f_sub(three, simd4f_mul(s, simd4f_mul(v, s))));
  return s;
}

vectorial_inline float simd4f_dot4_scalar(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return simd4f_get_x(__builtin_ia32_dpps(lhs, rhs, 0xF1));
#else
  const simd4f m = simd4f_mul(lhs, rhs);
  _simd4f_union u = {m};
  return u.f[0] + u.f[1] + u.f[2] + u.f[3];
#endif
}
vectorial_inline float simd4f_dot3_scalar(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return simd4f_get_x(__builtin_ia32_dpps(lhs, rhs, 0x71));
#else
  _simd4f_union l = {lhs};
  _simd4f_union r = {rhs};
  return l.f[0] * r.f[0] + l.f[1] * r.f[1] + l.f[2] * r.f[2];
#endif
}
vectorial_inline float simd4f_dot2_2_scalar(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return simd4f_get_x(__builtin_ia32_dpps(lhs, rhs, 0xC1));
#else
  _simd4f_union l = {lhs};
  _simd4f_union r = {rhs};
  return l.f[2] * r.f[2] + l.f[3] * r.f[3];
#endif
}
vectorial_inline float simd4f_dot2_scalar(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return simd4f_get_x(__builtin_ia32_dpps(lhs, rhs, 0x31));
#else
  _simd4f_union l = {lhs};
  _simd4f_union r = {rhs};
  return l.f[0] * r.f[0] + l.f[1] * r.f[1];
#endif
}

vectorial_inline simd4f simd4f_dot4(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return __builtin_ia32_dpps(lhs, rhs, 0xFF);
#else
  return simd4f_splat(simd4f_dot4_scalar(lhs, rhs));
#endif
}
vectorial_inline simd4f simd4f_dot3(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return __builtin_ia32_dpps(lhs, rhs, 0x77);
#else
  return simd4f_splat(simd4f_dot3_scalar(lhs, rhs));
#endif
}
vectorial_inline simd4f simd4f_dot2x2(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  simd4f a = __builtin_ia32_dpps(lhs, rhs, 0x33);
  simd4f b = __builtin_ia32_dpps(lhs, rhs, 0xCC);
  return simd4f_add(a, b);
#else
  const simd4f m = simd4f_mul(lhs, rhs);
  const simd4f s1 = simd4f_add(simd4f_splat_xz(m), simd4f_splat_yw(m));
  return s1;
#endif
}
vectorial_inline simd4f simd4f_dot2_2(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return __builtin_ia32_dpps(lhs, rhs, 0xCC);
#else
  return simd4f_splat(simd4f_dot2_2_scalar(lhs, rhs));
#endif
}
vectorial_inline simd4f simd4f_dot2(
  simd4f lhs,
  simd4f rhs)
{
#ifdef _SSE4_1
  return __builtin_ia32_dpps(lhs, rhs, 0x33);
#else
  return simd4f_splat(simd4f_dot2_scalar(lhs, rhs));
#endif
}

vectorial_inline simd4f simd4f_cross3(
  simd4f l,
  simd4f r)
{
  const mask4i m1 = { 1, 2, 0, 3 };
  const mask4i m2 = { 2, 0, 1, 3 };
  return simd4f_sub(
      simd4f_mul(__builtin_shuffle(l, m1), __builtin_shuffle(r, m2)),
      simd4f_mul(__builtin_shuffle(l, m2), __builtin_shuffle(r, m1)));
}

vectorial_inline float simd4f_length4_scalar(
  simd4f v)
{
  return sqrtf(simd4f_dot4_scalar(v, v));
}
vectorial_inline float simd4f_length3_scalar(
  simd4f v)
{
  return sqrtf(simd4f_dot3_scalar(v, v));
}
vectorial_inline float simd4f_length2_2_scalar(
  simd4f v)
{
  return sqrtf(simd4f_dot2_2_scalar(v, v));
}
vectorial_inline float simd4f_length2_scalar(
  simd4f v)
{
  return sqrtf(simd4f_dot2_scalar(v, v));
}

vectorial_inline simd4f simd4f_shuffle_wxyz(
  simd4f s)
{
  const mask4i m = { 3, 0, 1, 2 };
  return __builtin_shuffle(s, m);
}
vectorial_inline simd4f simd4f_shuffle_zwxy(
  simd4f s)
{
  const mask4i m = { 2, 3, 0, 1 };
  return __builtin_shuffle(s, m);
}
vectorial_inline simd4f simd4f_shuffle_yzwx(
  simd4f s)
{
  const mask4i m = { 1, 2, 3, 0 };
  return __builtin_shuffle(s, m);
}
vectorial_inline simd4f simd4f_shuffle_yxwz(
  simd4f s)
{
  const mask4i m = { 1, 0, 3, 2 };
  return __builtin_shuffle(s, m);
}

vectorial_inline simd4f simd4f_zero_w(
  simd4f s)
{
  simd4f r = __builtin_ia32_unpckhps(s, simd4f_zero());
  return __builtin_ia32_movlhps(s, r);
}
vectorial_inline simd4f simd4f_zero_zw(
  simd4f s)
{
  return __builtin_ia32_movlhps(s, simd4f_zero());
}

vectorial_inline simd4f simd4f_merge_high(
  simd4f xyzw,
  simd4f abcd)
{
  return __builtin_ia32_movhlps(abcd, xyzw);
}

vectorial_inline simd4f simd4f_flip_sign_0101(
  simd4f s)
{
  const _simd4f_uif upnpn = { { 0x00000000, 0x80000000, 0x00000000, 0x80000000 } };
  return __builtin_ia32_xorps(s, simd4f_uload4(upnpn.f));
}

vectorial_inline simd4f simd4f_flip_sign_1010(
  simd4f s)
{
  const _simd4f_uif unpnp = { { 0x80000000, 0x00000000, 0x80000000, 0x00000000 } };
  return __builtin_ia32_xorps(s, simd4f_uload4(unpnp.f));
}

vectorial_inline simd4f simd4f_min(
  simd4f a,
  simd4f b)
{
  return __builtin_ia32_minps(a, b);
}

vectorial_inline simd4f simd4f_max(
  simd4f a,
  simd4f b)
{
  return __builtin_ia32_maxps(a, b);
}

#endif

#endif

