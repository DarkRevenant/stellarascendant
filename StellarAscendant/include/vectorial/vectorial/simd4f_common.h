/*
 Vectorial
 Copyright (c) 2010 Mikko Lehtonen
 Copyright (c) 2014 Google, Inc.
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD4F_COMMON_H
#define VECTORIAL_SIMD4F_COMMON_H

vectorial_inline simd4f simd4f_length4(
  simd4f v)
{
  return simd4f_sqrt(simd4f_dot4(v, v));
}
vectorial_inline simd4f simd4f_length3(
  simd4f v)
{
  return simd4f_sqrt(simd4f_dot3(v, v));
}
vectorial_inline simd4f simd4f_length2x2(
  simd4f v)
{
  return simd4f_sqrt(simd4f_dot2x2(v, v));
}
vectorial_inline simd4f simd4f_length2_2(
  simd4f v)
{
  return simd4f_sqrt(simd4f_dot2_2(v, v));
}
vectorial_inline simd4f simd4f_length2(
  simd4f v)
{
  return simd4f_sqrt(simd4f_dot2(v, v));
}

vectorial_inline float simd4f_length4_squared_scalar(
  simd4f v)
{
  return simd4f_dot4_scalar(v, v);
}
vectorial_inline float simd4f_length3_squared_scalar(
  simd4f v)
{
  return simd4f_dot3_scalar(v, v);
}
vectorial_inline float simd4f_length2_2_squared_scalar(
  simd4f v)
{
  return simd4f_dot2_2_scalar(v, v);
}
vectorial_inline float simd4f_length2_squared_scalar(
  simd4f v)
{
  return simd4f_dot2_scalar(v, v);
}

vectorial_inline simd4f simd4f_length4_squared(
  simd4f v)
{
  return simd4f_dot4(v, v);
}
vectorial_inline simd4f simd4f_length3_squared(
  simd4f v)
{
  return simd4f_dot3(v, v);
}
vectorial_inline simd4f simd4f_length2x2_squared(
  simd4f v)
{
  return simd4f_dot2x2(v, v);
}
vectorial_inline simd4f simd4f_length2_2_squared(
  simd4f v)
{
  return simd4f_dot2_2(v, v);
}
vectorial_inline simd4f simd4f_length2_squared(
  simd4f v)
{
  return simd4f_dot2(v, v);
}

vectorial_inline simd4f simd4f_normalize4(
  simd4f a)
{
  simd4f invlen = simd4f_rsqrt(simd4f_dot4(a, a));
  return simd4f_mul(a, invlen);
}
vectorial_inline simd4f simd4f_normalize3(
  simd4f a)
{
  simd4f invlen = simd4f_rsqrt(simd4f_dot3(a, a));
  return simd4f_mul(a, invlen);
}
vectorial_inline simd4f simd4f_normalize2x2(
  simd4f a)
{
  simd4f invlen = simd4f_rsqrt(simd4f_dot2x2(a, a));
  return simd4f_mul(a, invlen);
}
vectorial_inline simd4f simd4f_normalize2_2(
  simd4f a)
{
  simd4f invlen = simd4f_rsqrt(simd4f_dot2_2(a, a));
  return simd4f_mul(a, invlen);
}
vectorial_inline simd4f simd4f_normalize2(
  simd4f a)
{
  simd4f invlen = simd4f_rsqrt(simd4f_dot2(a, a));
  return simd4f_mul(a, invlen);
}

vectorial_inline simd4f simd4f_rotate2x2(
  simd4f v,
  float radians)
{
  float sine = sinf(radians);
  float cosine = cosf(radians);
  simd4f c1 = simd4f_splat(cosine);
  simd4f c2 = simd4f_splat(sine);
  simd4f r1 = simd4f_mul(v, c1);
  simd4f r2 = simd4f_mul(v, c2);
  return simd4f_addsub(r1, simd4f_shuffle_yxwz(r2));
}

vectorial_inline simd4f simd4f_scale2x2(
  simd4f v,
  float x,
  float y)
{
  simd4f c = simd4f_splat_2f(x, y);
  return simd4f_mul(v, c);
}

vectorial_inline simd4f simd4f_translate2x2(
  simd4f v,
  float x,
  float y)
{
  simd4f c = simd4f_splat_2f(x, y);
  return simd4f_add(v, c);
}

#endif
