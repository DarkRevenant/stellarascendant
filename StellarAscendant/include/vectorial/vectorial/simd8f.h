/*
 Vectorial
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */

#ifndef VECTORIAL_SIMD8F_H
#define VECTORIAL_SIMD8F_H

#include <vectorial/config.h>
#include <vectorial/simd2f.h>
#include <vectorial/simd4f.h>

/* EXTREMELY IMPORTANT USAGE NOTE:
 * simd8f can *NEVER* be passed as a function argument, and most likely can't ever be returned directly either.
 * The functions in this library are special exceptions to this rule, specifically because they are inlined.
 * This is actually why the SIMD functions have forced inlining, even on a debug build.
 * User-space functions must take great care to use pointers for passing around vectors.
 * Therefore, simd4f should be preferred in any situation where the pointer indirection would be a significant cost.
 * Never directly use malloc/calloc for a SIMD structure; always align on a 32-byte boundary.
 */

#if defined(VECTORIAL_GNU)
#  if defined(_AVX)
#    include <vectorial/simd8f_gnu_avx.h>
#  else
#    include <vectorial/simd8f_gnu_sse.h>
#  endif
#else
#  error No implementation defined
#endif

#include <vectorial/simd8f_common.h>

#endif

