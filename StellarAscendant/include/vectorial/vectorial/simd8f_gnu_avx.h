/*
 Vectorial
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD8F_GNU_AVX_H
#define VECTORIAL_SIMD8F_GNU_AVX_H

#if defined(VECTORIAL_GNU) && defined(_AVX)

#include <math.h>
#include <string.h>  // memcpy

#define SIMD8F_ALIGNMENT (32)

typedef float simd8f __attribute__ ((vector_size (32))) simd8f_aligned32;

typedef int mask8i __attribute__ ((vector_size (32))) simd8f_aligned32;

typedef simd8f_aligned32 union {
  simd8f s;
  float f[8];
} _simd8f_union;

typedef simd8f_aligned32 union {
  unsigned int ui[8];
  float f[8];
} _simd8f_uif;

vectorial_inline simd4f simd8f_index4(
  simd8f s,
  int i)
{
  _simd8f_union u = { s };
  return simd4f_uload4(&(u.f[i]));
}
vectorial_inline simd2f simd8f_index2(
  simd8f s,
  int i)
{
  _simd8f_union u = { s };
  return simd2f_uload2(&(u.f[i]));
}
vectorial_inline float simd8f_index(
  simd8f s,
  int i)
{
  return s[i];
}

vectorial_inline float simd8f_get_x(
  simd8f s)
{
  return s[0];
}
vectorial_inline float simd8f_get_y(
  simd8f s)
{
  return s[1];
}
vectorial_inline float simd8f_get_z(
  simd8f s)
{
  return s[2];
}
vectorial_inline float simd8f_get_w(
  simd8f s)
{
  return s[3];
}
vectorial_inline float simd8f_get_a(
  simd8f s)
{
  return s[4];
}
vectorial_inline float simd8f_get_b(
  simd8f s)
{
  return s[5];
}
vectorial_inline float simd8f_get_c(
  simd8f s)
{
  return s[6];
}
vectorial_inline float simd8f_get_d(
  simd8f s)
{
  return s[7];
}

vectorial_inline simd8f simd8f_create(
  float x,
  float y,
  float z,
  float w,
  float a,
  float b,
  float c,
  float d)
{
  simd8f s = { x, y, z, w, a, b, c, d };
  return s;
}

vectorial_inline simd8f simd8f_zero(
  void)
{
  return simd8f_create(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
}

vectorial_inline simd8f simd8f_uload8(
  const float *restrict ary)
{
  return __builtin_ia32_loadups256(ary);
}
vectorial_inline simd8f simd8f_uload6(
  const float *restrict ary)
{
  simd8f s = { ary[0], ary[1], ary[2], ary[3], ary[4], ary[5] };
  return s;
}
vectorial_inline simd8f simd8f_uload4(
  const float *restrict ary)
{
  simd8f s = { ary[0], ary[1], ary[2], ary[3] };
  return s;
}
vectorial_inline simd8f simd8f_uload3x2(
  const float *restrict ary)
{
  simd8f s = { ary[0], ary[1], ary[2], 0, ary[3], ary[4], ary[5] };
  return s;
}
vectorial_inline simd8f simd8f_uload3(
  const float *restrict ary)
{
  simd8f s = { ary[0], ary[1], ary[2] };
  return s;
}
vectorial_inline simd8f simd8f_uload2(
  const float *restrict ary)
{
  simd8f s = { ary[0], ary[1] };
  return s;
}

vectorial_inline void simd8f_ustore8(
  const simd8f val,
  float *restrict ary)
{
  __builtin_ia32_storeups256(ary, val);
}
vectorial_inline void simd8f_ustore6(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 6);
}
vectorial_inline void simd8f_ustore4(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 4);
}
vectorial_inline void simd8f_ustore3x2(
  const simd8f val,
  float *restrict ary)
{
  float *ptr = (float *) &val;
  memcpy(ary, ptr, sizeof(float) * 3);
  memcpy(ary + 3, ptr + 4, sizeof(float) * 3);
}
vectorial_inline void simd8f_ustore3(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 3);
}
vectorial_inline void simd8f_ustore2(
  const simd8f val,
  float *restrict ary)
{
  memcpy(ary, &val, sizeof(float) * 2);
}

vectorial_inline simd8f simd8f_splat(
  float v)
{
  simd8f s = { v, v, v, v, v, v, v, v };
  return s;
}
vectorial_inline simd8f simd8f_splat_4f(
  float x,
  float y,
  float z,
  float w)
{
  simd8f s = { x, y, z, w, x, y, z, w };
  return s;
}
vectorial_inline simd8f simd8f_splat_2f(
  float x,
  float y)
{
  simd8f s = { x, y, x, y, x, y, x, y };
  return s;
}

vectorial_inline simd8f simd8f_splat_yb(
  simd8f v)
{
  const mask8i m = { 1, 1, 1, 1, 5, 5, 5, 5 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd8f simd8f_splat_zc(
  simd8f v)
{
  const mask8i m = { 2, 2, 2, 2, 6, 6, 6, 6 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd8f simd8f_splat_wd(
  simd8f v)
{
  const mask8i m = { 3, 3, 3, 3, 7, 7, 7, 7 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd8f simd8f_splat_xzac(
  simd8f v)
{
  const mask8i m = { 0, 0, 2, 2, 4, 4, 6, 6 };
  return __builtin_shuffle(v, m);
}
vectorial_inline simd8f simd8f_splat_ywbd(
  simd8f v)
{
  const mask8i m = { 1, 1, 3, 3, 5, 5, 7, 7 };
  return __builtin_shuffle(v, m);
}

vectorial_inline simd8f simd8f_add(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret = lhs + rhs;
  return ret;
}

vectorial_inline simd8f simd8f_sub(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret = lhs - rhs;
  return ret;
}

vectorial_inline simd8f simd8f_mul(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret = lhs * rhs;
  return ret;
}

vectorial_inline simd8f simd8f_div(
  simd8f lhs,
  simd8f rhs)
{
  simd8f ret = lhs / rhs;
  return ret;
}

vectorial_inline simd8f simd8f_madd(
  simd8f m1,
  simd8f m2,
  simd8f a)
{
  return simd8f_add(simd8f_mul(m1, m2), a);
}

vectorial_inline simd8f simd8f_addsub(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_addsubps256(lhs, rhs);
}

vectorial_inline simd8f simd8f_reciprocal(
  simd8f v)
{
  simd8f s = __builtin_ia32_rcpps256(v);
  const simd8f two = simd8f_splat(2.0f);
  s = simd8f_mul(s, simd8f_sub(two, simd8f_mul(v, s)));
  return s;
}

vectorial_inline simd8f simd8f_sqrt(
  simd8f v)
{
  return __builtin_ia32_sqrtps_nr256(v);
}

vectorial_inline simd8f simd8f_rsqrt(
  simd8f v)
{
  return __builtin_ia32_rsqrtps_nr256(v);
}

vectorial_inline float simd8f_dot4_2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0xF1));
}
vectorial_inline float simd8f_dot4_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0xF1));
}
vectorial_inline float simd8f_dot3_2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0x71));
}
vectorial_inline float simd8f_dot3_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0x71));
}
vectorial_inline float simd8f_dot2_4_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0xC1));
}
vectorial_inline float simd8f_dot2_3_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0x31));
}
vectorial_inline float simd8f_dot2_2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0xC1));
}
vectorial_inline float simd8f_dot2_scalar(
  simd8f lhs,
  simd8f rhs)
{
  return simd8f_get_x(__builtin_ia32_dpps256(lhs, rhs, 0x31));
}

vectorial_inline simd8f simd8f_dot4x2(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0xFF);
}
vectorial_inline simd8f simd8f_dot4_2(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0xFF);
}
vectorial_inline simd8f simd8f_dot4(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0xFF);
}
vectorial_inline simd8f simd8f_dot3x2(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0x77);
}
vectorial_inline simd8f simd8f_dot3_2(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0x77);
}
vectorial_inline simd8f simd8f_dot3(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0x77);
}
vectorial_inline simd8f simd8f_dot2x4(
  simd8f lhs,
  simd8f rhs)
{
  simd8f a = __builtin_ia32_dpps256(lhs, rhs, 0x33);
  simd8f b = __builtin_ia32_dpps256(lhs, rhs, 0xCC);
  return simd8f_add(a, b);
}
vectorial_inline simd8f simd8f_dot2x3(
  simd8f lhs,
  simd8f rhs)
{
  simd8f a = __builtin_ia32_dpps256(lhs, rhs, 0x33);
  simd8f b = __builtin_ia32_dpps256(lhs, rhs, 0xCC);
  return simd8f_add(a, b);
}
vectorial_inline simd8f simd8f_dot2x2(
  simd8f lhs,
  simd8f rhs)
{
  simd8f a = __builtin_ia32_dpps256(lhs, rhs, 0x33);
  simd8f b = __builtin_ia32_dpps256(lhs, rhs, 0xCC);
  return simd8f_add(a, b);
}
vectorial_inline simd8f simd8f_dot2_4(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0xCC);
}
vectorial_inline simd8f simd8f_dot2_3(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0x33);
}
vectorial_inline simd8f simd8f_dot2_2(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0xCC);
}
vectorial_inline simd8f simd8f_dot2(
  simd8f lhs,
  simd8f rhs)
{
  return __builtin_ia32_dpps256(lhs, rhs, 0x33);
}

vectorial_inline simd8f simd8f_cross3x2(
  simd8f l,
  simd8f r)
{
  const mask8i m1 = { 1, 2, 0, 3, 5, 6, 4, 7 };
  const mask8i m2 = { 2, 0, 1, 3, 6, 4, 5, 7 };
  return simd8f_sub(
      simd8f_mul(__builtin_shuffle(l, m1), __builtin_shuffle(r, m2)),
      simd8f_mul(__builtin_shuffle(l, m2), __builtin_shuffle(r, m1)));
}
vectorial_inline simd8f simd8f_cross3_2(
  simd8f l,
  simd8f r)
{
  const mask8i m1 = { 0, 1, 2, 3, 5, 6, 4, 7 };
  const mask8i m2 = { 0, 1, 2, 3, 6, 4, 5, 7 };
  return simd8f_sub(
      simd8f_mul(__builtin_shuffle(l, m1), __builtin_shuffle(r, m2)),
      simd8f_mul(__builtin_shuffle(l, m2), __builtin_shuffle(r, m1)));
}
vectorial_inline simd8f simd8f_cross3(
  simd8f l,
  simd8f r)
{
  const mask8i m1 = { 1, 2, 0, 3, 4, 5, 6, 7 };
  const mask8i m2 = { 2, 0, 1, 3, 4, 5, 6, 7 };
  return simd8f_sub(
      simd8f_mul(__builtin_shuffle(l, m1), __builtin_shuffle(r, m2)),
      simd8f_mul(__builtin_shuffle(l, m2), __builtin_shuffle(r, m1)));
}

vectorial_inline float simd8f_length4_2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot4_2_scalar(v, v));
}
vectorial_inline float simd8f_length4_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot4_scalar(v, v));
}
vectorial_inline float simd8f_length3_2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot3_2_scalar(v, v));
}
vectorial_inline float simd8f_length3_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot3_scalar(v, v));
}
vectorial_inline float simd8f_length2_4_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_4_scalar(v, v));
}
vectorial_inline float simd8f_length2_3_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_3_scalar(v, v));
}
vectorial_inline float simd8f_length2_2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_2_scalar(v, v));
}
vectorial_inline float simd8f_length2_scalar(
  simd8f v)
{
  return sqrtf(simd8f_dot2_scalar(v, v));
}

vectorial_inline simd8f simd8f_shuffle_yxwz_x2(
  simd8f s)
{
  const mask8i m = { 1, 0, 3, 2, 5, 4, 7, 6 };
  return __builtin_shuffle(s, m);
}

vectorial_inline simd8f simd8f_zero_w_x2(
  simd8f s)
{
  const mask8i m = { 0, 1, 2, 7, 4, 5, 6, 11 };
  return __builtin_shuffle(s, simd8f_zero(), m);
}
vectorial_inline simd8f simd8f_zero_zw_x2(
  simd8f s)
{
  const mask8i m = { 0, 1, 6, 7, 4, 5, 10, 11 };
  return __builtin_shuffle(s, simd8f_zero(), m);
}

vectorial_inline simd8f simd8f_merge_high_4x2(
  simd8f xyzw,
  simd8f abcd)
{
  const mask8i m = { 2, 3, 2, 3, 6, 7, 6, 7 };
  return __builtin_shuffle(xyzw, abcd, m);
}

vectorial_inline simd8f simd8f_flip_sign_0101_x2(
  simd8f s)
{
  const _simd8f_uif upnpn = {
    { 0x00000000, 0x80000000, 0x00000000, 0x80000000,
      0x00000000, 0x80000000, 0x00000000, 0x80000000 } };
  return __builtin_ia32_xorps256(s, simd8f_uload8(upnpn.f));
}
vectorial_inline simd8f simd8f_flip_sign_1010_x2(
  simd8f s)
{
  const _simd8f_uif unpnp = {
    { 0x80000000, 0x00000000, 0x80000000, 0x00000000,
      0x80000000, 0x00000000, 0x80000000, 0x00000000 } };
  return __builtin_ia32_xorps256(s, simd8f_uload8(unpnp.f));
}

vectorial_inline simd8f simd8f_min(
  simd8f a,
  simd8f b)
{
  return __builtin_ia32_minps256(a, b);
}

vectorial_inline simd8f simd8f_max(
  simd8f a,
  simd8f b)
{
  return __builtin_ia32_maxps256(a, b);
}

#endif

#endif

