/*
 Vectorial
 Copyright (c) 2014 Google, Inc.
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */

#ifndef VECTORIAL_SIMD2F_H
#define VECTORIAL_SIMD2F_H

#include <vectorial/config.h>

#if defined(VECTORIAL_GNU)
#  if defined(_SSE)
#    include <vectorial/simd2f_gnu_sse.h>
#  else
#    include <vectorial/simd2f_gnu_mmx.h>
#  endif
#else
#  error No implementation defined
#endif

#include <vectorial/simd2f_common.h>

#endif

