/*
 Vectorial
 Copyright (c) 2010 Mikko Lehtonen
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_CONFIG_H
#define VECTORIAL_CONFIG_H

#ifndef VECTORIAL_FORCED
#  if defined(_SSE) || (_M_IX86_FP > 0) || (_M_X64 > 0)
#    define VECTORIAL_SSE
// Don't use gnu extension for arm, buggy with some gccs with armv6 and -Os,
// Also doesn't seem perform as well
#  elif defined(__GNUC__) && !defined(__arm__)
#    define VECTORIAL_GNU
#  else
#    define VECTORIAL_SCALAR
#  endif
#endif

#ifdef VECTORIAL_SCALAR
#  define VECTORIAL_SIMD_TYPE "scalar"
#endif

#ifdef VECTORIAL_SSE
#  define VECTORIAL_SIMD_TYPE "sse"
#endif

#ifdef VECTORIAL_GNU
#  define VECTORIAL_SIMD_TYPE "gnu"
#endif

#if defined(VECTORIAL_FORCED) && !defined(VECTORIAL_SIMD_TYPE)
#  error VECTORIAL_FORCED set but no simd-type found, try f.ex. VECTORIAL_SCALAR
#endif

#if defined(__GNUC__)
#  define vectorial_inline  static inline __attribute__((always_inline))
#  define simd4f_aligned8   __attribute__ ((aligned (8)))
#  define simd4f_aligned16  __attribute__ ((aligned (16)))
#  define simd8f_aligned32  __attribute__ ((aligned (32)))
#elif defined(_WIN32)
#  define vectorial_inline   static inline
#  define vectorial_restrict
#  define simd4f_aligned8    __declspec(align(8))
#  define simd4f_aligned16   __declspec(align(16))
#  define simd8f_aligned32   __declspec(align(32))
#else
#  define vectorial_inline    static inline
#  define vectorial_restrict  restrict
#  define simd4f_aligned8
#  define simd4f_aligned16
#  define simd8f_aligned32
#endif
// #define vectorial_restrict

#ifdef __GNUC__
#  define vectorial_pure __attribute__((pure))
#else
#  define vectorial_pure
#endif

#ifdef _WIN32
#  if defined(min) || defined(max)
#pragma message ( "set NOMINMAX as preprocessor macro, undefining min/max " )
#undef min
#undef max
#  endif
#endif

#define SIMD_PARAM(t, p) t p

#define VECTORIAL_PI      3.14159265f
#define VECTORIAL_HALFPI  1.57079633f

#endif
