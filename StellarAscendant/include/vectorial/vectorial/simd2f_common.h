/*
 Vectorial
 Copyright (c) 2014 Google
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD2F_COMMON_H
#define VECTORIAL_SIMD2F_COMMON_H

vectorial_inline simd2f simd2f_length2(
  simd2f v)
{
  return simd2f_sqrt(simd2f_dot2(v, v));
}

vectorial_inline float simd2f_length2_squared_scalar(
  simd2f v)
{
  return simd2f_dot2_scalar(v, v);
}

vectorial_inline simd2f simd2f_length2_squared(
  simd2f v)
{
  return simd2f_dot2(v, v);
}

vectorial_inline simd2f simd2f_normalize2(
  simd2f a)
{
  simd2f invlen = simd2f_rsqrt(simd2f_dot2(a, a));
  return simd2f_mul(a, invlen);
}

vectorial_inline simd2f simd2f_rotate2(
  simd2f v,
  float radians)
{
  float sine = sinf(radians);
  float cosine = cosf(radians);
  simd2f c1 = simd2f_splat(cosine);
  simd2f c2 = simd2f_splat(sine);
  simd2f r1 = simd2f_mul(v, c1);
  simd2f r2 = simd2f_mul(v, c2);
  return simd2f_addsub(r1, simd2f_shuffle_yx(r2));
}

vectorial_inline simd2f simd2f_scale2(
  simd2f v,
  float x,
  float y)
{
  simd2f c = simd2f_create(x, y);
  return simd2f_mul(v, c);
}

vectorial_inline simd2f simd2f_translate2(
  simd2f v,
  float x,
  float y)
{
  simd2f c = simd2f_create(x, y);
  return simd2f_add(v, c);
}

#endif
