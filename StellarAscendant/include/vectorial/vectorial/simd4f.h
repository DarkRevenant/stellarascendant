/*
 Vectorial
 Copyright (c) 2010 Mikko Lehtonen
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */

#ifndef VECTORIAL_SIMD4F_H
#define VECTORIAL_SIMD4F_H

#include <vectorial/config.h>
#include <vectorial/simd2f.h>

#if defined(VECTORIAL_GNU)
#  include <vectorial/simd4f_gnu.h>
#else
#  error No implementation defined
#endif

#include <vectorial/simd4f_common.h>

#endif

