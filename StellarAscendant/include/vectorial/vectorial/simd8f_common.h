/*
 Vectorial
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD8F_COMMON_H
#define VECTORIAL_SIMD8F_COMMON_H

vectorial_inline simd8f simd8f_length4x2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot4x2(v, v));
}
vectorial_inline simd8f simd8f_length4_2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot4_2(v, v));
}
vectorial_inline simd8f simd8f_length4(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot4(v, v));
}
vectorial_inline simd8f simd8f_length3x2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot3x2(v, v));
}
vectorial_inline simd8f simd8f_length3_2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot3_2(v, v));
}
vectorial_inline simd8f simd8f_length3(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot3(v, v));
}
vectorial_inline simd8f simd8f_length2x4(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2x4(v, v));
}
vectorial_inline simd8f simd8f_length2x3(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2x3(v, v));
}
vectorial_inline simd8f simd8f_length2x2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2x2(v, v));
}
vectorial_inline simd8f simd8f_length2_4(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2_4(v, v));
}
vectorial_inline simd8f simd8f_length2_3(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2_3(v, v));
}
vectorial_inline simd8f simd8f_length2_2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2_2(v, v));
}
vectorial_inline simd8f simd8f_length2(
  simd8f v)
{
  return simd8f_sqrt(simd8f_dot2(v, v));
}

vectorial_inline float simd8f_length4_2_squared_scalar(
  simd8f v)
{
  return simd8f_dot4_2_scalar(v, v);
}
vectorial_inline float simd8f_length4_squared_scalar(
  simd8f v)
{
  return simd8f_dot4_scalar(v, v);
}
vectorial_inline float simd8f_length3_2_squared_scalar(
  simd8f v)
{
  return simd8f_dot3_2_scalar(v, v);
}
vectorial_inline float simd8f_length3_squared_scalar(
  simd8f v)
{
  return simd8f_dot3_scalar(v, v);
}
vectorial_inline float simd8f_length2_4_squared_scalar(
  simd8f v)
{
  return simd8f_dot2_4_scalar(v, v);
}
vectorial_inline float simd8f_length2_3_squared_scalar(
  simd8f v)
{
  return simd8f_dot2_3_scalar(v, v);
}
vectorial_inline float simd8f_length2_2_squared_scalar(
  simd8f v)
{
  return simd8f_dot2_2_scalar(v, v);
}
vectorial_inline float simd8f_length2_squared_scalar(
  simd8f v)
{
  return simd8f_dot2_scalar(v, v);
}

vectorial_inline simd8f simd8f_length4x2_squared(
  simd8f v)
{
  return simd8f_dot4x2(v, v);
}
vectorial_inline simd8f simd8f_length4_2_squared(
  simd8f v)
{
  return simd8f_dot4_2(v, v);
}
vectorial_inline simd8f simd8f_length4_squared(
  simd8f v)
{
  return simd8f_dot4(v, v);
}
vectorial_inline simd8f simd8f_length3x2_squared(
  simd8f v)
{
  return simd8f_dot3x2(v, v);
}
vectorial_inline simd8f simd8f_length3_2_squared(
  simd8f v)
{
  return simd8f_dot3_2(v, v);
}
vectorial_inline simd8f simd8f_length3_squared(
  simd8f v)
{
  return simd8f_dot3(v, v);
}
vectorial_inline simd8f simd8f_length2x4_squared(
  simd8f v)
{
  return simd8f_dot2x4(v, v);
}
vectorial_inline simd8f simd8f_length2x3_squared(
  simd8f v)
{
  return simd8f_dot2x3(v, v);
}
vectorial_inline simd8f simd8f_length2x2_squared(
  simd8f v)
{
  return simd8f_dot2x2(v, v);
}
vectorial_inline simd8f simd8f_length2_4_squared(
  simd8f v)
{
  return simd8f_dot2_4(v, v);
}
vectorial_inline simd8f simd8f_length2_3_squared(
  simd8f v)
{
  return simd8f_dot2_3(v, v);
}
vectorial_inline simd8f simd8f_length2_2_squared(
  simd8f v)
{
  return simd8f_dot2_2(v, v);
}
vectorial_inline simd8f simd8f_length2_squared(
  simd8f v)
{
  return simd8f_dot2(v, v);
}

vectorial_inline simd8f simd8f_normalize4x2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot4x2(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize4_2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot4_2(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize4(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot4(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize3x2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot3x2(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize3_2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot3_2(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize3(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot3(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2x4(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2x4(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2x3(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2x3(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2x2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2x2(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2_4(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2_4(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2_3(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2_3(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2_2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2_2(a, a));
  return simd8f_mul(a, invlen);
}
vectorial_inline simd8f simd8f_normalize2(
  simd8f a)
{
  simd8f invlen = simd8f_rsqrt(simd8f_dot2(a, a));
  return simd8f_mul(a, invlen);
}

vectorial_inline simd8f simd8f_rotate2x4(
  simd8f v,
  float radians)
{
  float sine = sinf(radians);
  float cosine = cosf(radians);
  simd8f c1 = simd8f_splat(cosine);
  simd8f c2 = simd8f_splat(sine);
  simd8f r1 = simd8f_mul(v, c1);
  simd8f r2 = simd8f_mul(v, c2);
  return simd8f_addsub(r1, simd8f_shuffle_yxwz_x2(r2));
}

vectorial_inline simd8f simd8f_scale2x4(
  simd8f v,
  float x,
  float y)
{
  simd8f c = simd8f_splat_2f(x, y);
  return simd8f_mul(v, c);
}

vectorial_inline simd8f simd8f_translate2x4(
  simd8f v,
  float x,
  float y)
{
  simd8f c = simd8f_splat_2f(x, y);
  return simd8f_add(v, c);
}

#endif
