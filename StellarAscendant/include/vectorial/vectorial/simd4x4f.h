/*
 Vectorial
 Copyright (c) 2010 Mikko Lehtonen
 Copyright (c) 2014 Google, Inc.
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD4X4F_H
#define VECTORIAL_SIMD4X4F_H

#include <vectorial/config.h>
#include <vectorial/simd4f.h>

/*
 Note, x,y,z,w are conceptually columns with matrix math.
 */

#define SIMD4X4F_ALIGNMENT (16)

typedef simd4f_aligned16 struct {
  simd4f x, y, z, w;
} simd4x4f;

#if defined(VECTORIAL_GNU)
#  include <vectorial/simd4x4f_gnu.h>
#else
#  error No implementation defined
#endif

#include <vectorial/simd4x4f_common.h>

#endif
