/*
 Vectorial
 Copyright (c) 2010 Mikko Lehtonen
 Copyright (c) 2016 Magehand LLC
 Licensed under the terms of the two-clause BSD License (see LICENSE)
 */
#ifndef VECTORIAL_SIMD4X4F_GNU_H
#define VECTORIAL_SIMD4X4F_GNU_H

#ifdef VECTORIAL_GNU

vectorial_inline void simd4x4f_transpose_inplace(
  simd4x4f *restrict s)
{
  const simd4f dx = { s->x[0], s->y[0], s->z[0], s->w[0] };
  const simd4f dy = { s->x[1], s->y[1], s->z[1], s->w[1] };
  const simd4f dz = { s->x[2], s->y[2], s->z[2], s->w[2] };
  const simd4f dw = { s->x[3], s->y[3], s->z[3], s->w[3] };

  s->x = dx;
  s->y = dy;
  s->z = dz;
  s->w = dw;
}

vectorial_inline void simd4x4f_transpose(
  const simd4x4f *restrict s,
  simd4x4f *restrict out)
{
  *out = *s;
  simd4x4f_transpose_inplace(out);
}

#endif

#endif
