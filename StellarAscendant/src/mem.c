/**********************************************************************************************************************
 * mem.c
 * Copyright (C) 2016 Magehand LLC
 *********************************************************************************************************************/

#include <stdatomic.h>

/** SA Headers **/
#include "mem.h"

/**
 ** Global Variables
 **/

atomic_ullong _pool_mem = 0ULL;
atomic_ullong _image_mem = 0ULL;
atomic_ullong _texture_vmem = 0ULL;
