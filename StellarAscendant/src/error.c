/**********************************************************************************************************************
 * error.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   getError      Returns string representation of the given SA error
 *********************************************************************************************************************/

/** SA Headers **/
#include "common.h"
#include "error.h"

/**
 ** Constants
 **/

/* Must match ___ERROR_H___ */
static const wchar_t* SA_Error_Images[NUM_SA_ERROR_TYPES] = {
  /* Special Case */
  W("Success"), /* SA_ERROR_SUCCESS */

  /* SA Errors */
  W("Generic error"),           //* SA_ERROR_STANDARD         *//
  W("Unknown error"),           //* SA_ERROR_UNKNOWN          *//
  W("Out of memory"),           //* SA_ERROR_MEMORY           *//
  W("Syntax error"),            //* SA_ERROR_SYNTAX           *//
  W("Invalid file format"),     //* SA_ERROR_FILE_INVALID     *//
  W("Unsupported file format"), //* SA_ERROR_FILE_UNSUPPORTED *//
  W("Invalid argument"),        //* SA_ERROR_ARGS             *//
  W("Invalid operation"),       //* SA_ERROR_OP_INVALID       *//
  W("Subsystem is offline"),    //* SA_ERROR_OFFLINE          *//
  W("File error"),              //* SA_ERROR_FILE             *//
  W("Element not found"),       //* SA_ERROR_NOT_FOUND        *//
  W("XML error"),               //* SA_ERROR_XML              *//
  W("Element type mismatch"),   //* SA_ERROR_TYPE_MISMATCH    *//
  W("Input error"),             //* SA_ERROR_INPUT            *//

    /* Omit generic errors */
    };

/**
 ** Global Functions
 **/

/* Returns string representation of the given SA error */
const wchar_t* getError(
  SA_Error_t err)
{
  if (err <= 0) {
    if (err < LAST_SA_ERROR)
      err = SA_ERROR_UNKNOWN;
    return SA_Error_Images[-err];
  }
  else {
    unsigned int errSA = ((unsigned int) err) >> SA_GENERIC_ERROR_SHIFT;
    if ((errSA == 0) || (errSA > LAST_SA_GENERIC_ERROR))
      return SA_Error_Images[-SA_ERROR_UNKNOWN];
    if ((SA_GENERIC_ERROR_XML_CREATE == GENERIC_ERROR(err)) ||
        (SA_GENERIC_ERROR_XML_PARSE == GENERIC_ERROR(err)) ||
        (SA_GENERIC_ERROR_XML_SAVE == GENERIC_ERROR(err)))
      return SA_Error_Images[-SA_ERROR_XML];
    else if (SA_GENERIC_ERROR_MS_SYSTEM == GENERIC_ERROR(err))
      return winError(err & SA_GENERIC_ERROR_MASK);
    else
      return _wcserror(err & SA_GENERIC_ERROR_MASK);
  }
}
