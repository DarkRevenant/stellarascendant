/**********************************************************************************************************************
 * common.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initCommon         Initializes common structures
 *   closeCommon        Closes common structures
 *   getTimestamp       Returns time in milliseconds since GLFW was initialized
 *   getSysTime         Returns system time in microseconds
 *   getTime            Returns high-resolution time in seconds since GLFW was initialized
 *   buildPath          Creates a file path by concatenating a varying number of elements; must be freed
 *   getPathTail        Get the tail element of a path
 *   flushToilet        Flush the log
 *   closeToilet        Stops and destroys the toilet thread
 *
 * Inline Functions:
 *   a_malloc           Assertive malloc - crashes the program if it fails
 *   a_calloc           Assertive calloc - crashes the program if it fails
 *
 * Macro Functions:
 *   BUILD_PATH         Convenient macro that makes buildPath easier to use
 *   logMsg             Prints a formatted message to a log stream
 *   _START_logMsg      Starts a formatted composite message to a log stream, with multiple lines of content
 *   logAppend          Appends to a composite message
 *   _END_logMsg        Ends a composite message
 *********************************************************************************************************************/

/** Global Headers **/
/* (headers that are synonymous with common.h) */
#include <stdlib.h>
#include <intrin.h>
#include <malloc.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>
#include <wchar.h>
#include <windows.h>

/** SA Headers **/
/* (headers that are synonymous with common.h) */
#include "config.h"
#include "error.h"
#include "main.h"
#include "mem.h"

#ifndef ___COMMON_H___
#define ___COMMON_H___

#include <pthread.h>
#include <stdio.h>

/** SA Headers **/
#include "utils/rand.h"

/**
 ** Typedefs and Definitions
 **/

/* Helps for casting void pointer fields */
#ifdef _WIN64
typedef long long void_t;
#else
typedef long void_t;
#endif

typedef enum
{
  SA_FALSE = 0,
  SA_TRUE = 1,

  SA_OVERWRITTEN_CONFIG = 2,
  SA_CREATED_CONFIG = 1,

  SA_EXIT = 1,

  SA_FAILURE = SA_ERROR_STANDARD,
  SA_SUCCESS = SA_ERROR_SUCCESS
} SA_Status_t;

typedef enum {
  LOG_FATAL,   //* Unrecoverable failure - reserve for dire problems like running out of memory *//
  LOG_ERROR,   //* Can/will cause an error state, and should be addressed promptly              *//
  LOG_WARNING, //* Undesired problems that won't cause an error state                           *//
  LOG_INFO,    //* Useful stuff that will also appear on release builds                         *//
  LOG_DEBUG,   //* Useful stuff that should only appear on debug builds                         *//

  NUM_LOG_MSG_TYPES
} SA_LogMsg_t;

#define BLANK

#define DEFAULT_PERM (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)

#define PBACK     W("..")
#define PBACK_LEN (2)
#define PSEP      W("\\")
#define PSEP_LEN  (1)

/**
 ** Macros
 **/

#define ARRAY_LEN(A) (sizeof(A) / sizeof(*(A)))

#define V2D(dec) ( (void_t) (dec) )
#define D2V(dec) ( (void *) ((void_t) (dec)) )

#define SQRT_ONE_HALF (0.7071067811865475)
#define SQRT_TWO      (1.414213562373095)

#define BYTES_PER_KBYTE     (1024)
#define BYTES_PER_MBYTE     (1048576)
#define KBYTES_TO_BYTES(kb) ((kb) * BYTES_PER_KBYTE)
#define MBYTES_TO_BYTES(mb) ((mb) * BYTES_PER_MBYTE)

#define MSEC_PER_SEC      (1000LL)
#define USEC_PER_SEC      (1000000LL)
#define NSEC_PER_SEC      (1000000000LL)
#define SEC_TO_MSEC(sec)  ((sec) * MSEC_PER_SEC)
#define SEC_TO_USEC(sec)  ((sec) * USEC_PER_SEC)
#define SEC_TO_NSEC(sec)  ((sec) * NSEC_PER_SEC)
#define MSEC_TO_SEC(msec) ((msec) / MSEC_PER_SEC)
#define USEC_TO_SEC(usec) ((usec) / USEC_PER_SEC)
#define NSEC_TO_SEC(nsec) ((nsec) / NSEC_PER_SEC)

#define MAX(a, b)            \
  ({                         \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    (_a > _b) ? _a : _b;     \
  })
#define MIN(a, b)            \
  ({                         \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    (_a < _b) ? _a : _b;     \
  })
#define ABS(x)               \
  ({                         \
    __typeof__ (x) _x = (x); \
    (_x < 0) ? -_x : _x;     \
  })

#define WITHIN_EPSILON(a, b, epsilon) \
  ( ABS((a) - (b)) > (epsilon) )

/* File paths */
#define BUILD_PATH(...)                            \
  ({                                               \
    wchar_t *_path_list[] = { __VA_ARGS__, NULL }; \
    buildPath(_path_list);                         \
  })

/* Wide strings */
#define WSTRINGIFY(s) TO_WSTRING(s)
#define TO_WSTRING(s) W(#s)
#define W(s)          L ## s

/* String conversion */
#define STRINGIFY(s)  TO_STRING(s)
#define TO_STRING(s)  #s

/* Logging functions */
#define logMsg(type, msg, ...)        wlogMsg       (type, W(msg), ##__VA_ARGS__)
#define _START_logMsg(type, msg, ...) _START_wlogMsg(type, W(msg), ##__VA_ARGS__)
#define logAppend(msg, ...)           wlogAppend    (      W(msg), ##__VA_ARGS__)
#define _END_logMsg()                 _END_wlogMsg()

#define LOCK_LOG()   (logSafe ? pthread_spin_lock(&logLock)    : 0)
#define UNLOCK_LOG() (logSafe ? pthread_spin_unlock(&logLock)  : 0)
#define FLUSH_LOG()  (logSafe ? fflush(logFile)                : 0)

#define LOG_FUNC(type, stream, ...)                     \
  ({                                                    \
    if (logToConsole) {                                 \
      fwprintf((stream), __VA_ARGS__);                  \
    }                                                   \
    if (logToFile) {                                    \
      fwprintf(logFile, __VA_ARGS__);                   \
      FLUSH_LOG();                                      \
    }                                                   \
  })

#define SUPPRESS_LOG_FATAL   (logSuppressFatal)
#define SUPPRESS_LOG_ERROR   (logSuppressError)
#define SUPPRESS_LOG_WARNING (logSuppressWarning)
#define SUPPRESS_LOG_INFO    (logSuppressInfo)
#define SUPPRESS_LOG_DEBUG   (logSuppressDebug)

#ifdef _DEBUG
#  define DEBUG_LOG_DEBUG   (SA_TRUE)
#  define DEBUG_LOG_INFO    (SA_TRUE)
#  define DEBUG_LOG_WARNING (SA_TRUE)
#  define DEBUG_LOG_ERROR   (SA_TRUE)
#  define DEBUG_LOG_FATAL   (SA_TRUE)
#else
#  define DEBUG_LOG_DEBUG   (SA_FALSE)
#  define DEBUG_LOG_INFO    (SA_FALSE)
#  define DEBUG_LOG_WARNING (SA_TRUE)
#  define DEBUG_LOG_ERROR   (SA_TRUE)
#  define DEBUG_LOG_FATAL   (SA_TRUE)
#endif

#define SUPPRESS_LOG(type) SUPPRESS_ ## type
#define DEBUG_LOG(type)    DEBUG_    ## type

#define wlogMsg(type, msg, ...)                                                                        \
  ({                                                                                                   \
    SA_LogMsg_t _type = (type);                                                                        \
    if (!SUPPRESS_LOG(type) && DEBUG_LOG(type)) {                                                      \
      LOCK_LOG();                                                                                      \
      LOG_FUNC(_type, stdout, W("[%llu] <%s> at %s:%d\n    %s(): ") msg W("\n"),                       \
               getTimestamp(), SA_LogMsg_Images[_type], __FILE__, __LINE__, __func__ , ##__VA_ARGS__); \
               UNLOCK_LOG();                                                                           \
    }                                                                                                  \
    else if (!SUPPRESS_LOG(type) && !DEBUG_LOG(type)) {                                                \
      LOCK_LOG();                                                                                      \
      LOG_FUNC(_type, stdout, W("[%llu] <%s>\n    %s(): ") msg W("\n"),                                \
               getTimestamp(), SA_LogMsg_Images[_type], __func__ , ##__VA_ARGS__);                     \
               UNLOCK_LOG();                                                                           \
    }                                                                                                  \
  })

#define _START_wlogMsg(type, msg, ...)                                                                 \
  ({                                                                                                   \
    SA_LogMsg_t _type = (type);                                                                        \
    int _display = SA_TRUE;                                                                            \
    if (!SUPPRESS_LOG(type) && DEBUG_LOG(type)) {                                                      \
      LOCK_LOG();                                                                                      \
      LOG_FUNC(_type, stdout, W("[%llu] <%s> at %s:%d\n    %s(): ") msg W("\n"),                       \
               getTimestamp(), SA_LogMsg_Images[_type], __FILE__, __LINE__, __func__ , ##__VA_ARGS__); \
    }                                                                                                  \
    else if (!SUPPRESS_LOG(type) && !DEBUG_LOG(type)) {                                                \
      LOCK_LOG();                                                                                      \
      LOG_FUNC(_type, stdout, W("[%llu] <%s>\n    %s(): ") msg W("\n"),                                \
               getTimestamp(), SA_LogMsg_Images[_type], __func__ , ##__VA_ARGS__);                     \
    }                                                                                                  \
    else {                                                                                             \
      _display = SA_FALSE;                                                                             \
    }                                                                                                  \
    if (_display) {

#define _END_wlogMsg() \
      UNLOCK_LOG();    \
    }                  \
  })

#define wlogAppend(msg, ...) \
      LOG_FUNC(_type, stdout, W("    ") msg W("\n"), ##__VA_ARGS__);

/* These are extremely imprecise at the microsecond level, but work well for long-term averages.
 * getTime() is far more precise but costs close to 1 microsecond to run, making it less ideal.
 */
#define PROFILE_START(id)                \
  static uint64_t _ ## id ## _total = 0; \
  static size_t   _ ## id ## _count = 0; \
  const  uint64_t _ ## id ## _start = getSysTime();

#define PROFILE_SAMPLE(id)                        \
  uint64_t _ ## id ## _sample = getSysTime();     \
           _ ## id ## _total += PROFILE_TIME(id); \
           _ ## id ## _count++;

#define PROFILE_TIME(id) \
  ( _ ## id ## _sample - _ ## id ## _start )

#define PROFILE_AVG(id) \
  ( (double) _ ## id ## _total / _ ## id ## _count )

#define PROFILE_PRINT(id, div)                    \
  if (0 == (_ ## id ## _count % (div))) {         \
    _START_logMsg(LOG_INFO, "%s stat:", #id);     \
    logAppend("time: %d μsec", PROFILE_TIME(id)); \
    logAppend("avg : %f μsec", PROFILE_AVG(id));  \
    _END_logMsg();                                \
  };

/* These are very precise at the microsecond level, and work well for events that don't occur that often.
 * getSysTime() is far less precise but costs virtually nothing, making it better for rapid events.
 */
#define PROFILE_START_PRECISE(id)              \
  static double _ ## id ## _total = 0.0;       \
  static size_t _ ## id ## _count = 0;         \
  const  double _ ## id ## _start = getTime();

#define PROFILE_SAMPLE_PRECISE(id)                      \
  double _ ## id ## _sample = getTime();                \
         _ ## id ## _total += PROFILE_TIME_PRECISE(id); \
         _ ## id ## _count++;

#define PROFILE_TIME_PRECISE(id) \
  ( _ ## id ## _sample - _ ## id ## _start )

#define PROFILE_AVG_PRECISE(id) \
  ( _ ## id ## _total / _ ## id ## _count )

#define PROFILE_PRINT_PRECISE(id, div)                                 \
  if (0 == (_ ## id ## _count % (div))) {                              \
    _START_logMsg(LOG_INFO, "%s stat:", #id);                          \
    logAppend("time: %f μsec", SEC_TO_USEC(PROFILE_TIME_PRECISE(id))); \
    logAppend("avg : %f μsec", SEC_TO_USEC(PROFILE_AVG_PRECISE(id)));  \
    _END_logMsg();                                                     \
  };

/**
 ** Global Constants
 **/

extern const char *SA_LogMsg_Images[];

/**
 ** Global Variables
 **/

extern uint64_t initSysTime;

extern wchar_t *baseDir;

extern pthread_spinlock_t logLock;
extern FILE *logFile;
extern int logFD;

extern Rand_t random;

/**
 ** Prototypes
 **/

extern int initCommon(
  void);

extern void closeCommon(
  void);

extern int removeLogFile(
  void);

/* Returns time in milliseconds since GLFW was initialized */
extern uint64_t getTimestamp(
  void);

/* Returns system time in microseconds */
extern uint64_t getSysTime(
  void);

/* Returns high-resolution time in seconds since GLFW was initialized */
extern double getTime(
  void);

/* Output must be freed; input list must have a NULL sentinel */
extern wchar_t* buildPath(
  wchar_t **pathList);

extern wchar_t* getPathTail(
  wchar_t *path);

extern int flushToilet(
  void);

extern int closeToilet(
  void);

/**
 ** Inline Functions
 **/

/* Assertive/forced memory allocation functions; if they fail, kill the program! */
static inline void* a_malloc(
  size_t size)
{
  void *ret = malloc(size);
  if (ret == NULL) {
    logMsg(LOG_FATAL, "Memory allocation of %'lld bytes failed!", (long long ) size);
    pthread_exit(D2V(SA_ERROR_MEMORY));
  }
  return ret;
}

static inline void* a_calloc(
  size_t count,
  size_t eltsize)
{
  void *ret = calloc(count, eltsize);
  if (ret == NULL) {
    logMsg(LOG_FATAL, "Memory allocation of %'lld x %'lld bytes failed!", (long long ) count, (long long ) eltsize);
    pthread_exit(D2V(SA_ERROR_MEMORY));
  }
  return ret;
}
static inline void* a_realloc(
  void *ptr,
  size_t newsize)
{
  void *ret = realloc(ptr, newsize);
  if (ret == NULL) {
    logMsg(LOG_FATAL, "Memory reallocation of %'lld bytes failed!", (long long ) newsize);
    pthread_exit(D2V(SA_ERROR_MEMORY));
  }
  return ret;
}

#endif /* ___COMMON_H___ */
