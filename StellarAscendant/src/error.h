/**********************************************************************************************************************
 * error.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   getError           Returns string representation of the given SA error
 *
 * Inline Functions:
 *   winError           Produces an error string for a Windows error type
 *
 * Macro Functions:
 *   genError           Packs a SA generic error
 *   IS_GENERIC_ERROR   Tests for a SA generic error
 *   GENERIC_ERROR      Unpacks a SA generic error
 *********************************************************************************************************************/

#ifndef ___ERROR_H___
#define ___ERROR_H___

#include <pthread.h>
#include <stdint.h>
#include <string.h>
#include <windows.h>

/**
 ** Typedefs and Definitions
 **/

#define SA_GENERIC_ERROR_SHIFT (24)
#define SA_GENERIC_ERROR_MASK  (0x00FFFFFF)

typedef enum
{
  /* Special Case */
  SA_ERROR_SUCCESS = 0,

  /* SA Errors */
  SA_ERROR_STANDARD = -1,
  SA_ERROR_UNKNOWN = -2,
  SA_ERROR_MEMORY = -3,
  SA_ERROR_SYNTAX = -4,
  SA_ERROR_FILE_INVALID = -5,
  SA_ERROR_FILE_UNSUPPORTED = -6,
  SA_ERROR_ARGS = -7,
  SA_ERROR_OP_INVALID = -8,
  SA_ERROR_OFFLINE = -9,
  SA_ERROR_FILE = -10,
  SA_ERROR_NOT_FOUND = -11,
  SA_ERROR_XML = -12,
  SA_ERROR_TYPE_MISMATCH = -13,
  SA_ERROR_INPUT = -14,

  _SA_ERROR1,
  LAST_SA_ERROR = _SA_ERROR1 - 1,
  NUM_SA_ERROR_TYPES = -_SA_ERROR1 + 2,

  /* Generic Errors (contains additional error info) */
  SA_GENERIC_ERROR_FILE = 1,
  SA_GENERIC_ERROR_THREAD = 2,
  SA_GENERIC_ERROR_XML_CREATE = 3,
  SA_GENERIC_ERROR_XML_PARSE = 4,
  SA_GENERIC_ERROR_XML_SAVE = 5,
  SA_GENERIC_ERROR_ENCODING = 6,
  SA_GENERIC_ERROR_MS_SYSTEM = 7,

  _SA_ERROR2,
  LAST_SA_GENERIC_ERROR = _SA_ERROR2 - 1
} SA_Error_t;

/**
 ** Macros
 **/

/* Expansion of wcserror for pthreads */
#define _wcserror(err) ( ((err) == ENOTSUP) ? L"Not supported" : _wcserror(err) )

/* Macro to create a generic error code with most of the bits reserved for non-SA error types */
#define genError(saErr, err) ( ((saErr) << SA_GENERIC_ERROR_SHIFT) | (err) )

/* Macro to extract the SA generic error type from a generic error */
#define IS_GENERIC_ERROR(err)                                                        \
  ({                                                                                 \
    __typeof__ (err) _err   = (err);                                                 \
    __typeof__ (err) _errSA = _err >> SA_GENERIC_ERROR_SHIFT;                        \
    (_err <= 0) ? 0 : (((_errSA == 0) || (_errSA > LAST_SA_GENERIC_ERROR)) ? 0 : 1); \
  })

/* Macro to extract the SA generic error type from a generic error */
#define GENERIC_ERROR(err) ( (err) >> SA_GENERIC_ERROR_SHIFT )

/**
 ** Prototypes
 **/

extern const wchar_t* getError(
  SA_Error_t err);

/**
 ** Inline Functions
 **/

/* Not thread-safe, since it uses a common buffer - should work fine when compiled for safe logs */
static inline wchar_t* winError(
  uint32_t err)
{
  static wchar_t buf[256];
  FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err,
      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buf, sizeof(buf) / sizeof(*(buf)), NULL);
  return buf;
}

#endif /* ___ERROR_H___ */
