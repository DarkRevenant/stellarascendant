/**********************************************************************************************************************
 * sched.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initScheduler           Initializes the SA thread scheduler
 *   closeScheduler          Terminates the SA thread scheduler
 *   setSchedulerMode        Changes the scheduler mode and returns the new mode
 *   queueTask               Schedules and ultimately runs the given task
 *   flushQueue              Sends queued tasks to run; necessary after calls to queueTask
 *   bindFuture              Blocks until the future's task is finished, and retrieves the result
 *   tryBindFuture           Retrieves the future's result, if available
 *********************************************************************************************************************/

#ifndef ___SCHED_H___
#define ___SCHED_H___

#include <pthread.h>
#include <stdint.h>

/**
 ** Typedefs and Definitions
 **/

typedef void* (*TaskFunc_t)(
  void *arg);

typedef void* Future_t;

typedef enum {
  TASK_LOWEST_PRIORITY = 0,

  /* Unless the game is in Loading Mode, at max half of the active worker threads may be working on low-priority
   * tasks.  Use this for things like file loading/processing or slow (longer than 1 frame) calculations.
   */
  TASK_LOW_PRIORITY = TASK_LOWEST_PRIORITY,

  /* This exists between low-priority and high-priority in the priority queue.  Medium priority is intended for tasks
   * that have loose timing restrictions, but ultimately still need to finish at an acceptable pace.
   */
  TASK_MEDIUM_PRIORITY,

  /* This is effectively the standard priority level.  Most tasks in Stellar Ascendant are important, as a real-time
   * action game has strict timing requirements not far off from a real-time operating system.  Note that this is not
   * the *highest* priority level, as that level has a specific intention.
   */
  TASK_HIGH_PRIORITY,

  /* This is for tasks that shall complete as soon as possible.  Even high-priority tasks will ultimately clog up the
   * queue when every worker thread is busy.  A time-critical task effectively circumvents the system; there is a
   * small pool of maximum-priority threads on top of the normal set, reserved for time-critical tasks, and spill-over
   * goes into the standard thread pool.  Overuse of this priority level can, in turn, block the time-critical threads
   * - so save it for occasions where the result must be obtained immediately.
   */
  TASK_TIME_CRITICAL,

  TASK_HIGHEST_PRIORITY = TASK_TIME_CRITICAL,

  NUM_TASK_PRIORITY_LEVELS
} TaskPriority_t;

typedef enum {
  MODE_OFFLINE,

  /* This is the starting mode.  Loading mode corresponds to non-real-time segments of play, such as the start and
   * shutdown sequences, loading screens, and menus.  There is no limit to TASK_LOW_PRIORITY tasks, unlike other
   * modes.
   */
  MODE_LOADING,

  /* This mode corresponds to gameplay.  Specifically, this mode corresponds to the situation where the gameplay state
   * is advancing, but nothing is being rendered.  The number of permitted active worker threads is reduced by N, to a
   * minimum of 2.
   */
  MODE_PLAYING_ADVANCE,

  /* This mode corresponds to gameplay.  Specifically, this mode corresponds to the situation where the graphics are
   * being rendered, but the gameplay loop is idle.  The number of permitted active worker threads is reduced by M, to
   * a minimum of 2.
   */
  MODE_PLAYING_DRAW,

  /* This mode corresponds to gameplay.  Specifically, this mode corresponds to the situation where the gameplay
   * state is advancing, and the graphics are being rendered, simultaneously.  The number of permitted active worker
   * threads is reduced by N + M, to a minimum of 2.
   */
  MODE_PLAYING_ADVANCE_DRAW,

  /* This mode corresponds to gameplay.  Specifically, this mode corresponds to the situation where the gameplay loop
   * is idle, nothing is being rendered, and the game is waiting to start the next frame.  All of the worker threads
   * are permitted for use.  There are two situations where this condition is useful.  First, if there is an abundance
   * of tasks queued between frames for some reason, then this can cause more of them to be processed in the
   * intervening time.
   */
  MODE_PLAYING_SYNC,

  NUM_SCHEDULER_MODES
} SchedulerMode_t;

/**
 ** Global Variables
 **/

/* Resolves to the workerID of the current thread (or -1 if not a worker thread) */
extern __thread int workerID;

extern int64_t workerCount;
extern int64_t criticalCount;
extern int64_t reservedThreads;
extern double lowPriRatio;

extern int64_t schedulerThreadPri;
extern int64_t workerThreadPri;
extern int64_t criticalThreadPri;
extern int useWorkerAffinity;

/**
 ** Prototypes
 **/

extern pthread_t* initScheduler(
  void);

/*  */
extern int closeScheduler(
  void);

extern SchedulerMode_t setSchedulerMode(
  SchedulerMode_t mode);

/* Recursive tasks are possible but not recommended, as this can cause an total lock-up if we try blocking for task
 * completion in a worker thread.
 * Remember that *arg shall not be freed by the caller before the task is completed.
 * May return NULL if the operation failed, which is generally the case if the scheduler is offline.
 */
extern Future_t* queueTask(
  TaskFunc_t task,
  void *arg,
  TaskPriority_t priority);

/* The queue typically doesn't automatically flush - call this when you want them to run! */
extern void flushQueue(
  void);

/* Frees the future object if SA_SUCCESS is returned */
extern int bindFuture(
  Future_t *future,
  void* *result);

/* Frees the future object if SA_SUCCESS is returned */
extern int tryBindFuture(
  Future_t *future,
  void* *result);

#endif /* ___SCHED_H___ */
