/**********************************************************************************************************************
 * control.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initControls                 Initializes the control subsystem
 *   closeControls                Terminates the control subsystem
 *   pollInputs                   Polls the current state of inputs, producing input events as necessary
 *   changeControlContext         Change the current control context
 *
 * Local Functions:
 *   keyCallbackGLFW              Callback hook for GLFW raw keyboard input
 *   charCallbackGLFW             Callback hook for GLFW unicode character input
 *   cursorPosCallbackGLFW        Callback hook for GLFW mouse movement input
 *   cursorEnterCallbackGLFW      Callback hook for GLFW mouse entering or exiting the window
 *   mouseButtonCallbackGLFW      Callback hook for GLFW mouse button input
 *   scrollCallbackGLFW           Callback hook for GLFW mouse scrolling input
 *   joystickCallbackGLFW         Callback hook for GLFW controller connection or disconnection
 *********************************************************************************************************************/

#include <pthread.h>

/** SA Headers **/
#include "control.h"
#include "common.h"
#include "keymap.h"
#include "display/display.h"

/** Utility Headers **/
#include <iconv.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 ** Definitions
 **/

#define GLFW_CHAR_ENCODING    "UTF-32LE"
#define INPUT_CHAR_BUFFER_LEN (5)

/**
 ** Global Variables
 **/

ControlContext_t currentControlContext = CONTROL_CONTEXT_MENU;
ControllerType_t activeControllerType = CONTROLLER_KEYBOARD_MOUSE;

/* -1 with no controller plugged in */
int activeControllerIndex = -1;

/**
 ** Local Variables
 **/
static iconv_t glfw2wchar = 0;

static pthread_mutex_t controlLock = (pthread_mutex_t) 0;

static int numControllerAxes = 0;
static int numControllerButtons = 0;

/**
 ** Prototypes
 **/

static void keyCallbackGLFW(
  GLFWwindow *window,
  int key,
  int scancode,
  int action,
  int mods);

static void charCallbackGLFW(
  GLFWwindow *window,
  unsigned int codepoint);

static void cursorPosCallbackGLFW(
  GLFWwindow *window,
  double xpos,
  double ypos);

static void cursorEnterCallbackGLFW(
  GLFWwindow *window,
  int entered);

static void mouseButtonCallbackGLFW(
  GLFWwindow *window,
  int button,
  int action,
  int mods);

static void scrollCallbackGLFW(
  GLFWwindow *window,
  double xoffset,
  double yoffset);

static void joystickCallbackGLFW(
  int joy,
  int event);

/**
 ** Global Functions
 **/

int initControls(
  GLFWwindow *window)
{
  int err;

  /* Determine what our primary controller is */
  /* TODO: Make a more sophisticated, graphical method of choosing controller */
  int stick = -1;
  for (int i = GLFW_JOYSTICK_1; i <= GLFW_JOYSTICK_LAST; i++) {
    if (glfwJoystickPresent(i)) {
      stick = i;
      break;
    }
  }
  activeControllerIndex = stick;

  /* Determine what the default assumptive control type is */
  if (activeControllerIndex == -1)
    activeControllerType = CONTROLLER_KEYBOARD_MOUSE;
  else
    activeControllerType = CONTROLLER_GAMEPAD;

  numControllerAxes = 0;
  numControllerButtons = 0;

  /* Bind key mappings */
  err = bindKeymap();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to bind key mappings: (%d) %ls", err, getError(err));
    return err;
  }

  /* Set input callbacks */
  glfwSetKeyCallback(window, keyCallbackGLFW);
  glfwSetCharCallback(window, charCallbackGLFW);
  glfwSetCursorPosCallback(window, cursorPosCallbackGLFW);
  glfwSetCursorEnterCallback(window, cursorEnterCallbackGLFW);
  glfwSetMouseButtonCallback(window, mouseButtonCallbackGLFW);
  glfwSetScrollCallback(window, scrollCallbackGLFW);
  glfwSetJoystickCallback(joystickCallbackGLFW);

  /* Create encoding conversion handle */
  glfw2wchar = iconv_open("WCHAR_T", GLFW_CHAR_ENCODING);
  if ((iconv_t) -1 == glfw2wchar) {
    logMsg(LOG_WARNING, "Failed to create GLFW encoding conversion handle: (%d) %ls",
        errno, _wcserror(errno));
    return genError(SA_GENERIC_ERROR_ENCODING, errno);
  }

  /* Create mutex object to synchronize the control subsystem */
  err = pthread_mutex_init(&controlLock, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create mutex: (%d) %ls", err, _wcserror(err));
    return genError(SA_GENERIC_ERROR_THREAD, err);
  }

  return SA_SUCCESS;
}

int closeControls(
  void)
{
  /* Destroy encoding conversion handle */
  iconv_close(glfw2wchar);
  glfw2wchar = 0;

  /* Unbind key mappings */
  freeKeymap();

  /* Destroy synchronization mutex */
  if (controlLock)
    pthread_mutex_destroy(&controlLock);
  controlLock = (pthread_mutex_t) 0;

  return SA_SUCCESS;
}

int pollInputs(
  void)
{
  int rtnVal = SA_SUCCESS;
  int count;

  if (!controlLock)
    return SA_ERROR_OFFLINE;

  pthread_mutex_lock(&controlLock);

  /* Handle standard input events first, before continuing on */
  glfwPollEvents();

  /* TODO: Make yet another fucking thread that polls controller inputs asynchronously and sticks them in a queue */
  /* No controller plugged in; exit */
  if (activeControllerIndex == -1) {
    rtnVal = SA_SUCCESS;
    goto exit;
  }

  /* Grab the controller axis values */
  const float *axes = glfwGetJoystickAxes(activeControllerIndex, &count);
  if (!axes) {
    logMsg(LOG_WARNING, "Active controller is not responding; resetting configuration");
    rtnVal = SA_ERROR_INPUT;
    goto reset_config;
  }
  else if (count != numControllerAxes) {
    logMsg(LOG_WARNING, "Active controller configuration is invalid; resetting configuration");
    rtnVal = SA_ERROR_INPUT;
    goto reset_config;
  }

  if (0 == numControllerAxes) {
    /* Initialize the controller axis binding table */
    numControllerAxes = count;
    initControllerAxes(numControllerAxes);
  }

  /* Grab the controller button values */
  const unsigned char *buttons = glfwGetJoystickButtons(activeControllerIndex, &count);
  if (!buttons) {
    logMsg(LOG_WARNING, "Active controller is not responding; resetting configuration");
    rtnVal = SA_ERROR_INPUT;
    goto reset_config;
  }
  else if (count != numControllerButtons) {
    logMsg(LOG_WARNING, "Active controller configuration is invalid; resetting configuration");
    rtnVal = SA_ERROR_INPUT;
    goto reset_config;
  }

  if (0 == numControllerButtons) {
    /* Initialize the controller button binding table */
    numControllerButtons = count;
    initControllerButtons(numControllerButtons);
  }

  /* Process axis-type inputs */
  for (size_t i = 0; i < numControllerAxes; i++) {
    double position = (double) axes[i];
  }

  /* Process button-type inputs */
  for (size_t i = 0; i < numControllerButtons; i++) {
    int state = (int) buttons[i];
  }

  rtnVal = SA_SUCCESS;
  goto exit;

  reset_config:
  {
    int stick = -1;
    for (int i = GLFW_JOYSTICK_1; i <= GLFW_JOYSTICK_LAST; i++) {
      if (glfwJoystickPresent(i)) {
        stick = i;
        break;
      }
    }
    activeControllerIndex = stick;

    /* Determine what the default assumptive control type is */
    if (activeControllerIndex == -1)
      activeControllerType = CONTROLLER_KEYBOARD_MOUSE;
    else
      activeControllerType = CONTROLLER_GAMEPAD;

    numControllerAxes = 0;
    numControllerButtons = 0;
  }

  exit:
  pthread_mutex_unlock(&controlLock);
  return rtnVal;
}

int changeControlContext(
  ControlContext_t context)
{
  if (!controlLock)
    return SA_ERROR_OFFLINE;

  pthread_mutex_lock(&controlLock);
  currentControlContext = context;
  pthread_mutex_unlock(&controlLock);

  return SA_SUCCESS;
}

/**
 ** Local Functions
 **/

static void keyCallbackGLFW(
  GLFWwindow *window,
  int key,
  int scancode,
  int action,
  int mods)
{
  if (GLFW_KEY_UNKNOWN == key) {
    return;
  }
}

static void charCallbackGLFW(
  GLFWwindow *window,
  unsigned int codepoint)
{
  union utf32 {
    unsigned int code;
    char buf[4];
  };

  wchar_t buf[INPUT_CHAR_BUFFER_LEN];
  union utf32 glfwChar;
  glfwChar.code = codepoint;

  /* Reset converter */
  iconv(glfw2wchar, NULL, NULL, NULL, NULL);

  /* Convert from UTF-32 little-endian to whatever wchar_t is on this platform */
  size_t inLen = sizeof(glfwChar.buf);
  size_t outLen = INPUT_CHAR_BUFFER_LEN * sizeof(wchar_t);
  char *inPtr = glfwChar.buf;
  char *outPtr = (char*) buf;
  size_t result = iconv(glfw2wchar, &inPtr, &inLen, &outPtr, &outLen);
  if (0 > result) {
    logMsg(LOG_WARNING, "Failed to convert codepoint %u to usable character: (%d) %ls",
        codepoint, errno, _wcserror(errno));
    return;
  }

  /* Add the null term */
  *((wchar_t*) outPtr) = W('\0');
}

static void cursorPosCallbackGLFW(
  GLFWwindow *window,
  double xpos,
  double ypos)
{
  logMsg(LOG_INFO, "%.2f: %.1f x %.1f",
      currInputFrame, xpos, ypos);
}

static void cursorEnterCallbackGLFW(
  GLFWwindow *window,
  int entered)
{
  if (entered) {

  }
  else {

  }
}

static void mouseButtonCallbackGLFW(
  GLFWwindow *window,
  int button,
  int action,
  int mods)
{
}

static void scrollCallbackGLFW(
  GLFWwindow *window,
  double xoffset,
  double yoffset)
{
}

static void joystickCallbackGLFW(
  int joy,
  int event)
{
  /* TODO: Create settings to override this behavior, in case the user has some unique environment */
  if (event == GLFW_CONNECTED) {
    if (joy == -1) {
      activeControllerType = CONTROLLER_GAMEPAD;
      activeControllerIndex = joy;
    }
  }
  else if (event == GLFW_DISCONNECTED) {
    if (activeControllerIndex == joy) {
      int stick = -1;
      for (int i = GLFW_JOYSTICK_1; i <= GLFW_JOYSTICK_LAST; i++) {
        if ((i != joy) && glfwJoystickPresent(i)) {
          stick = i;
          break;
        }
      }

      activeControllerIndex = stick;
      if (activeControllerIndex == -1)
        activeControllerType = CONTROLLER_KEYBOARD_MOUSE;

      numControllerAxes = 0;
      numControllerButtons = 0;
    }
  }
}
