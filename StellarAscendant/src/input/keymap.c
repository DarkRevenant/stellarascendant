/**********************************************************************************************************************
 * keymap.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   bindKeymap              Loads the key configuration and binds controls to actions
 *   freeKeymap              Closes and frees the key configuration
 *   initControllerAxes      Registers controller axis inputs and creates binding table
 *   initControllerButtons   Registers controller button inputs and creates binding table
 *   keyInput                Processes a keyboard input with the control mapping system
 *   mouseCursorInput        Processes a mouse cursor input with the control mapping system
 *   mouseButtonInput        Processes a mouse button input with the control mapping system
 *   scrollEventInput        Processes a scroll wheel event input with the control mapping system
 *   scrollAxisInput         Processes a scroll wheel axis input with the control mapping system
 *   controllerAxisInput     Processes a controller axis input with the control mapping system
 *   controllerButtonInput   Processes a controller button input with the control mapping system
 *
 * Local Functions:
 *   setKeyNames             Fills the relevant arrays with known names for keys and buttons
 *   refreshKeymap           Parse and save the key configuration file
 *   setDefaults             Set the default key bindings
 *   initKeys                Creates binding table for keyboard inputs
 *   initMousePointer        Creates binding table for mouse pointer inputs
 *   initMouseButtons        Creates binding table for mouse button inputs
 *   initScrollEvents        Creates binding table for scroll event inputs
 *   initScrollAxes          Creates binding table for scroll axis inputs
 *   checkCombo              Checks if an action's combo components are currently fulfilled
 *   getBindingName          Gets the proper name of the given binding
 *   getBindingFromString    Converts a string into an internal binding handle
 *   getInputTypeFromString  Converts a string into an InputType enumeration
 *   isInputTypeValid        Checks if an input type is valid for a given action usage
 *********************************************************************************************************************/

#include <assert.h>
#include <float.h>
#include <math.h>

/** SA Headers **/
#include "keymap.h"
#include "control.h"
#include "common.h"

/** Utility Headers **/
#include <iconv.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 ** Definitions and Typedefs
 **/

#define KEY_CONFIG_NAME      "Controls"
#define KEY_CONFIG_FILE_NAME W("keymap.xml")

#define BINDING_PARAM_NAME          "map"
#define INPUT_TYPE_PARAM_NAME       "type"
#define COMBO_PARAM_PREFIX          "combo"
#define COMBO_BINDING_PARAM_NAME    "Map"
#define COMBO_INPUT_TYPE_PARAM_NAME "Type"

#define MOD_KEY_SHIFT   W("Shift")
#define MOD_KEY_CONTROL W("Control")
#define MOD_KEY_ALT     W("Alt")
#define MOD_KEY_SUPER   W("Super")
#define MOD_SEP         W("+")

#define NUM_MOD_COMBOS ((GLFW_MOD_SHIFT | GLFW_MOD_CONTROL | GLFW_MOD_ALT | GLFW_MOD_SUPER) + 1)

#define MAX_ACTIONS_PER_BIND (10)
#define MAX_COMBO_DEPTH      (3)

#define MOUSE_CURSOR_NAME        W("Cursor")
#define CONTROLLER_AXIS_PREFIX   W("Axis ")
#define CONTROLLER_BUTTON_PREFIX W("Button ")

#define NO_BIND W("none")

#define IS_CURRENT_CONTEXT(context)                                                                      \
  ( ( ((context) == CONTROL_CONTEXT_ALL)                                                          ||     \
     (((context) == CONTROL_CONTEXT_MENU)     && (currentControlContext == CONTROL_CONTEXT_MENU)) ||     \
     (((context) == CONTROL_CONTEXT_GAMEPLAY) && (currentControlContext == CONTROL_CONTEXT_GAMEPLAY))) ? \
       SA_TRUE : SA_FALSE                                                                                \
  )

#define KEYMAP_CONFIG_BUF_LEN (128)

typedef enum {
  CONTROLLER_CONFIG_KEYBOARD_MOUSE,
  CONTROLLER_CONFIG_XBOX_360,
  CONTROLLER_CONFIG_PS4,

  NUM_CONTROLLER_CONFIGURATIONS
} ControllerConfiguration_t;

typedef enum {
  SET_PRIMARY,
  SET_SECONDARY,
  SET_TERTIARY,

  NUM_BINDING_SETS
} BindingSet_t;

typedef enum {
  INPUT_TYPE_NONE,
  INPUT_TYPE_KEYBOARD,
  INPUT_TYPE_KEYBOARD_MOD,
  INPUT_TYPE_MOUSE_CURSOR,
  INPUT_TYPE_MOUSE_BUTTON,
  INPUT_TYPE_MOUSE_SCROLL,
  INPUT_TYPE_MOUSE_SCROLL_AXIS,
  INPUT_TYPE_CONTROLLER_AXIS,
  INPUT_TYPE_CONTROLLER_BUTTON,

  NUM_INPUT_TYPES
} InputType_t;

typedef struct {
  unsigned long bind;
  InputType_t input;
  double activePosition;
  int activeState;
} ActionInfo_t;

typedef struct {
  const SA_Action_t actionType;
  const ActionUsage_t usage;
  const ControlContext_t context;

  ActionInfo_t info;
  size_t comboDepth;
  ActionInfo_t combo[MAX_COMBO_DEPTH];
} ActionDef_t;

typedef struct {
  double position;
  int state;
  int mods;
  double x;
  double y;

  size_t count;
  ActionDef_t *actions[MAX_ACTIONS_PER_BIND];
} ControlActions_t;

typedef struct {
  double livezone; /* For edge-triggering */
  double midpoint;
  double deadzone; /* Applied after midpoint */
  double gain; /* Applied after deadzone */

  /* Special helper for edge-triggered events that are supposed to land in a diagonal position with two axes */
  /* Used for analog sticks, analog d-pads, etc. */
  long radialPair;
} ControlProps_t;

/**
 ** Constants
 **/

static const char *ControllerType_Images[NUM_CONTROLLER_TYPES] = {
  "keyboard", //* CONTROLLER_KEYBOARD_MOUSE *//
  "gamepad",  //* CONTROLLER_GAMEPAD        *//
    };

static const char *BindingSet_Images[NUM_BINDING_SETS] = {
  "",  //* SET_PRIMARY   *//
  "2", //* SET_SECONDARY *//
  "3", //* SET_TERTIARY  *//
    };

static const wchar_t *InputType_Images[NUM_INPUT_TYPES] = {
  W("none"),           //* INPUT_TYPE_NONE              *//
  W("Key"),            //* INPUT_TYPE_KEYBOARD          *//
  W("Modifier"),       //* INPUT_TYPE_KEYBOARD_MOD      *//
  W("Mouse Cursor"),   //* INPUT_TYPE_MOUSE_CURSOR      *//
  W("Mouse Button"),   //* INPUT_TYPE_MOUSE_BUTTON      *//
  W("Scroll"),         //* INPUT_TYPE_MOUSE_SCROLL      *//
  W("Scroll Axis"),    //* INPUT_TYPE_MOUSE_SCROLL_AXIS *//
  W("Gamepad Axis"),   //* INPUT_TYPE_CONTROLLER_AXIS   *//
  W("Gamepad Button"), //* INPUT_TYPE_CONTROLLER_BUTTON *//
    };

static const wchar_t *mouseNames[GLFW_MOUSE_BUTTON_LAST + 1] = {
  W("Left Click"),     //* GLFW_MOUSE_BUTTON_1 *//
  W("Right Click"),    //* GLFW_MOUSE_BUTTON_2 *//
  W("Middle Click"),   //* GLFW_MOUSE_BUTTON_3 *//
  W("Mouse Button 4"), //* GLFW_MOUSE_BUTTON_4 *//
  W("Mouse Button 5"), //* GLFW_MOUSE_BUTTON_5 *//
  W("Mouse Button 6"), //* GLFW_MOUSE_BUTTON_6 *//
  W("Mouse Button 7"), //* GLFW_MOUSE_BUTTON_7 *//
  W("Mouse Button 8"), //* GLFW_MOUSE_BUTTON_8 *//
    };

static const wchar_t *ScrollEvent_Images[NUM_SCROLL_EVENTS] = {
  W("Scroll Right"), //* SCROLL_RIGHT *//
  W("Scroll Left"),  //* SCROLL_LEFT  *//
  W("Scroll Down"),  //* SCROLL_DOWN  *//
  W("Scroll Up"),    //* SCROLL_UP    *//
    };

static const wchar_t *ScrollAxis_Images[NUM_SCROLL_AXES] = {
  W("Vertical Scroll"),   //* SCROLL_AXIS_VERTICAL   *//
  W("Horizontal Scroll"), //* SCROLL_AXIS_HORIZONTAL *//
    };

#define ACTION_DEF_START() \
  static const ActionDef_t actionTableTemplate[NUM_SA_ACTIONS] = {
#define ACTION_DEF(X, type, context, name) \
    { SA_ACT_ ## X , ACTION_USAGE_ ## type, CONTROL_CONTEXT_ ## context },
#define ACTION_DEF_LAST(X, type, context, name) \
    { SA_ACT_ ## X , ACTION_USAGE_ ## type, CONTROL_CONTEXT_ ## context }
#define ACTION_DEF_END() \
  };

#include "actions.inc" /* actionTableTemplate[NUM_SA_ACTIONS] */

/**
 ** Local Variables
 **/

#define ACTION_DEF_START() \
  static const char *SA_Action_Images[NUM_SA_ACTIONS] = {
#define ACTION_DEF(X, type, context, name) \
    STRINGIFY(X),
#define ACTION_DEF_END() \
  };

#include "actions.inc" /* SA_Action_Images[NUM_SA_ACTIONS] */

#define ACTION_DEF_START() \
  static const wchar_t *actionNames[NUM_SA_ACTIONS] = {
#define ACTION_DEF(X, type, context, name) \
    W(name),
#define ACTION_DEF_END() \
  };

#include "actions.inc" /* actionNames[NUM_SA_ACTIONS] */

static ActionDef_t actionTable[NUM_CONTROLLER_TYPES][NUM_BINDING_SETS][NUM_SA_ACTIONS];

static SA_Config_t keyConfig = { KEY_CONFIG_NAME };

static ControlActions_t keyTable[GLFW_KEY_LAST + 1];
static ControlActions_t mousePointerTable[1];
static ControlActions_t mouseButtonTable[GLFW_MOUSE_BUTTON_LAST + 1];
static ControlActions_t scrollEventTable[NUM_SCROLL_EVENTS];
static ControlActions_t scrollAxisTable[NUM_SCROLL_AXES];
static ControlActions_t *controllerAxisTable = NULL;
static ControlActions_t *controllerButtonTable = NULL;

static ControlProps_t scrollAxisProps[NUM_SCROLL_AXES];
static ControlProps_t *controllerAxisProps = NULL;

static wchar_t *keyNames[GLFW_KEY_LAST + 1];
static wchar_t *modNames[NUM_MOD_COMBOS];

static int numControllerAxes = 0;
static int numControllerButtons = 0;

/**
 ** Prototypes
 **/

static int setKeyNames(
  void);

static int refreshKeymap(
  void);

static void setDefaults(
  ControllerType_t type,
  ControllerConfiguration_t config);

static int initKeys(
  void);

static int initMousePointer(
  void);

static int initMouseButtons(
  void);

static int initScrollEvents(
  void);

static int initScrollAxes(
  void);

static int checkCombo(
  const ActionDef_t *action);

static const wchar_t* getBindingName(
  const ActionInfo_t *info,
  wchar_t *scratchBuffer);

static int getBindingFromString(
  InputType_t inputType,
  wchar_t *bindStr,
  unsigned long *bind);

static InputType_t getInputTypeFromString(
  const wchar_t *inputTypeStr);

static int isInputTypeValid(
  ActionUsage_t usage,
  InputType_t inputType);

/**
 ** Global Functions
 **/

int bindKeymap(
  void)
{
  int err;

  /* Clear tables */
  memset(scrollAxisProps, 0, sizeof(scrollAxisProps));
  memset(keyTable, 0, sizeof(keyTable));
  memset(mousePointerTable, 0, sizeof(mousePointerTable));
  memset(mouseButtonTable, 0, sizeof(mouseButtonTable));
  memset(scrollEventTable, 0, sizeof(scrollEventTable));
  memset(scrollAxisTable, 0, sizeof(scrollAxisTable));
  memset(actionTable, 0, sizeof(actionTable));
  for (ControllerType_t type = 0; type < NUM_CONTROLLER_TYPES; type++) {
    for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
      memcpy(actionTable[type][set], actionTableTemplate, sizeof(actionTableTemplate));

      for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
        memset(&(actionTable[type][set][a].info), 0, sizeof(actionTable[type][set][a].info));
        memset(&(actionTable[type][set][a].combo), 0, sizeof(actionTable[type][set][a].combo));
        actionTable[type][set][a].comboDepth = 0;
      }
    }
  }

  /* Determine the names of the keys */
  setKeyNames();

  /* Get the actual key mapping configuration */
  keyConfig.path = BUILD_PATH(baseDir, SA_CONFIG_DIR_PATH, KEY_CONFIG_FILE_NAME);
  err = refreshKeymap();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to read key mappings: (%d) %ls", err, getError(err));
    return err;
  }

  /* Initialize input binding tables */
  err = initKeys();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to bind mappings for keyboard: (%d) %ls", err, getError(err));
    return err;
  }
  err = initMousePointer();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to bind mappings for mouse pointer: (%d) %ls", err, getError(err));
    return err;
  }
  err = initMouseButtons();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to bind mappings for mouse buttons: (%d) %ls", err, getError(err));
    return err;
  }
  err = initScrollEvents();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to bind mappings for scroll events: (%d) %ls", err, getError(err));
    return err;
  }
  err = initScrollAxes();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to bind mappings for scroll axes: (%d) %ls", err, getError(err));
    return err;
  }

  return SA_SUCCESS;
}

int freeKeymap(
  void)
{
  /* Free all the names we allocated earlier - this is a roundabout but consistent method of determining which ones
   * to free.
   */
  for (size_t i = 0; i <= GLFW_KEY_LAST; i++) {
    if (!keyNames[i])
      continue;

    const char *name = glfwGetKeyName(i, 0);
    if (name)
      free(keyNames[i]);
    keyNames[i] = NULL;
  }

  /* Free the dynamically-allocated controller tables */
  if (controllerAxisTable)
    free(controllerAxisTable);
  if (controllerButtonTable)
    free(controllerButtonTable);
  if (controllerAxisProps)
    free(controllerAxisProps);
  controllerAxisTable = NULL;
  controllerButtonTable = NULL;
  controllerAxisProps = NULL;

  return SA_SUCCESS;
}

int execKeymap(
  void)
{
  /* Clear 1-frame scroll events */
  for (size_t i = 0; i < ARRAY_LEN(scrollEventTable); i++) {
    ControlActions_t *scrollEventActions = &(scrollEventTable[i]);
    scrollEventActions->state = 0;
  }

  return SA_SUCCESS;
}

int initControllerAxes(
  int count)
{
  if (controllerAxisTable)
    free(controllerAxisTable);
  if (controllerAxisProps)
    free(controllerAxisProps);
  controllerAxisTable = NULL;
  controllerAxisProps = NULL;

  numControllerAxes = count;
  controllerAxisTable = a_calloc(numControllerAxes, sizeof(ControlActions_t));

  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_GAMEPAD][set][a]);

      if (action->info.input == INPUT_TYPE_CONTROLLER_AXIS) {
        if ((action->info.bind >= 0) && (action->info.bind < numControllerAxes)) {
          ControlActions_t *ctlActions = &(controllerAxisTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to controller %ls %d",
                MAX_ACTIONS_PER_BIND, CONTROLLER_AXIS_PREFIX, action->info.bind);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;

          /* TODO: Add a way to configure this */
          action->info.activePosition = 1.0;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_CONTROLLER_AXIS) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < numControllerAxes)) {
            ControlActions_t *ctlActions = &(controllerAxisTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to controller %ls %d",
                  MAX_ACTIONS_PER_BIND, CONTROLLER_AXIS_PREFIX, action->info.bind);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;

            /* TODO: Add a way to configure this */
            action->combo[i].activePosition = 1.0;
          }
        }
      }
    }
  }

  /* TODO: Add a way to configure this */
  controllerAxisProps = a_calloc(numControllerAxes, sizeof(ControlProps_t));
  for (size_t i = 0; i < numControllerAxes; i++) {
    controllerAxisProps[i].deadzone = 0.2;
    controllerAxisProps[i].midpoint = 0.0;
    controllerAxisProps[i].gain = 1.0;
    controllerAxisProps[i].livezone = 0.1;
    controllerAxisProps[i].radialPair = -1;
  }

  return SA_SUCCESS;
}

int initControllerButtons(
  int count)
{
  if (controllerButtonTable)
    free(controllerButtonTable);
  controllerButtonTable = NULL;

  numControllerButtons = count;
  controllerButtonTable = a_calloc(numControllerButtons, sizeof(ControlActions_t));

  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_GAMEPAD][set][a]);

      if (action->info.input == INPUT_TYPE_CONTROLLER_BUTTON) {
        if ((action->info.bind >= 0) && (action->info.bind < numControllerButtons)) {
          ControlActions_t *ctlActions = &(controllerButtonTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to controller %ls %d",
                MAX_ACTIONS_PER_BIND, CONTROLLER_BUTTON_PREFIX, action->info.bind);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;

          /* TODO: Add a way to configure this */
          action->info.activeState = 1;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_CONTROLLER_BUTTON) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < numControllerButtons)) {
            ControlActions_t *ctlActions = &(controllerButtonTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to controller %ls %d",
                  MAX_ACTIONS_PER_BIND, CONTROLLER_BUTTON_PREFIX, action->info.bind);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;

            /* TODO: Add a way to configure this */
            action->combo[i].activeState = 1;
          }
        }
      }
    }
  }

  return SA_SUCCESS;
}

/* Returned array should not be freed */
SA_ActionEvent_t** keyInput(
  unsigned long key,
  int state,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;
  if (key >= ARRAY_LEN(keyTable))
    return NULL;

  ControlActions_t *ctlActions = &(keyTable[key]);
  if (0 == ctlActions->count)
    return NULL;

  /* Adjust raw state to commanded state; currently, they are the same */
  int cmdState = state;

  /* If the button state hasn't changed, skip it */
  if (cmdState == ctlActions->state)
    return rtnBuf;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    ActionInfo_t *info = &(action->info);
    switch (action->usage) {
      case ACTION_USAGE_BUTTON_PRESS:
      case ACTION_USAGE_BUTTON_RELEASE: {
        int state;
        if (ctlActions->state != info->activeState) {
          /* Positive edge */
          state = 1;
          if (cmdState != info->activeState)
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }
        else {
          /* Negative edge */
          state = 0;
          if (cmdState == info->activeState)
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }

        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->state = state;
        rtnIndex++;
        break;
      }

      case ACTION_USAGE_AXIS: {
        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->position = (double) state;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls",
            actionNames[action->actionType], action->usage, keyNames[action->actionType]);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->state = cmdState;

  *count = rtnIndex;
  return rtnBuf;
}

SA_ActionEvent_t** mouseCursorInput(
  double x,
  double y,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;

  ControlActions_t *ctlActions = &(mousePointerTable[0]);
  if (0 == ctlActions->count)
    return NULL;

  /* Adjust raw position to commanded position; currently, they are the same */
  double cmdX = x;
  double cmdY = y;

  /* If the position hasn't changed (somehow), skip it */
  if ((cmdX == ctlActions->x) && (cmdY == ctlActions->y))
    return rtnBuf;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    switch (action->usage) {
      case ACTION_USAGE_POINTER: {
        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->x = cmdX;
        rtnBuf[rtnIndex]->y = cmdY;
        rtnIndex++;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls",
            actionNames[action->actionType], action->usage, MOUSE_CURSOR_NAME);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->x = cmdX;
  ctlActions->y = cmdY;

  *count = rtnIndex;
  return rtnBuf;
}

/* Returned array should not be freed */
SA_ActionEvent_t** mouseButtonInput(
  unsigned long button,
  int state,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;
  if (button >= ARRAY_LEN(mouseButtonTable))
    return NULL;

  ControlActions_t *ctlActions = &(mouseButtonTable[button]);
  if (0 == ctlActions->count)
    return NULL;

  /* Adjust raw state to commanded state; currently, they are the same */
  int cmdState = state;

  /* If the button state hasn't changed, skip it */
  if (cmdState == ctlActions->state)
    return rtnBuf;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    ActionInfo_t *info = &(action->info);
    switch (action->usage) {
      case ACTION_USAGE_BUTTON_PRESS:
      case ACTION_USAGE_BUTTON_RELEASE: {
        int state;
        if (ctlActions->state != info->activeState) {
          /* Positive edge */
          state = 1;
          if (cmdState != info->activeState)
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }
        else {
          /* Negative edge */
          state = 0;
          if (cmdState == info->activeState)
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }

        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->state = state;
        rtnIndex++;
        break;
      }

      case ACTION_USAGE_AXIS: {
        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->position = (double) state;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls",
            actionNames[action->actionType], action->usage, mouseNames[action->actionType]);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->state = cmdState;

  *count = rtnIndex;
  return rtnBuf;
}

/* Returned array should not be freed */
SA_ActionEvent_t** scrollEventInput(
  ScrollEvent_t event,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;
  if ((event >= NUM_SCROLL_EVENTS) || (event < 0))
    return NULL;

  ControlActions_t *ctlActions = &(scrollEventTable[event]);
  if (0 == ctlActions->count)
    return NULL;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    /* It's physically impossible to "hold down" a scroll axis like you would a button or trigger.  That's why we
     * need this janky bullshit.
     */
    switch (action->usage) {
      case ACTION_USAGE_BUTTON_PRESS: {
        if (!checkCombo(action))
          goto next_action;

        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->state = 1;
        rtnIndex++;
        break;
      }

      case ACTION_USAGE_BUTTON_RELEASE: {
        if (!checkCombo(action))
          goto next_action;

        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->state = 0;
        rtnIndex++;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls",
            actionNames[action->actionType], action->usage, ScrollEvent_Images[event]);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->position = 1;

  *count = rtnIndex;
  return rtnBuf;
}

/* Returned array should not be freed */
SA_ActionEvent_t** scrollAxisInput(
  ScrollAxis_t axis,
  double position,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;
  if ((axis >= NUM_SCROLL_AXES) || (axis < 0))
    return NULL;

  ControlActions_t *ctlActions = &(scrollAxisTable[axis]);
  if (0 == ctlActions->count)
    return NULL;

  /* Adjust raw position to commanded position */
  ControlProps_t *ctlProps = &(scrollAxisProps[axis]);
  double cmdPosition = position - ctlProps->midpoint;
  if (WITHIN_EPSILON(cmdPosition, 0.0, ctlProps->deadzone)) {
    cmdPosition = 0.0;
  }
  else {
    cmdPosition -= copysign(ctlProps->deadzone, cmdPosition);
    cmdPosition *= ctlProps->gain / (1.0 - ctlProps->deadzone);
    cmdPosition = fmax(-1.0, fmin(1.0, cmdPosition));
  }

  /* On the off-chance the axis position hasn't changed at all, skip it */
  if (cmdPosition == ctlActions->position)
    return rtnBuf;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    switch (action->usage) {
      case ACTION_USAGE_AXIS: {
        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->position = cmdPosition;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls",
            actionNames[action->actionType], action->usage, ScrollAxis_Images[axis]);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->position = cmdPosition;

  *count = rtnIndex;
  return rtnBuf;
}

/* Returned array should not be freed */
SA_ActionEvent_t** controllerAxisInput(
  unsigned long binding,
  double position,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;
  if (!controllerAxisTable || !controllerAxisProps || (binding >= numControllerAxes))
    return NULL;

  ControlActions_t *ctlActions = &(controllerAxisTable[binding]);
  if (0 == ctlActions->count)
    return NULL;

  /* Adjust raw position to commanded position */
  ControlProps_t *ctlProps = &(controllerAxisProps[binding]);
  double cmdPosition = position - ctlProps->midpoint;
  if (WITHIN_EPSILON(cmdPosition, 0.0, ctlProps->deadzone)) {
    cmdPosition = 0.0;
  }
  else {
    cmdPosition -= copysign(ctlProps->deadzone, cmdPosition);
    cmdPosition *= ctlProps->gain / (1.0 - ctlProps->deadzone);
    cmdPosition = fmax(-1.0, fmin(1.0, cmdPosition));
  }

  /* On the off-chance the axis position hasn't changed at all, skip it */
  if (cmdPosition == ctlActions->position)
    return rtnBuf;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    ActionInfo_t *info = &(action->info);
    switch (action->usage) {
      case ACTION_USAGE_BUTTON_PRESS:
      case ACTION_USAGE_BUTTON_RELEASE: {
        int radialCombo = SA_FALSE;
        if (ctlProps->radialPair >= 0) {
          for (size_t j = 0; j < action->comboDepth; j++) {
            const ActionInfo_t *comboInfo = &(action->combo[j]);
            switch (comboInfo->input) {
              case INPUT_TYPE_CONTROLLER_AXIS: {
                if ((unsigned long) ctlProps->radialPair == comboInfo->bind) {
                  radialCombo = SA_TRUE;
                  goto exitRadialCheck;
                }
                break;
              }

              default: {
                break;
              }
            }
          }
        }
        exitRadialCheck: ;

        int state;
        if (!WITHIN_EPSILON(ctlActions->position, info->activePosition, ctlProps->livezone)) {
          /* Positive edge */
          state = 1;
          if (!WITHIN_EPSILON(cmdPosition * (radialCombo ? SQRT_TWO : 1.0), info->activePosition, ctlProps->livezone))
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }
        else {
          /* Negative edge */
          state = 0;
          if (WITHIN_EPSILON(cmdPosition * (radialCombo ? SQRT_TWO : 1.0), info->activePosition, ctlProps->livezone))
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }

        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->state = state;
        rtnIndex++;
        break;
      }

      case ACTION_USAGE_AXIS: {
        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->position = cmdPosition;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls %u",
            actionNames[action->actionType], action->usage, CONTROLLER_AXIS_PREFIX, binding);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->position = cmdPosition;

  *count = rtnIndex;
  return rtnBuf;
}

/* Returned array should not be freed */
SA_ActionEvent_t** controllerButtonInput(
  unsigned long binding,
  int state,
  int *count)
{
  static SA_ActionEvent_t *rtnBuf[MAX_ACTIONS_PER_BIND];

  *count = 0;
  if (!controllerButtonTable || (binding >= numControllerButtons))
    return NULL;

  ControlActions_t *ctlActions = &(controllerButtonTable[binding]);
  if (0 == ctlActions->count)
    return NULL;

  /* Adjust raw state to commanded state; currently, they are the same */
  int cmdState = state;

  /* If the button state hasn't changed, skip it */
  if (cmdState == ctlActions->state)
    return rtnBuf;

  size_t rtnIndex = 0;
  for (size_t i = 0; i < ctlActions->count; i++) {
    ActionDef_t *action = ctlActions->actions[i];
    if (!action)
      goto next_action;
    if (!IS_CURRENT_CONTEXT(action->context))
      goto next_action;

    ActionInfo_t *info = &(action->info);
    switch (action->usage) {
      case ACTION_USAGE_BUTTON_PRESS:
      case ACTION_USAGE_BUTTON_RELEASE: {
        int state;
        if (ctlActions->state != info->activeState) {
          /* Positive edge */
          state = 1;
          if (cmdState != info->activeState)
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }
        else {
          /* Negative edge */
          state = 0;
          if (cmdState == info->activeState)
            goto next_action;
          if (!checkCombo(action))
            goto next_action;
        }

        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->state = state;
        rtnIndex++;
        break;
      }

      case ACTION_USAGE_AXIS: {
        /* The action is active - fill the return buffer */
        rtnBuf[rtnIndex]->action = action->actionType;
        rtnBuf[rtnIndex]->usage = action->usage;
        rtnBuf[rtnIndex]->position = (double) state;
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected %s usage (%d) for %ls %u",
            actionNames[action->actionType], action->usage, CONTROLLER_BUTTON_PREFIX, binding);
        continue;
      }
    }

    next_action:
    continue;
  }

  ctlActions->state = cmdState;

  *count = rtnIndex;
  return rtnBuf;
}

/**
 ** Local Functions
 **/

static int setKeyNames(
  void)
{
  /* Clear every array before beginning */
  memset(keyNames, 0, sizeof(keyNames));
  memset(modNames, 0, sizeof(modNames));

  /* Fill in with string literals for unprintable keys */
  keyNames[GLFW_KEY_ESCAPE] = W("Escape");
  keyNames[GLFW_KEY_ENTER] = W("Enter");
  keyNames[GLFW_KEY_TAB] = W("Tab");
  keyNames[GLFW_KEY_BACKSPACE] = W("Backspace");
  keyNames[GLFW_KEY_INSERT] = W("Insert");
  keyNames[GLFW_KEY_DELETE] = W("Delete");
  keyNames[GLFW_KEY_RIGHT] = W("Right");
  keyNames[GLFW_KEY_LEFT] = W("Left");
  keyNames[GLFW_KEY_DOWN] = W("Down");
  keyNames[GLFW_KEY_UP] = W("Up");
  keyNames[GLFW_KEY_PAGE_UP] = W("Page Up");
  keyNames[GLFW_KEY_PAGE_DOWN] = W("Page Down");
  keyNames[GLFW_KEY_HOME] = W("Home");
  keyNames[GLFW_KEY_END] = W("End");
  keyNames[GLFW_KEY_CAPS_LOCK] = W("Caps Lock");
  keyNames[GLFW_KEY_SCROLL_LOCK] = W("Scroll Lock");
  keyNames[GLFW_KEY_NUM_LOCK] = W("Num Lock");
  keyNames[GLFW_KEY_PRINT_SCREEN] = W("Print Screen");
  keyNames[GLFW_KEY_PAUSE] = W("Pause");
  keyNames[GLFW_KEY_F1] = W("F1");
  keyNames[GLFW_KEY_F2] = W("F2");
  keyNames[GLFW_KEY_F3] = W("F3");
  keyNames[GLFW_KEY_F4] = W("F4");
  keyNames[GLFW_KEY_F5] = W("F5");
  keyNames[GLFW_KEY_F6] = W("F6");
  keyNames[GLFW_KEY_F7] = W("F7");
  keyNames[GLFW_KEY_F8] = W("F8");
  keyNames[GLFW_KEY_F9] = W("F9");
  keyNames[GLFW_KEY_F10] = W("F10");
  keyNames[GLFW_KEY_F11] = W("F11");
  keyNames[GLFW_KEY_F12] = W("F12");
  keyNames[GLFW_KEY_F13] = W("F13");
  keyNames[GLFW_KEY_F14] = W("F14");
  keyNames[GLFW_KEY_F15] = W("F15");
  keyNames[GLFW_KEY_F16] = W("F16");
  keyNames[GLFW_KEY_F17] = W("F17");
  keyNames[GLFW_KEY_F18] = W("F18");
  keyNames[GLFW_KEY_F19] = W("F19");
  keyNames[GLFW_KEY_F20] = W("F20");
  keyNames[GLFW_KEY_F21] = W("F21");
  keyNames[GLFW_KEY_F22] = W("F22");
  keyNames[GLFW_KEY_F23] = W("F23");
  keyNames[GLFW_KEY_F24] = W("F24");
  keyNames[GLFW_KEY_F25] = W("F25");
  keyNames[GLFW_KEY_KP_ENTER] = W("Num Enter");
  keyNames[GLFW_KEY_KP_EQUAL] = W("Num =");
  keyNames[GLFW_KEY_LEFT_SHIFT] = W("Left Shift");
  keyNames[GLFW_KEY_LEFT_CONTROL] = W("Left Control");
  keyNames[GLFW_KEY_LEFT_ALT] = W("Left Alt");
  keyNames[GLFW_KEY_LEFT_SUPER] = W("Left Super");
  keyNames[GLFW_KEY_RIGHT_SHIFT] = W("Right Shift");
  keyNames[GLFW_KEY_RIGHT_CONTROL] = W("Right Control");
  keyNames[GLFW_KEY_RIGHT_ALT] = W("Right Alt");
  keyNames[GLFW_KEY_RIGHT_SUPER] = W("Right Super");
  keyNames[GLFW_KEY_MENU] = W("Menu");

  /* Fill in all the names for printable keys that we automatically receive from GLFW */
  for (size_t i = 0; i <= GLFW_KEY_LAST; i++) {
    const char *name = glfwGetKeyName(i, 0);
    if (name) {
      size_t len = strlen(name);

      wchar_t *wName = calloc(len + 1, sizeof(wchar_t));
      if (!wName)
        return SA_ERROR_MEMORY;
      for (size_t c = 0; c < len; c++)
        wName[c] = name[c];

      keyNames[i] = wName;
    }
  }

  /* Create string literals for modifier keys and their combinations */
  modNames[GLFW_MOD_SHIFT] = MOD_KEY_SHIFT;
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_CONTROL] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_CONTROL);
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_CONTROL | GLFW_MOD_ALT] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_CONTROL MOD_SEP MOD_KEY_ALT);
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_CONTROL | GLFW_MOD_ALT | GLFW_MOD_SUPER] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_CONTROL MOD_SEP MOD_KEY_ALT MOD_SEP MOD_KEY_SUPER);
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_CONTROL | GLFW_MOD_SUPER] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_CONTROL MOD_SEP MOD_KEY_SUPER);
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_ALT] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_ALT);
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_ALT | GLFW_MOD_SUPER] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_ALT MOD_SEP MOD_KEY_SUPER);
  modNames[GLFW_MOD_SHIFT | GLFW_MOD_SUPER] =
      (MOD_KEY_SHIFT MOD_SEP MOD_KEY_SUPER);

  modNames[GLFW_MOD_CONTROL] = MOD_KEY_CONTROL;
  modNames[GLFW_MOD_CONTROL | GLFW_MOD_ALT] =
      (MOD_KEY_CONTROL MOD_SEP MOD_KEY_ALT);
  modNames[GLFW_MOD_CONTROL | GLFW_MOD_ALT | GLFW_MOD_SUPER] =
      (MOD_KEY_CONTROL MOD_SEP MOD_KEY_ALT MOD_SEP MOD_KEY_SUPER);
  modNames[GLFW_MOD_CONTROL | GLFW_MOD_SUPER] =
      (MOD_KEY_CONTROL MOD_SEP MOD_KEY_SUPER);

  modNames[GLFW_MOD_ALT] = MOD_KEY_ALT;
  modNames[GLFW_MOD_ALT | GLFW_MOD_SUPER] =
      (MOD_KEY_ALT MOD_SEP MOD_KEY_SUPER);

  modNames[GLFW_MOD_SUPER] = MOD_KEY_SUPER;

  return SA_SUCCESS;
}

static int refreshKeymap(
  void)
{
#define OPT_CONFIG(optFunc, category, param, var, value)                  \
  ({                                                                      \
    int _err = optFunc(&keyConfig, (category), (param), &(var), (value)); \
    if (SA_SUCCESS != _err) {                                             \
      logMsg(LOG_WARNING, "Failed to parse key mappings: (%d) %ls",       \
             _err, getError(_err));                                       \
      rtnVal = _err;                                                      \
      goto exit;                                                          \
    }                                                                     \
  })

#define GET_CONFIG(getFunc, category, param, var)                   \
  ({                                                                \
    int _err = getFunc(&keyConfig, (category), (param), &(var));    \
    if ((SA_SUCCESS != _err) && (SA_ERROR_NOT_FOUND != _err)) {     \
      logMsg(LOG_WARNING, "Failed to parse key mappings: (%d) %ls", \
             _err, getError(_err));                                 \
      rtnVal = _err;                                                \
      goto exit;                                                    \
    }                                                               \
    _err;                                                           \
  })

  int err;
  int rtnVal = SA_SUCCESS;

  err = loadConfig(&keyConfig);
  if (SA_SUCCESS == err) {
    logMsg(LOG_DEBUG, "Key mappings loaded");
  }
  else if (SA_CREATED_CONFIG == err) {
    logMsg(LOG_INFO, "Key mappings not found; refreshing with default settings");
  }
  else if (SA_SUCCESS > err) {
    logMsg(LOG_ERROR, "Failed to load key mappings: (%d) %ls", err, getError(err));
    rtnVal = err;
    goto exit;
  }

  /* Set the key bindings to their default state */
  for (ControllerType_t type = 0; type < NUM_CONTROLLER_TYPES; type++) {
    switch (type) {
      case CONTROLLER_KEYBOARD_MOUSE: {
        setDefaults(type, CONTROLLER_CONFIG_KEYBOARD_MOUSE);
        break;
      }
      case CONTROLLER_GAMEPAD: {
        setDefaults(type, CONTROLLER_CONFIG_XBOX_360);
        break;
      }
      case NUM_CONTROLLER_TYPES:
      break;
    }
  }

  /* Set defaults for our particular controller */
  ControllerConfiguration_t controllerConfig;
  if (activeControllerType == CONTROLLER_KEYBOARD_MOUSE)
    controllerConfig = CONTROLLER_CONFIG_KEYBOARD_MOUSE;
  else if (activeControllerType == CONTROLLER_GAMEPAD)
    controllerConfig = CONTROLLER_CONFIG_XBOX_360;
  setDefaults(activeControllerType, controllerConfig);

  wchar_t parseBuf[KEYMAP_CONFIG_BUF_LEN];
  char categoryName[KEYMAP_CONFIG_BUF_LEN];
  char paramName[KEYMAP_CONFIG_BUF_LEN];
  wchar_t *bindStr = NULL;
  wchar_t *inputTypeStr = NULL;

  /* Parse all the different key mappings */
  for (ControllerType_t type = 0; type < NUM_CONTROLLER_TYPES; type++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      strcpy(categoryName, ControllerType_Images[type]);
      strcat(categoryName, "_");
      strcat(categoryName, SA_Action_Images[a]);

      for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
        ActionDef_t *action = &(actionTable[type][set][a]);

        /* Get binding string */
        const wchar_t *bindName = getBindingName(&(action->info), parseBuf);
        strcpy(paramName, BINDING_PARAM_NAME);
        strcat(paramName, BindingSet_Images[set]);

        if (SET_PRIMARY == set) {
          OPT_CONFIG(configOptString, categoryName, paramName,
              bindStr,
              bindName);

          /* Print user-friendly name on the top */
          configSetHeader(&keyConfig, categoryName, actionNames[a]);
        }
        else if (INPUT_TYPE_NONE != action->info.input) {
          OPT_CONFIG(configOptString, categoryName, paramName,
              bindStr,
              bindName);
        }
        else {
          if (SA_ERROR_NOT_FOUND == GET_CONFIG(configGetString, categoryName, paramName, bindStr))
            goto error;
        }

        /* Get input type string */
        strcpy(paramName, INPUT_TYPE_PARAM_NAME);
        strcat(paramName, BindingSet_Images[set]);

        if ((SET_PRIMARY == set) || (INPUT_TYPE_NONE != action->info.input)) {
          OPT_CONFIG(configOptString, categoryName, paramName,
              inputTypeStr,
              InputType_Images[action->info.input]);
        }
        else {
          if (SA_ERROR_NOT_FOUND == GET_CONFIG(configGetString, categoryName, paramName, inputTypeStr))
            goto error;
        }

        /* Convert strings to usable values */
        unsigned long bind = 0;
        InputType_t inputType = getInputTypeFromString(inputTypeStr);
        if ((INPUT_TYPE_NONE == inputType) ||
            (SA_SUCCESS != (err = getBindingFromString(inputType, bindStr, &bind))))
        {
          if (INPUT_TYPE_NONE != inputType) {
            logMsg(LOG_WARNING, "Failed to parse binding for %ls: (%d) %ls",
                actionNames[a], err, getError(err));
          }
          goto error;
        }

        /* Set the actual binding information */
        if (isInputTypeValid(action->usage, inputType) && (INPUT_TYPE_KEYBOARD_MOD != inputType)) {
          action->info.bind = bind;
          action->info.input = inputType;
        }
        else {
          logMsg(LOG_WARNING, "Invalid input type for %ls",
              actionNames[a]);
          goto error;
        }

        /* Free returned strings */
        if (bindStr) {
          free(bindStr);
          bindStr = NULL;
        }
        if (inputTypeStr) {
          free(inputTypeStr);
          inputTypeStr = NULL;
        }

        /* Get combination inputs */
        action->comboDepth = 0;
        for (size_t c = 0; c < MAX_COMBO_DEPTH; c++) {
          /* Get binding string for a combo component */
          const wchar_t *comboBindName = getBindingName(&(action->combo[c]), parseBuf);
          strcpy(paramName, COMBO_PARAM_PREFIX);
          sprintf(paramName, "%s%d", paramName, (int) (c + 1));
          strcat(paramName, COMBO_BINDING_PARAM_NAME);
          strcat(paramName, BindingSet_Images[set]);

          if (INPUT_TYPE_NONE != action->combo[c].input) {
            OPT_CONFIG(configOptString, categoryName, paramName,
                bindStr,
                comboBindName);
          }
          else {
            if (SA_ERROR_NOT_FOUND == GET_CONFIG(configGetString, categoryName, paramName, bindStr))
              goto combo_error;
          }

          /* Get input type string for a combo component */
          strcpy(paramName, COMBO_PARAM_PREFIX);
          sprintf(paramName, "%s%d", paramName, (int) (c + 1));
          strcat(paramName, COMBO_INPUT_TYPE_PARAM_NAME);
          strcat(paramName, BindingSet_Images[set]);

          if (INPUT_TYPE_NONE != action->combo[c].input) {
            OPT_CONFIG(configOptString, categoryName, paramName,
                inputTypeStr,
                InputType_Images[action->combo[c].input]);
          }
          else {
            if (SA_ERROR_NOT_FOUND == GET_CONFIG(configGetString, categoryName, paramName, inputTypeStr))
              goto combo_error;
          }

          /* Convert strings to usable values */
          unsigned long bind = 0;
          InputType_t inputType = getInputTypeFromString(inputTypeStr);
          if ((INPUT_TYPE_NONE == inputType) ||
              (SA_SUCCESS != (err = getBindingFromString(inputType, bindStr, &bind))))
          {
            if (INPUT_TYPE_NONE != inputType) {
              logMsg(LOG_WARNING, "Failed to parse combo%d binding for %ls: (%d) %ls",
                  c + 1, actionNames[a], err, getError(err));
            }
            goto combo_error;
          }

          /* Set the actual binding information */
          if (isInputTypeValid(action->usage, inputType)) {
            action->combo[c].bind = bind;
            action->combo[c].input = inputType;
            action->comboDepth = c + 1;
          }
          else {
            logMsg(LOG_WARNING, "Invalid combo%d input type for %ls",
                c + 1, actionNames[a]);
            goto combo_error;
          }

          goto clean_up_combo;

          combo_error:
          action->combo[c].bind = 0;
          action->combo[c].input = INPUT_TYPE_NONE;

          clean_up_combo:
          /* Finish the sub-iteration and free returned strings */
          if (bindStr) {
            free(bindStr);
            bindStr = NULL;
          }
          if (inputTypeStr) {
            free(inputTypeStr);
            inputTypeStr = NULL;
          }
        }

        goto clean_up;

        error:
        action->info.bind = 0;
        action->info.input = INPUT_TYPE_NONE;

        clean_up:
        /* Finish the iteration and free returned strings */
        if (bindStr) {
          free(bindStr);
          bindStr = NULL;
        }
        if (inputTypeStr) {
          free(inputTypeStr);
          inputTypeStr = NULL;
        }
      }
    }
  }

  /* Save what we've got */
  err = saveConfig(&keyConfig);
  if (SA_SUCCESS == err) {
    logMsg(LOG_DEBUG, "Key mappings saved");
  }
  else if (SA_SUCCESS > err) {
    logMsg(LOG_ERROR, "Failed to save key mappings: (%d) %ls", err, getError(err));
    rtnVal = err;
    goto exit;
  }

  exit:
  /* Clean up allocations */
  if (bindStr)
    free(bindStr);
  if (inputTypeStr)
    free(inputTypeStr);

  /* Close out the key configuration file once we're done with it */
  closeConfig(&keyConfig);
  return rtnVal;

#undef OPT_CONFIG
#undef GET_CONFIG
}

static void setDefaults(
  ControllerType_t type,
  ControllerConfiguration_t config)
{
  switch (config) {
    case CONTROLLER_CONFIG_KEYBOARD_MOUSE:
    default: {
      /* Buttons */
      assert(actionTable[type][SET_PRIMARY][SA_ACT_EXIT].usage == ACTION_USAGE_BUTTON_RELEASE);
      actionTable[type][SET_PRIMARY][SA_ACT_EXIT].info.input = INPUT_TYPE_KEYBOARD;
      actionTable[type][SET_PRIMARY][SA_ACT_EXIT].info.bind = GLFW_KEY_ESCAPE;

      assert(actionTable[type][SET_PRIMARY][SA_ACT_TEST1].usage == ACTION_USAGE_POINTER);
      actionTable[type][SET_PRIMARY][SA_ACT_TEST1].info.input = INPUT_TYPE_MOUSE_CURSOR;

      assert(actionTable[type][SET_PRIMARY][SA_ACT_TEST2].usage == ACTION_USAGE_BUTTON_PRESS);
      actionTable[type][SET_PRIMARY][SA_ACT_TEST2].info.input = INPUT_TYPE_MOUSE_BUTTON;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST2].info.bind = GLFW_MOUSE_BUTTON_LEFT;
      assert(actionTable[type][SET_SECONDARY][SA_ACT_TEST2].usage == ACTION_USAGE_BUTTON_PRESS);
      actionTable[type][SET_SECONDARY][SA_ACT_TEST2].info.input = INPUT_TYPE_KEYBOARD;
      actionTable[type][SET_SECONDARY][SA_ACT_TEST2].info.bind = GLFW_KEY_X;

      assert(actionTable[type][SET_PRIMARY][SA_ACT_TEST3].usage == ACTION_USAGE_AXIS);
      actionTable[type][SET_PRIMARY][SA_ACT_TEST3].info.input = INPUT_TYPE_MOUSE_SCROLL_AXIS;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST3].info.bind = SCROLL_AXIS_VERTICAL;

      assert(actionTable[type][SET_PRIMARY][SA_ACT_TEST4].usage == ACTION_USAGE_BUTTON_PRESS);
      actionTable[type][SET_PRIMARY][SA_ACT_TEST4].info.input = INPUT_TYPE_MOUSE_SCROLL;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST4].info.bind = SCROLL_UP;

      assert(actionTable[type][SET_PRIMARY][SA_ACT_TEST5].usage == ACTION_USAGE_BUTTON_PRESS);
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].info.input = INPUT_TYPE_KEYBOARD;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].info.bind = GLFW_KEY_A;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].comboDepth = 2;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].combo[0].input = INPUT_TYPE_KEYBOARD;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].combo[0].bind = GLFW_KEY_S;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].combo[1].input = INPUT_TYPE_KEYBOARD_MOD;
      actionTable[type][SET_PRIMARY][SA_ACT_TEST5].combo[1].bind = GLFW_MOD_SHIFT;
      break;
    }

    case CONTROLLER_CONFIG_XBOX_360: {
      actionTable[type][SET_PRIMARY][SA_ACT_TEST1].info.input = INPUT_TYPE_NONE;
      break;
    }

    case CONTROLLER_CONFIG_PS4: {
      actionTable[type][SET_PRIMARY][SA_ACT_TEST1].info.input = INPUT_TYPE_NONE;
      break;
    }
  }
}

/* Note that modifier keys are ignored here because modifiers are never valid primary inputs; the input handler will
 * check for them on other key events, but pressing a modifier key will never trigger an action.  This is consistent with
 * expected HCI behavior regarding keyboards.
 */
static int initKeys(
  void)
{
  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_KEYBOARD_MOUSE][set][a]);

      if (action->info.input == INPUT_TYPE_KEYBOARD) {
        if ((action->info.bind >= 0) && (action->info.bind < ARRAY_LEN(keyTable))) {
          ControlActions_t *ctlActions = &(keyTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to key %ls",
                MAX_ACTIONS_PER_BIND, keyNames[action->info.bind]);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;

          action->info.activeState = 1;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_KEYBOARD) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < ARRAY_LEN(keyTable))) {
            ControlActions_t *ctlActions = &(keyTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to key %ls",
                  MAX_ACTIONS_PER_BIND, keyNames[action->info.bind]);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;

            action->combo[i].activeState = 1;
          }
        }
      }
    }
  }

  return SA_SUCCESS;
}

static int initMousePointer(
  void)
{
  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_KEYBOARD_MOUSE][set][a]);

      if (action->info.input == INPUT_TYPE_MOUSE_CURSOR) {
        if ((action->info.bind >= 0) && (action->info.bind < ARRAY_LEN(mousePointerTable))) {
          ControlActions_t *ctlActions = &(mousePointerTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to mouse pointer",
                MAX_ACTIONS_PER_BIND);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_MOUSE_CURSOR) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < ARRAY_LEN(mousePointerTable))) {
            ControlActions_t *ctlActions = &(mousePointerTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to mouse pointer",
                  MAX_ACTIONS_PER_BIND);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;
          }
        }
      }
    }
  }

  return SA_SUCCESS;
}

static int initMouseButtons(
  void)
{
  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_KEYBOARD_MOUSE][set][a]);

      if (action->info.input == INPUT_TYPE_MOUSE_BUTTON) {
        if ((action->info.bind >= 0) && (action->info.bind < ARRAY_LEN(mouseButtonTable))) {
          ControlActions_t *ctlActions = &(mouseButtonTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to %ls",
                MAX_ACTIONS_PER_BIND, mouseNames[action->info.bind]);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;

          action->info.activeState = 1;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_MOUSE_BUTTON) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < ARRAY_LEN(mouseButtonTable))) {
            ControlActions_t *ctlActions = &(mouseButtonTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to %ls",
                  MAX_ACTIONS_PER_BIND, mouseNames[action->info.bind]);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;

            action->combo[i].activeState = 1;
          }
        }
      }
    }
  }

  return SA_SUCCESS;
}

static int initScrollEvents(
  void)
{
  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_KEYBOARD_MOUSE][set][a]);

      if (action->info.input == INPUT_TYPE_MOUSE_SCROLL) {
        if ((action->info.bind >= 0) && (action->info.bind < ARRAY_LEN(scrollEventTable))) {
          ControlActions_t *ctlActions = &(scrollEventTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to %ls",
                MAX_ACTIONS_PER_BIND, ScrollEvent_Images[action->info.bind]);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_MOUSE_SCROLL) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < ARRAY_LEN(scrollEventTable))) {
            ControlActions_t *ctlActions = &(scrollEventTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to %ls",
                  MAX_ACTIONS_PER_BIND, ScrollEvent_Images[action->info.bind]);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;
          }
        }
      }
    }
  }

  return SA_SUCCESS;
}

static int initScrollAxes(
  void)
{
  /* Fill in the binding array-lists */
  for (BindingSet_t set = 0; set < NUM_BINDING_SETS; set++) {
    for (SA_Action_t a = 0; a < NUM_SA_ACTIONS; a++) {
      ActionDef_t *action = &(actionTable[CONTROLLER_KEYBOARD_MOUSE][set][a]);

      if (action->info.input == INPUT_TYPE_MOUSE_SCROLL_AXIS) {
        if ((action->info.bind >= 0) && (action->info.bind < ARRAY_LEN(scrollAxisTable))) {
          ControlActions_t *ctlActions = &(scrollAxisTable[action->info.bind]);
          if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
            logMsg(LOG_ERROR, "Too many actions (%d) mapped to %ls",
                MAX_ACTIONS_PER_BIND, ScrollAxis_Images[action->info.bind]);
            return SA_ERROR_INPUT;
          }

          ctlActions->actions[ctlActions->count] = action;
          ctlActions->count++;
        }
      }

      for (size_t i = 0; i < MAX_COMBO_DEPTH; i++) {
        if (action->combo[i].input == INPUT_TYPE_MOUSE_SCROLL_AXIS) {
          if ((action->combo[i].bind >= 0) && (action->combo[i].bind < ARRAY_LEN(scrollAxisTable))) {
            ControlActions_t *ctlActions = &(scrollAxisTable[action->combo[i].bind]);
            if (ctlActions->count >= MAX_ACTIONS_PER_BIND) {
              logMsg(LOG_ERROR, "Too many actions (%d) mapped to %ls",
                  MAX_ACTIONS_PER_BIND, ScrollAxis_Images[action->info.bind]);
              return SA_ERROR_INPUT;
            }

            ctlActions->actions[ctlActions->count] = action;
            ctlActions->count++;
          }
        }
      }
    }
  }

  /* TODO: Add a way to configure this */
  for (ScrollAxis_t axis = 0; axis < NUM_SCROLL_AXES; axis++) {
    scrollAxisProps[axis].deadzone = 0.0;
    scrollAxisProps[axis].midpoint = 0.0;
    scrollAxisProps[axis].gain = 1.0;
  }

  return SA_SUCCESS;
}

static int checkCombo(
  const ActionDef_t *action)
{
  if (!action)
    return SA_FALSE;

  for (size_t j = 0; j < action->comboDepth; j++) {
    const ActionInfo_t *comboInfo = &(action->combo[j]);

    switch (comboInfo->input) {
      case INPUT_TYPE_KEYBOARD: {
        ControlActions_t *comboActions = &(keyTable[comboInfo->bind]);
        if (comboActions->state != comboInfo->activeState)
          return SA_FALSE;

        break;
      }

      case INPUT_TYPE_KEYBOARD_MOD: {
        ControlActions_t *comboActions[2];
        if (comboInfo->bind & GLFW_MOD_SHIFT) {
          comboActions[0] = &(keyTable[GLFW_KEY_LEFT_SHIFT]);
          comboActions[1] = &(keyTable[GLFW_KEY_RIGHT_SHIFT]);
          if ((comboActions[0]->state != 1) && (comboActions[1]->state != 1))
            return SA_FALSE;
        }
        if (comboInfo->bind & GLFW_MOD_CONTROL) {
          comboActions[0] = &(keyTable[GLFW_KEY_LEFT_CONTROL]);
          comboActions[1] = &(keyTable[GLFW_KEY_RIGHT_CONTROL]);
          if ((comboActions[0]->state != 1) && (comboActions[1]->state != 1))
            return SA_FALSE;
        }
        if (comboInfo->bind & GLFW_MOD_ALT) {
          comboActions[0] = &(keyTable[GLFW_KEY_LEFT_ALT]);
          comboActions[1] = &(keyTable[GLFW_KEY_RIGHT_ALT]);
          if ((comboActions[0]->state != 1) && (comboActions[1]->state != 1))
            return SA_FALSE;
        }
        if (comboInfo->bind & GLFW_MOD_SUPER) {
          comboActions[0] = &(keyTable[GLFW_KEY_LEFT_SUPER]);
          comboActions[1] = &(keyTable[GLFW_KEY_RIGHT_SUPER]);
          if ((comboActions[0]->state != 1) && (comboActions[1]->state != 1))
            return SA_FALSE;
        }

        break;
      }

      case INPUT_TYPE_MOUSE_BUTTON: {
        ControlActions_t *comboActions = &(mouseButtonTable[comboInfo->bind]);
        if (comboActions->state != comboInfo->activeState)
          return SA_FALSE;

        break;
      }

      case INPUT_TYPE_MOUSE_SCROLL: {
        ControlActions_t *comboActions = &(scrollEventTable[comboInfo->bind]);
        if (comboActions->state != comboInfo->activeState)
          return SA_FALSE;

        break;
      }

      case INPUT_TYPE_CONTROLLER_AXIS: {
        ControlActions_t *comboActions = &(controllerAxisTable[comboInfo->bind]);
        ControlProps_t *comboProps = &(controllerAxisProps[comboInfo->bind]);

        /* Special case for diagonal triggers on two axes (what the fuck is their control scheme?!) */
        if ((comboProps->radialPair >= 0) && ((unsigned long) comboProps->radialPair == action->info.bind)) {
          if (!WITHIN_EPSILON(comboActions->position * SQRT_TWO, comboInfo->activePosition, comboProps->livezone))
            return SA_FALSE;
        }
        else {
          if (!WITHIN_EPSILON(comboActions->position, comboInfo->activePosition, comboProps->livezone))
            return SA_FALSE;
        }

        break;
      }

      case INPUT_TYPE_CONTROLLER_BUTTON: {
        ControlActions_t *comboActions = &(controllerButtonTable[comboInfo->bind]);
        if (comboActions->state != comboInfo->activeState)
          return SA_FALSE;

        break;
      }

      case INPUT_TYPE_NONE: {
        break;
      }

      default: {
        logMsg(LOG_WARNING, "Unexpected input type (%d) for %ls combo component %d",
            comboInfo->input, actionNames[action->actionType], j);
        return SA_FALSE;
      }
    }
  }

  return SA_TRUE;
}

static const wchar_t* getBindingName(
  const ActionInfo_t *info,
  wchar_t *scratchBuffer)
{
  if (!info || !scratchBuffer)
    return NO_BIND;

  switch (info->input) {
    case INPUT_TYPE_KEYBOARD: {
      if (keyNames[info->bind])
        return keyNames[info->bind];
      else
        return NO_BIND;
    }

    case INPUT_TYPE_KEYBOARD_MOD: {
      if (modNames[info->bind])
        return modNames[info->bind];
      else
        return NO_BIND;
    }

    case INPUT_TYPE_MOUSE_CURSOR: {
      return MOUSE_CURSOR_NAME;
    }

    case INPUT_TYPE_MOUSE_BUTTON: {
      if (mouseNames[info->bind])
        return mouseNames[info->bind];
      else
        return NO_BIND;
    }

    case INPUT_TYPE_MOUSE_SCROLL: {
      if (ScrollEvent_Images[info->bind])
        return ScrollEvent_Images[info->bind];
      else
        return NO_BIND;
    }

    case INPUT_TYPE_MOUSE_SCROLL_AXIS: {
      if (ScrollAxis_Images[info->bind])
        return ScrollAxis_Images[info->bind];
      else
        return NO_BIND;
    }

    case INPUT_TYPE_CONTROLLER_AXIS: {
      _swprintf(scratchBuffer, CONTROLLER_AXIS_PREFIX W("%u"), info->bind);
      return &(scratchBuffer[0]);
    }

    case INPUT_TYPE_CONTROLLER_BUTTON: {
      _swprintf(scratchBuffer, CONTROLLER_BUTTON_PREFIX W("%u"), info->bind);
      return &(scratchBuffer[0]);
    }

    default: {
      return NO_BIND;
    }
  }

  return NO_BIND;
}

static int getBindingFromString(
  InputType_t inputType,
  wchar_t *bindStr,
  unsigned long *bind)
{
  if (!bindStr || !bind)
    return INPUT_TYPE_NONE;

  switch (inputType) {
    case INPUT_TYPE_KEYBOARD: {
      for (size_t b = 0; b < ARRAY_LEN(keyNames); b++) {
        if (!(keyNames[b]))
          continue;

        if (0 == wcsicmp(bindStr, keyNames[b])) {
          *bind = b;
          return SA_SUCCESS;
        }
      }

      return SA_ERROR_NOT_FOUND;
    }

    case INPUT_TYPE_KEYBOARD_MOD: {
      for (size_t b = 0; b < ARRAY_LEN(modNames); b++) {
        if (!(modNames[b]))
          continue;

        if (0 == wcsicmp(bindStr, modNames[b])) {
          *bind = b;
          return SA_SUCCESS;
        }
      }

      return SA_ERROR_NOT_FOUND;
    }

    case INPUT_TYPE_MOUSE_CURSOR: {
      if (0 == wcsicmp(bindStr, MOUSE_CURSOR_NAME)) {
        *bind = 0;
        return SA_SUCCESS;
      }

      return SA_ERROR_NOT_FOUND;
    }

    case INPUT_TYPE_MOUSE_BUTTON: {
      for (size_t b = 0; b < ARRAY_LEN(mouseNames); b++) {
        if (0 == wcsicmp(bindStr, mouseNames[b])) {
          *bind = b;
          return SA_SUCCESS;
        }
      }

      return SA_ERROR_NOT_FOUND;
    }

    case INPUT_TYPE_MOUSE_SCROLL: {
      for (size_t b = 0; b < ARRAY_LEN(ScrollEvent_Images); b++) {
        if (0 == wcsicmp(bindStr, ScrollEvent_Images[b])) {
          *bind = b;
          return SA_SUCCESS;
        }
      }

      return SA_ERROR_NOT_FOUND;
    }

    case INPUT_TYPE_MOUSE_SCROLL_AXIS: {
      for (size_t b = 0; b < ARRAY_LEN(ScrollAxis_Images); b++) {
        if (0 == wcsicmp(bindStr, ScrollAxis_Images[b])) {
          *bind = b;
          return SA_SUCCESS;
        }
      }

      return SA_ERROR_NOT_FOUND;
    }

    case INPUT_TYPE_CONTROLLER_AXIS: {
      /* Compare the first part of the input */
      if (0 != wcsnicmp(bindStr, CONTROLLER_AXIS_PREFIX, wcslen(CONTROLLER_AXIS_PREFIX)))
        return SA_ERROR_NOT_FOUND;

      wchar_t *tailPtr = &(bindStr[0]);
      long axis = wcstol(bindStr, &tailPtr, 10);

      /* Check for syntactical validity */
      if (tailPtr == &(bindStr[0]))
        return SA_ERROR_SYNTAX;

      *bind = (unsigned long) axis;
      return SA_SUCCESS;
    }

    case INPUT_TYPE_CONTROLLER_BUTTON: {
      /* Compare the first part of the input */
      if (0 != wcsnicmp(bindStr, CONTROLLER_BUTTON_PREFIX, wcslen(CONTROLLER_BUTTON_PREFIX)))
        return SA_ERROR_NOT_FOUND;

      wchar_t *tailPtr = &(bindStr[0]);
      long axis = wcstol(bindStr, &tailPtr, 10);

      /* Check for syntactical validity */
      if (tailPtr == &(bindStr[0]))
        return SA_ERROR_SYNTAX;

      *bind = (unsigned long) axis;
      return SA_SUCCESS;
    }

    default: {
      return SA_ERROR_NOT_FOUND;
    }
  }

  return SA_ERROR_NOT_FOUND;
}

static InputType_t getInputTypeFromString(
  const wchar_t *inputTypeStr)
{
  if (!inputTypeStr)
    return INPUT_TYPE_NONE;

  for (InputType_t type = 0; type < NUM_INPUT_TYPES; type++) {
    if (0 == wcsicmp(inputTypeStr, InputType_Images[type]))
      return type;
  }

  logMsg(LOG_WARNING, "\"%ls\" does not match any known input type", inputTypeStr);
  return INPUT_TYPE_NONE;
}

static int isInputTypeValid(
  ActionUsage_t usage,
  InputType_t inputType)
{
  switch (usage) {
    case ACTION_USAGE_BUTTON_PRESS:
    case ACTION_USAGE_BUTTON_RELEASE: {
      switch (inputType) {
        case INPUT_TYPE_KEYBOARD:
        case INPUT_TYPE_KEYBOARD_MOD:
        case INPUT_TYPE_MOUSE_BUTTON:
        case INPUT_TYPE_MOUSE_SCROLL:
        case INPUT_TYPE_CONTROLLER_AXIS:
        case INPUT_TYPE_CONTROLLER_BUTTON: {
          return SA_TRUE;
        }

        case INPUT_TYPE_MOUSE_CURSOR:
        case INPUT_TYPE_MOUSE_SCROLL_AXIS:
        default: {
          return SA_FALSE;
        }
      }

      return SA_FALSE;
    }

    case ACTION_USAGE_AXIS: {
      switch (inputType) {
        case INPUT_TYPE_KEYBOARD:
        case INPUT_TYPE_MOUSE_BUTTON:
        case INPUT_TYPE_MOUSE_SCROLL_AXIS:
        case INPUT_TYPE_CONTROLLER_AXIS:
        case INPUT_TYPE_CONTROLLER_BUTTON: {
          return SA_TRUE;
        }

        case INPUT_TYPE_KEYBOARD_MOD:
        case INPUT_TYPE_MOUSE_CURSOR:
        case INPUT_TYPE_MOUSE_SCROLL:
        default: {
          return SA_FALSE;
        }
      }

      return SA_FALSE;
    }

    case ACTION_USAGE_POINTER: {
      switch (inputType) {
        case INPUT_TYPE_MOUSE_CURSOR: {
          return SA_TRUE;
        }

        case INPUT_TYPE_KEYBOARD:
        case INPUT_TYPE_KEYBOARD_MOD:
        case INPUT_TYPE_MOUSE_BUTTON:
        case INPUT_TYPE_MOUSE_SCROLL:
        case INPUT_TYPE_MOUSE_SCROLL_AXIS:
        case INPUT_TYPE_CONTROLLER_AXIS:
        case INPUT_TYPE_CONTROLLER_BUTTON:
        default: {
          return SA_FALSE;
        }
      }

      return SA_FALSE;
    }

    default: {
      return SA_FALSE;
    }
  }

  return SA_FALSE;
}
