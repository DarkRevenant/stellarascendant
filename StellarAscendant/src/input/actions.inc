#ifndef ACTION_DEF_START
#define ACTION_DEF_START()
#endif
#ifndef ACTION_DEF
#define ACTION_DEF(X, type, context, name)
#endif
#ifndef ACTION_DEF_LAST
#define ACTION_DEF_LAST(X, type, context, name) ACTION_DEF(X, type, context, name)
#endif
#ifndef ACTION_DEF_END
#define ACTION_DEF_END()
#endif

ACTION_DEF_START()

ACTION_DEF(EXIT, BUTTON_RELEASE, ALL, "Exit")
ACTION_DEF(TEST1, POINTER, MENU, "Test Input #1")
ACTION_DEF(TEST2, BUTTON_PRESS, MENU, "Test Input #2")
ACTION_DEF(TEST3, AXIS, MENU, "Test Input #3")
ACTION_DEF(TEST4, BUTTON_PRESS, MENU, "Test Input #4")
ACTION_DEF_LAST(TEST5, BUTTON_PRESS, MENU, "Test Input #5")

ACTION_DEF_END()

#undef ACTION_DEF_START
#undef ACTION_DEF
#undef ACTION_DEF_LAST
#undef ACTION_DEF_END
