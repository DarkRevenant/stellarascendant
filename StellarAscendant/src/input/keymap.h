/**********************************************************************************************************************
 * keymap.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   bindKeymap              Loads the key configuration and binds controls to actions
 *   freeKeymap              Closes and frees the key configuration
 *   execKeymap              Advance key configuration module state machine
 *   initControllerAxes      Registers controller axis inputs and creates binding table
 *   initControllerButtons   Registers controller button inputs and creates binding table
 *   keyInput                Processes a keyboard input with the control mapping system
 *   mouseCursorInput        Processes a mouse cursor input with the control mapping system
 *   mouseButtonInput        Processes a mouse button input with the control mapping system
 *   scrollEventInput        Processes a scroll wheel event input with the control mapping system
 *   scrollAxisInput         Processes a scroll wheel axis input with the control mapping system
 *   controllerAxisInput     Processes a controller axis input with the control mapping system
 *   controllerButtonInput   Processes a controller button input with the control mapping system
 *********************************************************************************************************************/

#ifndef ___KEYMAP_H___
#define ___KEYMAP_H___

#include <stdint.h>

/** Utility Headers **/
#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 ** Definitions and Typedefs
 **/

typedef enum {
  CONTROLLER_KEYBOARD_MOUSE,
  CONTROLLER_GAMEPAD,

  NUM_CONTROLLER_TYPES
} ControllerType_t;

typedef enum {
  ACTION_USAGE_BUTTON_PRESS,
  ACTION_USAGE_BUTTON_RELEASE,
  ACTION_USAGE_AXIS,
  ACTION_USAGE_POINTER,

  NUM_ACTION_USAGE_TYPES
} ActionUsage_t;

typedef enum {
  SCROLL_RIGHT,
  SCROLL_LEFT,
  SCROLL_DOWN,
  SCROLL_UP,

  NUM_SCROLL_EVENTS
} ScrollEvent_t;

typedef enum {
  SCROLL_AXIS_VERTICAL,
  SCROLL_AXIS_HORIZONTAL,

  NUM_SCROLL_AXES
} ScrollAxis_t;

#define ACTION_DEF_START() \
  typedef enum {
#define ACTION_DEF(X, type, context, name) \
    SA_ACT_ ## X ,
#define ACTION_DEF_END() \
    NUM_SA_ACTIONS       \
  } SA_Action_t;

#include "actions.inc" /* SA_Action_t */

typedef struct {
  SA_Action_t action;
  ActionUsage_t usage;

  /* State information */
  double position;
  int state;
  double x;
  double y;
} SA_ActionEvent_t;

/**
 ** Prototypes
 **/

extern int bindKeymap(
  void);

extern int freeKeymap(
  void);

extern int execKeymap(
  void);

extern int initControllerAxes(
  int count);

extern int initControllerButtons(
  int count);

/* Returned array should not be freed */
extern SA_ActionEvent_t** keyInput(
  unsigned long key,
  int state,
  int *count);

extern SA_ActionEvent_t** mouseCursorInput(
  double x,
  double y,
  int *count);

/* Returned array should not be freed */
extern SA_ActionEvent_t** mouseButtonInput(
  unsigned long button,
  int state,
  int *count);

/* Returned array should not be freed */
extern SA_ActionEvent_t** scrollEventInput(
  ScrollEvent_t event,
  int *count);

/* Returned array should not be freed */
extern SA_ActionEvent_t** scrollAxisInput(
  ScrollAxis_t axis,
  double position,
  int *count);

/* Returned array should not be freed */
extern SA_ActionEvent_t** controllerAxisInput(
  unsigned long binding,
  double position,
  int *count);

/* Returned array should not be freed */
extern SA_ActionEvent_t** controllerButtonInput(
  unsigned long binding,
  int state,
  int *count);

#endif /* ___KEYMAP_H___ */
