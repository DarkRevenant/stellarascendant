/**********************************************************************************************************************
 * control.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initControls                 Initializes the control subsystem
 *   closeControls                Terminates the control subsystem
 *   pollInputs                   Polls the current state of inputs, producing input events as necessary
 *   changeControlContext         Change the current control context
 *********************************************************************************************************************/

/** SA Headers **/
/* (headers that are synonymous with control.h) */
#include "keymap.h"

#ifndef ___CONTROL_H___
#define ___CONTROL_H___

/** Utility Headers **/
#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 ** Definitions and Typedefs
 **/

typedef enum {
  CONTROL_CONTEXT_MENU,     //* Works in the menu; keyboard+mouse input is always effective       *//
  CONTROL_CONTEXT_GAMEPLAY, //* Works in-game; only active controller type is effective           *//
  CONTROL_CONTEXT_ALL,      //* Works in-game and in the menu; keyboard input is always effective *//

  NUM_CONTROL_CONTEXTS
} ControlContext_t;

typedef enum {
  ACTION_STATE_INACTIVE,
  ACTION_STATE_PRESSED,
  ACTION_STATE_HELD,
  ACTION_STATE_RELEASED,
  ACTION_STATE_MOVED,

  NUM_ACTION_STATE
} SA_ActionState_t;

/**
 ** Global Variables
 **/

extern ControlContext_t currentControlContext;
extern ControllerType_t activeControllerType;

/* -1 with no controller plugged in */
extern int activeControllerIndex;

/**
 ** Prototypes
 **/

extern int initControls(
  GLFWwindow *window);

extern int closeControls(
  void);

extern int pollInputs(
  void);

extern int changeControlContext(
  ControlContext_t context);

#endif /* ___CONTROL_H___ */
