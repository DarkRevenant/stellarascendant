/**********************************************************************************************************************
 * common.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initCommon         Initializes common structures
 *   closeCommon        Closes common structures
 *   removeLogFile      Closes and removes the log file
 *   getTimestamp       Returns time in milliseconds since GLFW was initialized
 *   getSysTime         Returns system time in microseconds
 *   getTime            Returns high-resolution time in seconds since GLFW was initialized
 *   buildPath          Creates a file path by concatenating a varying number of elements
 *   getPathTail        Get the tail element of a path
 *   flushToilet        Flush the log
 *   closeToilet        Stops and destroys the toilet thread
 *
 * Local Functions:
 *   createToilet       Starts a thread for the singular purpose of flushing logs
 *   runToilet          Runs the toilet thread in a loop
 *   freeDisplay        Frees memory allocated to the toilet thread
 *********************************************************************************************************************/

#include <fcntl.h>
#include <math.h>
#include <pthread.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

/** SA Headers **/
#include "common.h"
#include "config.h"
#include "display/display.h"
#include "utils/rand.h"

/**
 ** Global Constants
 **/

/* Must match SA_LogMsg_t */
const char *SA_LogMsg_Images[NUM_LOG_MSG_TYPES] = {
  "FATAL",
  "ERROR",
  "WARN",
  "INFO",
  "DEBUG"
};

/**
 ** Global Variables
 **/

uint64_t initSysTime = UINT64_C(0);

wchar_t *baseDir;

pthread_spinlock_t logLock = (pthread_spinlock_t) 0;
FILE *logFile = NULL;
int logFD = 0;

Rand_t random;

/**
 ** Constants
 **/

static const size_t toiletBowlStackSize = KBYTES_TO_BYTES(128);

/**
 ** Local Variables
 **/

static wchar_t *logFilePath = NULL;

static char logBuffer[65536];

static int toiletFilled = SA_FALSE;
static int plungeToilet = SA_FALSE;
static int clogToilet = SA_FALSE;

static pthread_t toiletBowl = (pthread_t) 0;
static pthread_mutex_t bathroomDoor = (pthread_mutex_t) 0;
static pthread_cond_t toiletFlush = (pthread_cond_t) 0;

/**
 ** Prototypes
 **/

static pthread_t* createToilet(
  void);

static void* runToilet(
  void *arg);

static void freeToilet(
  void);

/**
 ** Global Functions
 **/

int initCommon(
  void)
{
  wchar_t currDir[PATH_MAX];
  wchar_t *tempPath = NULL;
  int rtnVal = SA_SUCCESS;
  int err;

  initSysTime = getSysTime();

  /* Get the current directory */
  tempPath = _wgetcwd(currDir, ARRAY_LEN(currDir));
  if (!tempPath) {
    logMsg(LOG_ERROR, "Could not determine current directory: (%d) %ls", errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
    goto error;
  }

  /* Get lowercase base directory to compare to search terms */
  tempPath = getPathTail(currDir);
  if ((0 == wcsicmp(tempPath, SA_64_BIT_DIR)) ||
      (0 == wcsicmp(tempPath, SA_32_BIT_DIR)))
  {
    /* Base directory is behind the binary folder we're executing from */
    baseDir = PBACK;
  }
  else {
    /* Base directory is right here */
    baseDir = W("");
  }

  /* Create data directory, if necessary */
  tempPath = BUILD_PATH(baseDir, SA_DATA_DIR_PATH);
  err = _wmkdir(tempPath);
  if (0 > err) {
    if (errno != EEXIST) {
      logMsg(LOG_WARNING, "Failed to create data directory at \"%ls\": (%d) %ls",
          tempPath, errno, _wcserror(errno));
      rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
      goto error;
    }
  }

  /* Create log spinlock variable */
  err = pthread_spin_init(&logLock, PTHREAD_PROCESS_PRIVATE);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create spin-lock: (%d) %ls", err, _wcserror(err));
    rtnVal = genError(SA_GENERIC_ERROR_THREAD, err);
    goto error;
  }

  /* Create log file */
  size_t logIndex = 0;
  size_t logFileNameLen = wcslen(SA_LOG_FILE_NAME) + SA_LOG_FILE_INDICES + wcslen(SA_LOG_FILE_EXT) + 1;
  wchar_t *logFileName = alloca(logFileNameLen);

  while (logIndex < (size_t) round(pow(10, SA_LOG_FILE_INDICES))) {
    /* Create path string */
    if (logFilePath)
      free(logFilePath);
    snwprintf(logFileName, logFileNameLen,
        (SA_LOG_FILE_NAME W("%0") WSTRINGIFY(SA_LOG_FILE_INDICES) W("lld") SA_LOG_FILE_EXT),
        (long long) logIndex);
    logFilePath = BUILD_PATH(baseDir, SA_DATA_DIR_PATH, logFileName);

    /* Open the log file descriptor */
    logFD = _wopen(logFilePath, O_WRONLY | O_APPEND | O_CREAT | O_EXCL, DEFAULT_PERM);
    if (0 > logFD) {
      if (errno == EEXIST) {
        logIndex++;
        continue;
      }
      else {
        logMsg(LOG_WARNING, "Failed to create log file at \"%ls\": (%d) %ls", logFilePath, errno, _wcserror(errno));
        rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
        goto logFileError;
      }
    }
    else {
      /* It was created - we're done */
      break;
    }
  }

  if (0 > logFD) {
    logMsg(LOG_WARNING, "Too many log files!");
    rtnVal = SA_FAILURE;
    goto logFileError;
  }

  /* Open the log file stream */
  logFile = fdopen(logFD, "a");
  if (!logFile) {
    logMsg(LOG_WARNING, "Failed to create log file stream: (%d) %ls", errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
    goto logFileError;
  }

  if (logFilePath)
    logMsg(LOG_INFO, "Using log file %ls", logFilePath);
  logFileError:
  ;

  /* Set the log buffer to something high enough to never randomly pop out in the middle of a frame */
  setvbuf(stdout, logBuffer, _IOFBF, sizeof(logBuffer));

  createToilet();

  /* Create global random generator, then warm it up */
  initRand(&random, RAND_TYPE_ATOMIC);
  seedRand(&random, getSysTime());
  for (int i = 0; i < 512; i++)
    getRand(&random);

  return SA_SUCCESS;

  error:
  if (tempPath)
    free(tempPath);
  closeCommon();
  return rtnVal;
}

void closeCommon(
  void)
{
  closeToilet();

  /* Destroy log spinlock variable */
  if (logLock)
    pthread_spin_destroy(&logLock);
  logLock = (pthread_spinlock_t) 0;

  /* Close the log file stream/descriptor */
  if (logFile) {
    fclose(logFile);
    logFile = NULL;
    logFD = 0;
  }

  if (logFilePath)
    free(logFilePath);
  logFilePath = NULL;

  /* Destroy global random generator */
  destroyRand(&random);
}

int removeLogFile(
  void)
{
  int rtnVal = SA_SUCCESS;

  /* Close the log file stream/descriptor */
  if (logFile) {
    fclose(logFile);
    logFile = NULL;
    logFD = 0;
  }

  if (logFilePath) {
    int err = _wremove(logFilePath);
    if (0 > err) {
      rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
      goto exit;
    }
  }

  exit:
  return rtnVal;
}

/* Returns time in milliseconds since initialization */
uint64_t getTimestamp(
  void)
{
  return (getSysTime() - initSysTime) / (USEC_PER_SEC / MSEC_PER_SEC);
}

/* Returns system time in microseconds */
uint64_t getSysTime(
  void)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_usec + SEC_TO_USEC(tv.tv_sec);
}

/* Returns high-resolution time in seconds since initialization */
double getTime(
  void)
{
  if (isDisplayInitialized())
    return glfwGetTime() + firstGLFWTime;
  else
    return USEC_TO_SEC((double ) (getSysTime() - initSysTime));
}

/* Output must be freed; input list must have a NULL sentinel */
wchar_t* buildPath(
  wchar_t **pathList)
{
  const wchar_t *element;
  size_t pathLen;
  size_t i;

  /* Find full length of the path */
  pathLen = 0;
  i = 0;
  element = pathList[i];
  while (element) {
    size_t eleLen = wcslen(element);

    /* Add path separator length, except for the first element */
    if ((pathLen > 0) && (eleLen > 0))
      pathLen += PSEP_LEN;

    pathLen += eleLen;

    element = pathList[++i];
  }

  /* Allocate memory for the path */
  wchar_t *path = malloc((pathLen + 1) * sizeof(wchar_t));
  if (!path)
    return NULL;

  /* Create the path */
  pathLen = 0;
  i = 0;
  element = pathList[i];
  while (element) {
    size_t eleLen = wcslen(element);

    /* Add path separator, except for the first element */
    if ((pathLen > 0) && (eleLen > 0)) {
      memcpy(&(path[pathLen]), PSEP, PSEP_LEN * sizeof(wchar_t));
      pathLen += PSEP_LEN;
    }

    memcpy(&(path[pathLen]), element, eleLen * sizeof(wchar_t));
    pathLen += eleLen;

    element = pathList[++i];
  }

  /* Terminate the path */
  path[pathLen] = W('\0');
  return path;
}

wchar_t* getPathTail(
  wchar_t *path)
{
  if (!path)
    return NULL;

  /* Working backwards, find the first path separator.  We subtract 2, rather than 1, from the length of the path,
   * because we don't want to count the trailing path separator.
   */
  for (long i = wcslen(path) - 2; i >= 0; i--) {
    if (0 == wcsncmp(&(path[i]), PSEP, PSEP_LEN)) {
      return &(path[i + PSEP_LEN]);
    }
  }

  /* If no path separator found, then the tail is the entire path */
  return path;
}

int flushToilet(
  void)
{
  if (!toiletFilled)
    return SA_EXIT;

  /* Don't bother locking here - flushing the log can easily take longer than a frame */
  /* pthread_mutex_lock(&bathroomDoor); */
  plungeToilet = SA_TRUE;
  pthread_cond_signal(&toiletFlush);
  /* pthread_mutex_unlock(&bathroomDoor); */

  return SA_SUCCESS;
}

int closeToilet(
  void)
{
  /* Early exit if there's nothing left to close */
  if (!isDisplayInitialized())
    goto exit;

  /* Once we get a lock, signal to close and wait until it has actually closed */
  pthread_mutex_lock(&bathroomDoor);
  clogToilet = SA_TRUE;
  pthread_cond_broadcast(&toiletFlush);
  pthread_mutex_unlock(&bathroomDoor);

  /* Wait for thread to finish before exiting */
  void *threadRtn;
  pthread_join(toiletBowl, &threadRtn);

  /* Clean up data */
  exit:
  freeToilet();

  /* Flush one last time ;) */
  fflush(stdout);

  return SA_SUCCESS;
}

/**
 ** Local Functions
 **/

static pthread_t* createToilet(
  void)
{
  pthread_attr_t *toiletBowlAttr = alloca(sizeof(pthread_attr_t));

  int err;

  toiletFilled = SA_FALSE;
  plungeToilet = SA_FALSE;
  clogToilet = SA_FALSE;

  /* This is literally the shittiest thread - it gets the lowest priority! */
  struct sched_param schedParam;
  schedParam.sched_priority = sched_get_priority_min(SCHED_OTHER);

  /* Set toilet attributes */
  err = pthread_attr_init(toiletBowlAttr);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to initialize toilet attribute: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setdetachstate(toiletBowlAttr, PTHREAD_CREATE_JOINABLE);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set toilet attribute joinable: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setstacksize(toiletBowlAttr, toiletBowlStackSize);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set toilet bowl size to %'d quarts: (%d) %ls",
        toiletBowlStackSize, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set toilet bowl size to %'d quarts", toiletBowlStackSize);
  }
  err = pthread_attr_setinheritsched(toiletBowlAttr, PTHREAD_EXPLICIT_SCHED);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set toilet scheduling explicit: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setscope(toiletBowlAttr, PTHREAD_SCOPE_PROCESS);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set toilet to process scope: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  /* Only SCHED_OTHER is supported on Windows */
  err = pthread_attr_setschedpolicy(toiletBowlAttr, SCHED_OTHER);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set toilet scheduling policy to standard: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setschedparam(toiletBowlAttr, &schedParam);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set toilet scheduling priority to %d: (%d) %ls",
        schedParam.sched_priority, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set toilet scheduling priority to %d", schedParam.sched_priority);
  }

  /* Create mutex object to synchronize toilet to other threads */
  err = pthread_mutex_init(&bathroomDoor, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create mutex: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Create condition object to synchronize toilet to other threads */
  err = pthread_cond_init(&toiletFlush, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create condition: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Create toilet */
  err = pthread_create(&toiletBowl, toiletBowlAttr, runToilet, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create toilet: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Wait for the toilet to fill up */
  pthread_mutex_lock(&bathroomDoor);
  while (!toiletFilled)
    pthread_cond_wait(&toiletFlush, &bathroomDoor);
  pthread_mutex_unlock(&bathroomDoor);

  goto exit;

  error:
  freeToilet();

  exit:
  if (toiletBowlAttr)
    pthread_attr_destroy(toiletBowlAttr);
  return &toiletBowl;
}

static void* runToilet(
  void *arg)
{
  void *rtnVal = D2V(SA_SUCCESS);

  /* Signal to the world that the toilet is good to go */
  pthread_mutex_lock(&bathroomDoor);
  toiletFilled = SA_TRUE;
  pthread_cond_broadcast(&toiletFlush);
  pthread_mutex_unlock(&bathroomDoor);

  /* Run continuously */
  pthread_mutex_lock(&bathroomDoor);
  while (SA_TRUE) {
    if (clogToilet) {
      break;
    }
    else if (plungeToilet) {
      fflush(stdout);
      plungeToilet = SA_FALSE;
    }

    pthread_cond_wait(&toiletFlush, &bathroomDoor);
  }
  pthread_mutex_unlock(&bathroomDoor);

  /* Signal to the world that the toilet is closed for business */
  pthread_mutex_lock(&bathroomDoor);
  toiletFilled = SA_FALSE;
  pthread_cond_broadcast(&toiletFlush);
  pthread_mutex_unlock(&bathroomDoor);
  return rtnVal;
}

static void freeToilet(
  void)
{
  if (bathroomDoor)
    pthread_mutex_destroy(&bathroomDoor);
  if (toiletFlush)
    pthread_cond_destroy(&toiletFlush);
  bathroomDoor = (pthread_mutex_t) 0;
  toiletFlush = (pthread_cond_t) 0;
  toiletBowl = (pthread_t) 0;
}
