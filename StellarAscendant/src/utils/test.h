/**********************************************************************************************************************
 * test.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   testPool                Test out object pool functionality
 *   testScheduler           Test out scheduler functionality
 *   testDDS                 Test out DDS loading
 *   testSIMD                Test out SIMD timings
 *********************************************************************************************************************/

#ifndef ___TEST_H___
#define ___TEST_H___

/**
 ** Prototypes
 **/

extern void testPool(
  void);

extern void testScheduler(
  void);

extern void testDDS(
  void);

extern void testSIMD(
  void);

#endif /* ___TEST_H___ */
