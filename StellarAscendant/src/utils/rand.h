/**********************************************************************************************************************
 * rand.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initRand           Initializes a random number generator
 *   destroyRand        Closes a random number generator
 *   seedRand           Seeds a random number generator
 *   getRand            Generates a random integer from 0 to 2^64 - 1
 *   getRandUInt        Generates a random unsigned integer between two values, inclusive
 *   getRandInt         Generates a random signed integer between two values, inclusive
 *   getRandDouble      Generates a random double-precision float between two values, inclusive
 *   getRandFloat       Generates a random single-precision float between two values, inclusive
 *********************************************************************************************************************/

#ifndef ___RAND_H___
#define ___RAND_H___

#include <pthread.h>
#include <stdint.h>

/**
 ** Typedefs
 **/

struct _XorShiftStar {
  uint64_t s[16];
  size_t p;
};

struct _XorShiftStarAtomic {
  struct _XorShiftStar xss;
  pthread_spinlock_t lock;
};

typedef enum {
  RAND_TYPE_NONE,

  RAND_TYPE_XORSHIFT_STAR,
  RAND_TYPE_DEFAULT = RAND_TYPE_XORSHIFT_STAR,
  RAND_TYPE_XORSHIFT_STAR_ATOMIC,
  RAND_TYPE_ATOMIC = RAND_TYPE_XORSHIFT_STAR_ATOMIC,

  NUM_RAND_TYPES
} RandType_t;

typedef struct {
  RandType_t type;
  union {
    struct _XorShiftStar xss;
    struct _XorShiftStarAtomic xssa;
  };
} Rand_t;

/**
 ** Prototypes
 **/

extern void initRand(
  Rand_t *r,
  RandType_t type);

extern void destroyRand(
  Rand_t *r);

extern void seedRand(
  Rand_t *r,
  uint64_t seed);

extern uint64_t getRand(
  Rand_t *r);

extern uint64_t getRandUInt(
  Rand_t *r,
  uint64_t a,
  uint64_t b);

extern int64_t getRandInt(
  Rand_t *r,
  int64_t a,
  int64_t b);

extern double getRandDouble(
  Rand_t *r,
  double a,
  double b);

extern float getRandFloat(
  Rand_t *r,
  float a,
  float b);

#endif /* ___RAND_H___ */
