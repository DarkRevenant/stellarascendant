/**********************************************************************************************************************
 * rand.c
 * Copyright (C) 2016 Magehand LLC
 *
 * This module implements a variant of xorshift-star, originally created by Sebastiano Vigna.  There is also an unused
 * Mersenne Twister implementation in here.
 *
 * Global Functions:
 *   initRand           Initializes a random number generator
 *   destroyRand        Closes a random number generator
 *   seedRand           Seeds a random number generator
 *   getRand            Generates a random integer from 0 to 2^64 - 1
 *   getRandUInt        Generates a random unsigned integer between two values, inclusive
 *   getRandInt         Generates a random signed integer between two values, inclusive
 *   getRandDouble      Generates a random double-precision float between two values, inclusive
 *   getRandFloat       Generates a random single-precision float between two values, inclusive
 *
 * Local Functions:
 *   xssSeed            Initializes a xorshift* generator from a seed
 *   xssGen             Generates a value with a xorshift* generator
 *
 * Unused Functions:
 *   mtSeed             Initializes a Mersenne Twister generator from a seed
 *   mtExtract          Extracts a tempered value based on a Mersenne Twister generator (calls mtTwist periodically)
 *   mtTwist            Generate the next set of values for a Mersenne Twister generator
 *********************************************************************************************************************/

/** SA Headers **/
#include "utils/rand.h"
#include "common.h"

/**
 ** Typedefs
 **/

typedef struct _XorShiftStar XorShiftStar_t;
typedef struct _XorShiftStarAtomic XorShiftStarAtomic_t;

/**
 ** Prototypes
 **/

static void xssSeed(
  XorShiftStar_t *gen,
  uint64_t seed);

static uint64_t xssGen(
  XorShiftStar_t *gen);

/**
 ** Global Functions
 **/

void initRand(
  Rand_t *r,
  RandType_t type)
{
  switch (type) {
    case RAND_TYPE_XORSHIFT_STAR: {
      memset(&(r->xss), 0, sizeof(XorShiftStar_t));
      r->type = type;
      break;
    }

    case RAND_TYPE_XORSHIFT_STAR_ATOMIC: {
      memset(&(r->xssa), 0, sizeof(XorShiftStarAtomic_t));
      pthread_spin_init(&(r->xssa.lock), PTHREAD_PROCESS_PRIVATE);
      r->type = type;
      break;
    }

    default: {
      return;
    }
  }
}

void destroyRand(
  Rand_t *r)
{
  switch (r->type) {
    case RAND_TYPE_XORSHIFT_STAR: {
      r->type = RAND_TYPE_NONE;
      break;
    }

    case RAND_TYPE_XORSHIFT_STAR_ATOMIC: {
      r->type = RAND_TYPE_NONE;
      pthread_spin_destroy(&(r->xssa.lock));
      break;
    }

    default: {
      return;
    }
  }
}

void seedRand(
  Rand_t *r,
  uint64_t seed)
{
  switch (r->type) {
    case RAND_TYPE_XORSHIFT_STAR: {
      xssSeed(&(r->xss), seed);
      break;
    }

    case RAND_TYPE_XORSHIFT_STAR_ATOMIC: {
      pthread_spin_lock(&(r->xssa.lock));
      xssSeed(&(r->xssa.xss), seed);
      pthread_spin_unlock(&(r->xssa.lock));
      break;
    }

    default: {
      return;
    }
  }
}

uint64_t getRand(
  Rand_t *r)
{
  switch (r->type) {
    case RAND_TYPE_XORSHIFT_STAR: {
      return xssGen(&(r->xss));
    }

    case RAND_TYPE_XORSHIFT_STAR_ATOMIC: {
      pthread_spin_lock(&(r->xssa.lock));
      uint64_t result = xssGen(&(r->xssa.xss));
      pthread_spin_unlock(&(r->xssa.lock));
      return result;
    }

    default: {
      return 0;
    }
  }
}

/* This method is not strictly accurate, but it is very fast - that means it's good for video games! */
uint64_t getRandUInt(
  Rand_t *r,
  uint64_t a,
  uint64_t b)
{
  uint64_t raw = getRand(r);

  /* If the range is *exactly* zero, we have a degenerate case */
  uint64_t range = ABS(b - a) + 1;
  if (range == 0)
    return a;

  return MIN(a, b) + (raw % range);
}

/* This method is not strictly accurate, but it is very fast - that means it's good for video games! */
int64_t getRandInt(
  Rand_t *r,
  int64_t a,
  int64_t b)
{
  uint64_t raw = getRand(r);

  /* If the range is *exactly* zero, we have a degenerate case */
  uint64_t range = ABS(b - a) + 1;
  if (range == 0)
    return a;

  return MIN(a, b) + (raw % range);
}

double getRandDouble(
  Rand_t *r,
  double a,
  double b)
{
  /* If the range is zero, we have a degenerate case */
  double range = b - a;
  if (WITHIN_EPSILON(range, 0.0, 1e-7))
    return a;

  uint64_t raw = getRand(r);
  return a + (raw * (range / UINT64_MAX));
}

float getRandFloat(
  Rand_t *r,
  float a,
  float b)
{
  /* If the range is zero, we have a degenerate case */
  float range = b - a;
  if (WITHIN_EPSILON(range, 0.0f, 1e-7))
    return a;

  uint64_t raw = getRand(r);
  return a + (raw * (range / UINT64_MAX));
}

/**
 ** Local Functions
 **/

/* Not the highest-quality seed function in the world (read: pulled numbers out of my ass), but this *is* just a video
 * game.
 */
static void xssSeed(
  XorShiftStar_t *gen,
  uint64_t seed)
{
  gen->p = 0;
  gen->s[0] = seed;
  for (size_t i = 1; i < 16; i++)
    gen->s[i] = UINT64_C(252738775724052) * (gen->s[i - 1] ^ (gen->s[i - 1] >> 32)) + i;
}

static uint64_t xssGen(
  XorShiftStar_t *gen)
{
  const uint64_t s0 = gen->s[gen->p];
  gen->p = (gen->p + 1) & 15;
  uint64_t s1 = gen->s[gen->p];
  s1 ^= s1 << 31;
  gen->s[gen->p] = s1 ^ s0 ^ (s1 >> 11) ^ (s0 >> 30);
  return gen->s[gen->p] * UINT64_C(1181783497276652981);
}

/**
 ** Unused
 **/

#if 0
#define MT_W (UINT64_C(64))
#define MT_N (UINT64_C(312))
#define MT_M (UINT64_C(156))
#define MT_R (UINT64_C(31))
#define MT_A (0xB5026F5AA96619E9)
#define MT_U (UINT64_C(29))
#define MT_D (0x5555555555555555)
#define MT_S (UINT64_C(17))
#define MT_B (0x71D67FFFEDA60000)
#define MT_T (UINT64_C(37))
#define MT_C (0xFFF7EEE000000000)
#define MT_L (UINT64_C(43))
#define MT_F (UINT64_C(6364136223846793005))

typedef struct {
  uint64_t mt[MT_N];
  size_t index;
}MT_t;

static const uint64_t lowerMask = (UINT64_C(1) << MT_R) - 1;
static const uint64_t upperMask = ~((UINT64_C(1) << MT_R) - 1);

static void mtSeed(
    MT_t *gen,
    uint64_t seed)
{
  gen->index = MT_N;
  gen->mt[0] = seed;
  for (size_t i = 1; i < MT_N; i++)
  gen->mt[i] = MT_F * (gen->mt[i - 1] ^ (gen->mt[i - 1] >> (MT_W - 2))) + i;
}

static uint64_t mtExtract(
    MT_t *gen)
{
  if (gen->index >= MT_N)
  mtTwist(gen);

  uint64_t y = gen->mt[gen->index];
  y ^= (y >> MT_U) & MT_D;
  y ^= (y >> MT_S) & MT_B;
  y ^= (y >> MT_T) & MT_C;
  y ^= y >> MT_L;

  gen->index++;
  return y;
}

static void mtTwist(
    MT_t *gen)
{
  for (size_t i = 0; i < MT_N; i++) {
    uint64_t x = (gen->mt[i] & upperMask) + (gen->mt[(i + 1) % MT_N] & lowerMask);
    uint64_t xA = x >> 1;
    if (x % 2)
    xA ^= MT_A;
    gen->mt[i] = gen->mt[(i + MT_M) % MT_N] ^ xA;
  }
  gen->index = 0;
}
#endif
