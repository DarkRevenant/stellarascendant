/**********************************************************************************************************************
 * test.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   testPool                Test out object pool functionality
 *   testScheduler           Test out scheduler functionality
 *   testDDS                 Test out DDS loading
 *   testSIMD                Test out SIMD timings
 *
 * Local Functions:
 *   stressObjectPool        Stress the object pool system
 *   stressFastTasks         Stress the scheduler with fast tasks
 *   stressSlowTasks         Stress the scheduler with slow tasks
 *   stressVariableTasks     Stress the scheduler with variable tasks
 *   stressTaskScaling       Stress the scheduler with different groups of tasks
 *   stressUncompressedDDS   Stress the DDS loader on uncompressed textures
 *   stressCompressedDDS     Stress the DDS loader on compressed textures
 *   stressDot2f             Stress the dot2 vector operations
 *   stressRotate2f          Stress the rotate2 vector operations
 *
 * Helper Functions:
 *   testCreateObject        Basic object creator for testing the object pool system
 *   testDestroyObject       Basic object destroyer for testing the object pool system
 *   testFastTask            Basic fast task for testing the scheduler system
 *   testSlowTask            Basic slow task for testing the scheduler system
 *   testVariableTask        Basic variable-speed task for testing the scheduler system
 *   initIntPool             Initializes an integer pool
 *   closeIntPool            Closes an integer pool
 *   popInt                  Retrieves an unused integer from an integer pool
 *   pushInt                 Returns an integer to an integer pool
 *   initVec8Pool            Initializes a vector pool
 *   closeVec8Pool           Closes a vector pool
 *   popVec8                 Retrieves an unused vector from a vector pool
 *   pushVec8                Returns a vector to a vector pool
 *********************************************************************************************************************/

#include <assert.h>
#include <math.h>

/** SA Headers **/
#include "utils/test.h"
#include "common.h"
#include "main.h"
#include "sched.h"
#include "loading/dds.h"
#include "utils/object.h"
#include "utils/rand.h"

/** Utility Headers **/
#include <vectorial/simd4x4f.h>
#include <vectorial/simd8f.h>

#define _MEASURE_TIME_START() \
  ({                           \
    double _start = getTime();

#define _MEASURE_TIME_END(time) \
    (time) += getTime() - _start; \
  })

#define _DO_X_TIMES(iters) \
  for (size_t _i = 0; _i < (iters); _i++)

#define PRINT_VEC2(vec)                                        \
  ({                                                           \
    logAppend("%ls[0] = (%.3f, %.3f)", TO_WSTRING(vec),        \
              simd2f_index((vec), 0), simd2f_index((vec), 1)); \
  })

#define PRINT_VEC2x2(vec)                                                        \
  ({                                                                             \
    for (size_t _i = 0; _i < 2; _i++)                                            \
      logAppend("%ls[%d] = (%.3f, %.3f)", TO_WSTRING(vec), _i,                   \
                simd4f_index((vec), _i * 2), simd4f_index((vec), (_i * 2) + 1)); \
  })

#define PRINT_VEC2x4(vec)                                                        \
  ({                                                                             \
    for (size_t _i = 0; _i < 4; _i++)                                            \
      logAppend("%ls[%d] = (%.3f, %.3f)", TO_WSTRING(vec), _i,                   \
                simd8f_index((vec), _i * 2), simd8f_index((vec), (_i * 2) + 1)); \
  })

#define PRINT_VEC4(vec)                                             \
  ({                                                                \
    logAppend("%ls[0] = (%.3f, %.3f, %.3f, %.3f)", TO_WSTRING(vec), \
              simd4f_index((vec), 0), simd4f_index((vec), 1),       \
              simd4f_index((vec), 2), simd4f_index((vec), 3));      \
  })

#define PRINT_VEC4x2(vec)                                                              \
  ({                                                                                   \
    for (size_t _i = 0; _i < 2; _i++)                                                  \
      logAppend("%ls[%d] = (%.3f, %.3f, %.3f, %.3f)", TO_WSTRING(vec), _i,             \
                simd8f_index((vec), _i * 4),       simd8f_index((vec), (_i * 4) + 1),  \
                simd8f_index((vec), (_i * 4) + 2), simd8f_index((vec), (_i * 4) + 3)); \
  })

#define PRINT_TIMINGS_MSEC(id, time, ops)                          \
  ({                                                               \
    logAppend("%ls: %.3f msec, %0.3f msec per operation", id,      \
              SEC_TO_MSEC(time), ((time) / (ops)) * MSEC_PER_SEC); \
  })

#define PRINT_TIMINGS_USEC(id, time, ops)                          \
  ({                                                               \
    logAppend("%ls: %.3f msec, %0.3f μsec per operation", id,      \
              SEC_TO_MSEC(time), ((time) / (ops)) * USEC_PER_SEC); \
  })

#define PRINT_TIMINGS_NSEC(id, time, ops)                          \
  ({                                                               \
    logAppend("%ls: %.3f msec, %0.3f nanos per operation", id,     \
              SEC_TO_MSEC(time), ((time) / (ops)) * NSEC_PER_SEC); \
  })

#define PRINT_VECTOR_TIMINGS(id, time, ops, vecSize)                         \
  ({                                                                         \
    logAppend("%ls: %.3f msec, %0.3f nanos per vector", id,                  \
              SEC_TO_MSEC(time), ((time) / (ops)) * NSEC_PER_SEC / vecSize); \
  })

/**
 ** Typedefs and Definitions
 **/

typedef OBJECT_POOL(int)
IntPool_t;

typedef OBJECT_POOL(simd8f)
Vec8Pool_t;

/**
 ** Prototypes
 **/

static void stressObjectPool(
  void);

static void stressFastTasks(
  void);

static void stressSlowTasks(
  void);

static void stressVariableTasks(
  void);

static void stressTaskScaling(
  void);

static void stressUncompressedDDS(
  void);

static void stressCompressedDDS(
  void);

static void stressDot2f(
  void);

static void stressRotate2f(
  void);

static int testCreateObject(
  simd8f *obj);

static int testDestroyObject(
  simd8f *obj);

static void* testFastTask(
  void *arg);

static void* testSlowTask(
  void *arg);

static void* testVariableTask(
  void *arg);

DECLARE_POOL_FUNCS(int, IntPool_t, static, initIntPool, closeIntPool, popInt, pushInt);

DECLARE_POOL_FUNCS(simd8f, Vec8Pool_t, static, initVec8Pool, closeVec8Pool, popVec8, pushVec8);

/**
 ** Global Functions
 **/

void testPool(
  void)
{
  int err;
  IntPool_t pool = { NULL, NULL, 1000, 0, SA_FALSE, "Test" };

  logMsg(LOG_INFO, "Testing object pools...");

  err = initIntPool(&pool, 10);
  if (SA_SUCCESS != err)
    logMsg(LOG_ERROR, "Object pool test failed: (%d) %ls", err, getError(err));

  int *result = popInt(&pool);
  if (!result)
    logMsg(LOG_ERROR, "Object pool test failed: (%d) %ls", err, getError(err));

  err = pushInt(&pool, result);
  if (SA_SUCCESS != err)
    logMsg(LOG_ERROR, "Object pool test failed: (%d) %ls", err, getError(err));

  err = closeIntPool(&pool);
  if (SA_SUCCESS != err)
    logMsg(LOG_ERROR, "Object pool test failed: (%d) %ls", err, getError(err));

#if 1
  stressObjectPool();
#endif
}

void testScheduler(
  void)
{
  logMsg(LOG_INFO, "Testing scheduler...");

#if 0
  stressFastTasks();
#endif
#if 0
  stressSlowTasks();
#endif
#if 0
  stressVariableTasks();
#endif
#if 1
  stressTaskScaling();
#endif
}

void testDDS(
  void)
{
  int err;
  wchar_t *filePath;
  SA_Image_t *testImage;

  logMsg(LOG_INFO, "Testing DDS...");

  filePath = BUILD_PATH(baseDir, SA_TEST_DIR_PATH, W("utest2048x2048rgb.dds"));
  err = ddsLoad(filePath, &testImage);
  if (filePath)
    free(filePath);
  if (SA_SUCCESS != err)
    logMsg(LOG_ERROR, "DDS test failed for %ls: (%d) %ls", filePath, err, getError(err));
  else
    freeImage(testImage);

  filePath = BUILD_PATH(baseDir, SA_TEST_DIR_PATH, W("ctest2048x2048dxt1.dds"));
  err = ddsLoad(filePath, &testImage);
  if (filePath)
    free(filePath);
  if (SA_SUCCESS != err)
    logMsg(LOG_ERROR, "DDS test failed for %ls: (%d) %ls", filePath, err, getError(err));
  else
    freeImage(testImage);

#if 1
  stressUncompressedDDS();
#endif
#if 1
  stressCompressedDDS();
#endif
}

void testSIMD(
  void)
{
  logMsg(LOG_INFO, "Testing SIMD...");

#if 1
  stressDot2f();
#endif
#if 1
  stressRotate2f();
#endif
}

/**
 ** Local Functions
 **/

static void stressObjectPool(
  void)
{
#define TEST_OPS        (1000000000LL)
#define TEST_WIDTH      (500000LL)
#define TEST_THIN_WIDTH (TEST_WIDTH / 10000L)
#define TEST_ITERS      (TEST_OPS / TEST_WIDTH)

  int logPoolResizePrev = logPoolResize;
  logPoolResize = SA_FALSE;

  Vec8Pool_t fatPool = { testCreateObject, testDestroyObject, TEST_WIDTH, SIMD8F_ALIGNMENT, SA_FALSE,
    "Fat/Safe" };
  Vec8Pool_t thinPool = { testCreateObject, testDestroyObject, TEST_THIN_WIDTH, SIMD8F_ALIGNMENT, SA_TRUE,
    "Thin/Unsafe" };
  simd8f *store[TEST_WIDTH];

  logMsg(LOG_INFO, "Pool memory before init = %'llu bytes", POOL_MEM);

  initVec8Pool(&fatPool, 1);

  logMsg(LOG_INFO, "Pool memory after init = %'llu bytes", POOL_MEM);

  double timeFatPool = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      store[i] = popVec8(&fatPool);
    }
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      pushVec8(&fatPool, store[i]);
    }
  }
  _MEASURE_TIME_END(timeFatPool);

  logMsg(LOG_INFO, "Pool memory after test = %'llu bytes", POOL_MEM);

  _START_logMsg(LOG_INFO, "Final Result:");
  PRINT_VEC2x4(*(store[0]));
  _END_logMsg();

  closeVec8Pool(&fatPool);

  logMsg(LOG_INFO, "Pool memory before init = %'llu bytes", POOL_MEM);

  initVec8Pool(&thinPool, 1);

  logMsg(LOG_INFO, "Pool memory after init = %'llu bytes", POOL_MEM);

  double timeThinPool = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      store[i] = popVec8(&thinPool);
    }
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      pushVec8(&thinPool, store[i]);
    }
  }
  _MEASURE_TIME_END(timeThinPool);

  _START_logMsg(LOG_INFO, "Final Result:");
  PRINT_VEC2x4(*(store[0]));
  _END_logMsg();

  logMsg(LOG_INFO, "Pool memory after test = %'llu bytes", POOL_MEM);

  closeVec8Pool(&thinPool);

  logMsg(LOG_INFO, "Pool memory after close = %'llu bytes", POOL_MEM);

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_TIMINGS_NSEC(W("fatPool"), timeFatPool, TEST_OPS);
  PRINT_TIMINGS_NSEC(W("thinPool"), timeThinPool, TEST_OPS);
  _END_logMsg();

  logPoolResize = logPoolResizePrev;

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_THIN_WIDTH
#undef TEST_ITERS
}

static void stressFastTasks(
  void)
{
#define TEST_OPS   (3000000LL)
#define TEST_WIDTH (10000LL)
#define TEST_ITERS (TEST_OPS / TEST_WIDTH)

  void *result[TEST_WIDTH];
  Future_t *future[TEST_WIDTH];

  double timeSaturated = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      result[i] = D2V(0);
      future[i] = queueTask(testFastTask, D2V(i), TASK_TIME_CRITICAL);
    }
    flushQueue();
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      bindFuture(future[i], &(result[i]));
    }
  }
  _MEASURE_TIME_END(timeSaturated);

  _START_logMsg(LOG_INFO, "Final Result:");
  logAppend("%d", V2D(result[0]));
  _END_logMsg();

  double timeSequential = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      result[i] = D2V(0);
      future[i] = queueTask(testFastTask, D2V(i), TASK_TIME_CRITICAL);
      flushQueue();
      bindFuture(future[i], &(result[i]));
    }
  }
  _MEASURE_TIME_END(timeSequential);

  _START_logMsg(LOG_INFO, "Final Result:");
  logAppend("%d", V2D(result[0]));
  _END_logMsg();

  double timeMono = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      result[i] = testFastTask(D2V(i));
    }
  }
  _MEASURE_TIME_END(timeMono);

  _START_logMsg(LOG_INFO, "Final Result:");
  logAppend("%d", V2D(result[0]));
  _END_logMsg();

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_TIMINGS_USEC(W("saturated"), timeSaturated, TEST_OPS);
  PRINT_TIMINGS_USEC(W("sequential"), timeSequential, TEST_OPS);
  PRINT_TIMINGS_NSEC(W("mono"), timeMono, TEST_OPS);
  logAppend("overhead: %0.3f μsec per operation", ((timeSaturated - timeMono) / TEST_OPS) * USEC_PER_SEC);
  logAppend("latency: %0.3f μsec per operation", ((timeSequential - timeMono) / TEST_OPS) * USEC_PER_SEC);
  _END_logMsg();

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static void stressSlowTasks(
  void)
{
#define TEST_OPS   (800LL)
#define TEST_WIDTH (100LL)
#define TEST_ITERS (TEST_OPS / TEST_WIDTH)

  void *result[TEST_WIDTH];
  Future_t *future[TEST_WIDTH];

  double timeScheduled = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS * workerCount)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      result[i] = D2V(0);
      future[i] = queueTask(testSlowTask, D2V(i), TASK_HIGH_PRIORITY);
    }
    flushQueue();
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      bindFuture(future[i], &(result[i]));
    }
  }
  _MEASURE_TIME_END(timeScheduled);

  _START_logMsg(LOG_INFO, "Final Result:");
  logAppend("%d", V2D(result[0]));
  _END_logMsg();

  double timeMono = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      result[i] = testSlowTask(D2V(i));
    }
  }
  _MEASURE_TIME_END(timeMono);

  _START_logMsg(LOG_INFO, "Final Result:");
  logAppend("%d", V2D(result[0]));
  _END_logMsg();

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_TIMINGS_MSEC(W("scheduled"), timeScheduled, TEST_OPS * workerCount);
  PRINT_TIMINGS_MSEC(W("mono"), timeMono, TEST_OPS);
  logAppend("scaling: %0.2f× scaling", timeMono / (timeScheduled / workerCount));
  _END_logMsg();

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static void stressVariableTasks(
  void)
{
#define TEST_OPS    (2800LL)
#define TEST_WIDTH  (10LL)
#define TEST_HEIGHT (8LL)
#define TEST_ITERS  (TEST_OPS / (TEST_WIDTH * TEST_HEIGHT))

  const TaskPriority_t schedule[TEST_HEIGHT] = {
    TASK_HIGH_PRIORITY, TASK_LOW_PRIORITY, TASK_HIGH_PRIORITY, TASK_MEDIUM_PRIORITY,
    TASK_HIGH_PRIORITY, TASK_MEDIUM_PRIORITY, TASK_HIGH_PRIORITY, TASK_TIME_CRITICAL
  };

  const size_t opCost[TEST_HEIGHT] = {
    5E4, 5E5, 5E4, 1E5,
    5E4, 1E5, 5E4, 1E5
  };

  void *result[TEST_WIDTH][TEST_HEIGHT];
  Future_t *future[TEST_WIDTH][TEST_HEIGHT];

  double timeScheduled = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS * workerCount)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      for (size_t j = 0; j < TEST_HEIGHT; j++) {
        result[i][j] = D2V(0);
        future[i][j] = queueTask(testVariableTask, D2V(opCost[j]), schedule[j]);
      }
    }
    flushQueue();
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      for (size_t j = 0; j < TEST_HEIGHT; j++) {
        bindFuture(future[i][j], &(result[i][j]));
      }
    }
  }
  _MEASURE_TIME_END(timeScheduled);

  _START_logMsg(LOG_INFO, "Final Results:");
  for (size_t j = 0; j < TEST_HEIGHT; j++) {
    logAppend("%d", V2D(result[0][j]));
  }
  _END_logMsg();

  double timeMono = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      for (size_t j = 0; j < TEST_HEIGHT; j++) {
        result[i][j] = testVariableTask(D2V(opCost[j]));
      }
    }
  }
  _MEASURE_TIME_END(timeMono);

  _START_logMsg(LOG_INFO, "Final Results:");
  for (size_t j = 0; j < TEST_HEIGHT; j++) {
    logAppend("%d", V2D(result[0][j]));
  }
  _END_logMsg();

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_TIMINGS_MSEC(W("scheduled"), timeScheduled, TEST_OPS * workerCount);
  PRINT_TIMINGS_MSEC(W("mono"), timeMono, TEST_OPS);
  logAppend("scaling: %0.2f× scaling", timeMono / (timeScheduled / workerCount));
  _END_logMsg();

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_HEIGHT
#undef TEST_ITERS
}

static void stressTaskScaling(
  void)
{
#define TEST_OPS   (1000LL)
#define TEST_ITERS (TEST_OPS)

  const long opCost = 2E5;

  void **result = alloca(workerCount * sizeof(void*));
  Future_t **future = alloca(workerCount * sizeof(Future_t*));

  double *timeScheduled = alloca(workerCount * sizeof(double));
  memset(timeScheduled, 0, workerCount * sizeof(double));

  for (size_t n = 0; n < workerCount; n++) {
    _MEASURE_TIME_START();
    _DO_X_TIMES(TEST_ITERS)
    {
      for (size_t i = 0; i <= n; i++) {
        result[i] = D2V(0);
        future[i] = queueTask(testVariableTask, D2V(opCost), TASK_HIGH_PRIORITY);
      }
      flushQueue();
      for (size_t i = 0; i <= n; i++) {
        bindFuture(future[i], &(result[i]));
      }
    }
    _MEASURE_TIME_END(timeScheduled[n]);

    _START_logMsg(LOG_INFO, "Final Result:");
    logAppend("%d", V2D(result[0]));
    _END_logMsg();
  }

  double timeMono = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < 1; i++) {
      result[i] = testVariableTask(D2V(opCost));
    }
  }
  _MEASURE_TIME_END(timeMono);

  _START_logMsg(LOG_INFO, "Final Result:");
  logAppend("%d", V2D(result[0]));
  _END_logMsg();

  _START_logMsg(LOG_INFO, "Timings:");
  for (size_t n = 0; n < workerCount; n++) {
    wchar_t buf[20];
    _swprintf(buf, W("sched-%d×"), n + 1);
    PRINT_TIMINGS_MSEC(buf, timeScheduled[n], TEST_OPS * (n + 1));
    logAppend("%ls: %0.2f× scaling", buf, timeMono / (timeScheduled[n] / (n + 1)));
  }
  PRINT_TIMINGS_MSEC(W("mono"), timeMono, TEST_OPS);
  _END_logMsg();

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static void stressUncompressedDDS(
  void)
{
#define TEST_OPS   (1000LL)
#define TEST_WIDTH (50LL)
#define TEST_ITERS (TEST_OPS / TEST_WIDTH)

  int err;
  wchar_t *filePath;
  SA_Image_t *testImages[TEST_WIDTH];
  memset(testImages, 0, sizeof(testImages));

  filePath = BUILD_PATH(baseDir, SA_TEST_DIR_PATH, W("utest2048x2048rgb.dds"));

  logMsg(LOG_INFO, "Image memory before test = %'llu bytes", IMAGE_MEM);

  double time2048x2048rgb = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      err = ddsLoad(filePath, &testImages[i]);
      if (SA_SUCCESS != err) {
        goto exit;
      }
    }
    if (_i == 0) {
      logMsg(LOG_INFO, "Peak image memory = %'llu bytes", IMAGE_MEM);
    }
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      freeImage(testImages[i]);
      testImages[i] = NULL;
    }
  }
  _MEASURE_TIME_END(time2048x2048rgb);

  logMsg(LOG_INFO, "Image memory after test = %'llu bytes", IMAGE_MEM);

  if (filePath)
    free(filePath);

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_TIMINGS_MSEC(W("2048x2048rgb"), time2048x2048rgb, TEST_OPS);
  _END_logMsg();

  exit:
  for (size_t i = 0; i < TEST_WIDTH; i++) {
    if (testImages[i])
      freeImage(testImages[i]);
  }

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static void stressCompressedDDS(
  void)
{
#define TEST_OPS   (2000LL)
#define TEST_WIDTH (400LL)
#define TEST_ITERS (TEST_OPS / TEST_WIDTH)

  int err;
  wchar_t *filePath;
  SA_Image_t *testImages[TEST_WIDTH];
  memset(testImages, 0, sizeof(testImages));

  filePath = BUILD_PATH(baseDir, SA_TEST_DIR_PATH, W("ctest2048x2048dxt1.dds"));

  logMsg(LOG_INFO, "Image memory before test = %'llu bytes", IMAGE_MEM);

  double time2048x2048dxt1 = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      err = ddsLoad(filePath, &testImages[i]);
      if (SA_SUCCESS != err) {
        goto exit;
      }
    }
    if (_i == 0) {
      logMsg(LOG_INFO, "Peak image memory = %'llu bytes", IMAGE_MEM);
    }
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      freeImage(testImages[i]);
      testImages[i] = NULL;
    }
  }
  _MEASURE_TIME_END(time2048x2048dxt1);

  logMsg(LOG_INFO, "Image memory after test = %'llu bytes", IMAGE_MEM);

  if (filePath)
    free(filePath);

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_TIMINGS_MSEC(W("2048x2048dxt1"), time2048x2048dxt1, TEST_OPS);
  _END_logMsg();

  exit:
  for (size_t i = 0; i < TEST_WIDTH; i++) {
    if (testImages[i])
      freeImage(testImages[i]);
  }

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static void stressDot2f(
  void)
{
#define TEST_OPS   (1000000000LL)
#define TEST_WIDTH (2500LL)
#define TEST_ITERS (TEST_OPS / TEST_WIDTH)

  simd4f *test1xA = _aligned_malloc(TEST_WIDTH * sizeof(simd2f), SIMD2F_ALIGNMENT);
  simd4f *test1xB = _aligned_malloc(TEST_WIDTH * sizeof(simd2f), SIMD2F_ALIGNMENT);
  simd4f *test2xA = _aligned_malloc(TEST_WIDTH * sizeof(simd4f), SIMD4F_ALIGNMENT);
  simd4f *test2xB = _aligned_malloc(TEST_WIDTH * sizeof(simd4f), SIMD4F_ALIGNMENT);
  simd4f *test2xC = _aligned_malloc(TEST_WIDTH * sizeof(simd4f), SIMD4F_ALIGNMENT);
  simd4f *test2xD = _aligned_malloc(TEST_WIDTH * sizeof(simd4f), SIMD4F_ALIGNMENT);
  simd8f *test4xA = _aligned_malloc(TEST_WIDTH * sizeof(simd8f), SIMD8F_ALIGNMENT);
  simd8f *test4xB = _aligned_malloc(TEST_WIDTH * sizeof(simd8f), SIMD8F_ALIGNMENT);
  simd8f *test4xC = _aligned_malloc(TEST_WIDTH * sizeof(simd8f), SIMD8F_ALIGNMENT);
  simd8f *test4xD = _aligned_malloc(TEST_WIDTH * sizeof(simd8f), SIMD8F_ALIGNMENT);

  for (size_t i = 0; i < TEST_WIDTH; i++) {
    double x = (double) (i + 1);
    test1xA[i] = simd2f_create(x, -x);
    test1xB[i] = simd2f_create(x + 1, -x + 1);
    test2xA[i] = simd4f_create(x, -x, x + 1, -x + 1);
    test2xB[i] = simd4f_create(x + 2, -x + 2, x + 3, -x + 3);
    test2xC[i] = simd4f_create(x, -x, x + 1, -x + 1);
    test2xD[i] = simd4f_create(x + 2, -x + 2, x + 3, -x + 3);
    test4xA[i] = simd8f_create(x, -x, x + 1, -x + 1, x + 2, -x + 2, x + 3, -x + 3);
    test4xB[i] = simd8f_create(x + 4, -x + 4, x + 5, -x + 5, x + 6, -x + 6, x + 7, -x + 7);
    test4xC[i] = simd8f_create(x, -x, x + 1, -x + 1, x + 2, -x + 2, x + 3, -x + 3);
    test4xD[i] = simd8f_create(x + 4, -x + 4, x + 5, -x + 5, x + 6, -x + 6, x + 7, -x + 7);
  }

  _START_logMsg(LOG_INFO, "Initial Vector States:");
  PRINT_VEC2(test1xA[0]);
  PRINT_VEC2(test1xB[0]);
  PRINT_VEC2x2(test2xA[0]);
  PRINT_VEC2x2(test2xB[0]);
  PRINT_VEC4(test2xC[0]);
  PRINT_VEC4(test2xD[0]);
  PRINT_VEC2x4(test4xA[0]);
  PRINT_VEC2x4(test4xB[0]);
  PRINT_VEC4x2(test4xC[0]);
  PRINT_VEC4x2(test4xD[0]);
  _END_logMsg();

  double time1x2 = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test1xA[i] = simd2f_dot2(test1xA[i], test1xB[i]);
    }
  }
  _MEASURE_TIME_END(time1x2);

  double time2x2 = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test2xA[i] = simd4f_dot2x2(test2xA[i], test2xB[i]);
    }
  }
  _MEASURE_TIME_END(time2x2);

  double time2x4 = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test2xC[i] = simd4f_dot4(test2xC[i], test2xD[i]);
    }
  }
  _MEASURE_TIME_END(time2x4);

  double time4x2 = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test4xA[i] = simd8f_dot2x4(test4xA[i], test4xB[i]);
    }
  }
  _MEASURE_TIME_END(time4x2);

  double time4x4 = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test4xC[i] = simd8f_dot2x4(test4xC[i], test4xD[i]);
    }
  }
  _MEASURE_TIME_END(time4x4);

  _START_logMsg(LOG_INFO, "Final Vector States:");
  PRINT_VEC2(test1xA[0]);
  PRINT_VEC2(test1xB[0]);
  PRINT_VEC2x2(test2xA[0]);
  PRINT_VEC2x2(test2xB[0]);
  PRINT_VEC4(test2xC[0]);
  PRINT_VEC4(test2xD[0]);
  PRINT_VEC2x4(test4xA[0]);
  PRINT_VEC2x4(test4xB[0]);
  PRINT_VEC4x2(test4xC[0]);
  PRINT_VEC4x2(test4xD[0]);
  _END_logMsg();

  _aligned_free(test1xA);
  _aligned_free(test1xB);
  _aligned_free(test2xA);
  _aligned_free(test2xB);
  _aligned_free(test2xC);
  _aligned_free(test2xD);
  _aligned_free(test4xA);
  _aligned_free(test4xB);
  _aligned_free(test4xC);
  _aligned_free(test4xD);

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_VECTOR_TIMINGS(W("simd2f"), time1x2, TEST_OPS, 1);
  PRINT_VECTOR_TIMINGS(W("simd2fx2"), time2x2, TEST_OPS, 2);
  PRINT_VECTOR_TIMINGS(W("simd4f"), time2x4, TEST_OPS, 1);
  PRINT_VECTOR_TIMINGS(W("simd2fx4"), time4x2, TEST_OPS, 4);
  PRINT_VECTOR_TIMINGS(W("simd4fx2"), time4x4, TEST_OPS, 2);
  _END_logMsg();

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static void stressRotate2f(
  void)
{
#ifdef _DEBUG
#  define TEST_OPS   (50000000LL)
#  define TEST_WIDTH (5000LL)
#else
#  define TEST_OPS   (2000000000LL)
#  define TEST_WIDTH (5000LL)
#endif
#define TEST_ITERS   (TEST_OPS / TEST_WIDTH)

  simd2f *test1x = _aligned_malloc(TEST_WIDTH * sizeof(simd2f), SIMD2F_ALIGNMENT);
  simd4f *test2x = _aligned_malloc(TEST_WIDTH * sizeof(simd4f), SIMD4F_ALIGNMENT);
  simd8f *test4x = _aligned_malloc(TEST_WIDTH * sizeof(simd8f), SIMD8F_ALIGNMENT);

  for (size_t i = 0; i < TEST_WIDTH; i++) {
    double x = (double) (i + 1);
    test1x[i] = simd2f_create(x, -x);
    test2x[i] = simd4f_create(x, -x, x + 1, -x + 1);
    test4x[i] = simd8f_create(x, -x, x + 1, -x + 1, x + 2, -x + 2, x + 3, -x + 3);
  }

  _START_logMsg(LOG_INFO, "Initial Vector States:");
  PRINT_VEC2(test1x[0]);
  PRINT_VEC2x2(test2x[0]);
  PRINT_VEC2x4(test4x[0]);
  _END_logMsg();

  double time1x = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test1x[i] = simd2f_rotate2(test1x[i], 0.15f);
    }
  }
  _MEASURE_TIME_END(time1x);

  double time2x = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test2x[i] = simd4f_rotate2x2(test2x[i], 0.15f);
    }
  }
  _MEASURE_TIME_END(time2x);

  double time4x = 0.0;
  _MEASURE_TIME_START();
  _DO_X_TIMES(TEST_ITERS)
  {
    for (size_t i = 0; i < TEST_WIDTH; i++) {
      test4x[i] = simd8f_rotate2x4(test4x[i], 0.15f);
    }
  }
  _MEASURE_TIME_END(time4x);

  _START_logMsg(LOG_INFO, "Final Vector States:");
  PRINT_VEC2(test1x[0]);
  PRINT_VEC2x2(test2x[0]);
  PRINT_VEC2x4(test4x[0]);
  _END_logMsg();

  _aligned_free(test1x);
  _aligned_free(test2x);
  _aligned_free(test4x);

  _START_logMsg(LOG_INFO, "Timings:");
  PRINT_VECTOR_TIMINGS(W("simd2f"), time1x, TEST_OPS, 1);
  PRINT_VECTOR_TIMINGS(W("simd2fx2"), time2x, TEST_OPS, 2);
  PRINT_VECTOR_TIMINGS(W("simd2fx4"), time4x, TEST_OPS, 4);
  _END_logMsg();

#undef TEST_OPS
#undef TEST_WIDTH
#undef TEST_ITERS
}

static int testCreateObject(
  simd8f *obj)
{
  *obj = simd8f_splat((float) getSysTime());
  return SA_SUCCESS;
}

static int testDestroyObject(
  simd8f *obj)
{
  *obj = simd8f_zero();
  return SA_SUCCESS;
}

static void* testFastTask(
  void* arg)
{
  long num = V2D(arg);
  return D2V(num + 1);
}

static void* testSlowTask(
  void* arg)
{
  float num = (float) V2D(arg);
  Rand_t r;
  uint64_t t = getSysTime();
  IntPool_t pool = { NULL, NULL, 500000, 0, SA_TRUE, "Pool" };

  initRand(&r, RAND_TYPE_DEFAULT);
  seedRand(&r, t);
  initIntPool(&pool, 1);
  for (size_t i = 0; i < 500000; i++) {
    popInt(&pool);
    num += getRandFloat(&r, -1.0f, 1.0f);
    num += (float) ((double) getSysTime() / (double) t);
    num += (float) pool._head / (float) 500000;
  }

  closeIntPool(&pool);
  return D2V((long ) num + 1);
}

static void* testVariableTask(
  void* arg)
{
  long iters = V2D(arg);
  float num = 0.0f;
  Rand_t r;
  uint64_t t = getSysTime();
  IntPool_t pool = { NULL, NULL, iters, 0, SA_TRUE, "Pool" };

  initRand(&r, RAND_TYPE_DEFAULT);
  seedRand(&r, t);
  initIntPool(&pool, 1);
  for (size_t i = 0; i < iters; i++) {
    popInt(&pool);
    num += getRandFloat(&r, -1.0f, 1.0f);
    num += (float) ((double) getSysTime() / (double) t);
    num += (float) pool._head / (float) iters;
  }

  closeIntPool(&pool);
  return D2V((long ) num + 1);
}

DEFINE_POOL_FUNCS(int, IntPool_t, static, initIntPool, closeIntPool, popInt, pushInt);

DEFINE_POOL_FUNCS(simd8f, Vec8Pool_t, static, initVec8Pool, closeVec8Pool, popVec8, pushVec8);
