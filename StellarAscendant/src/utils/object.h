/**********************************************************************************************************************
 * object.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Template Functions:
 *   initFunc      Initializes an object pool
 *   closeFunc     Closes an object pool
 *   popFunc       Retrieves an unused object from an object pool
 *   pushFunc      Returns an object to an object pool
 *********************************************************************************************************************/

#ifndef ___OBJECT_H___
#define ___OBJECT_H___

#include <stdint.h>

/**
 ** Macros
 **/

/* Byte alignment only works if the size of the object is a multiple of the alignment width */
#define OBJECT_POOL(type)                                     \
  struct {                                                    \
    int (*const createFunc)(type*);                           \
    int (*const destroyFunc)(type*);                          \
    const size_t objectsPerBlock;                             \
    const size_t byteAlignment; /* 0 for default alignment */ \
    const int threadSafe;                                     \
                                                              \
    const char *name; /* For logging purposes */              \
                                                              \
    type             **_stack;                                \
    void             **_mem;                                  \
    size_t             _head;                                 \
    size_t             _size;                                 \
    size_t             _blocks;                               \
    pthread_spinlock_t _lock;                                 \
  }

#define DECLARE_POOL_FUNCS(type, poolType, storage, initFunc, closeFunc, popFunc, pushFunc) \
  storage int initFunc(                                                                     \
    poolType *pool,                                                                         \
    size_t blocks);                                                                         \
                                                                                            \
  storage int closeFunc(                                                                    \
    poolType *pool);                                                                        \
                                                                                            \
  storage type* popFunc(                                                                    \
    poolType *pool);                                                                        \
                                                                                            \
  storage int pushFunc(                                                                     \
    poolType *pool,                                                                         \
    type *obj);

#define DEFINE_POOL_FUNCS(type, poolType, storage, initFunc, closeFunc, popFunc, pushFunc)        \
  storage int initFunc(                                                                           \
    poolType *pool,                                                                               \
    size_t blocks)                                                                                \
  {                                                                                               \
    int rtnVal;                                                                                   \
    int err;                                                                                      \
                                                                                                  \
    if (NULL == pool) {                                                                           \
      rtnVal = SA_ERROR_ARGS;                                                                     \
      goto error;                                                                                 \
    }                                                                                             \
                                                                                                  \
    /* Set members to sane defaults */                                                            \
    pool->_stack  = NULL;                                                                         \
    pool->_mem    = NULL;                                                                         \
    pool->_head   = 0;                                                                            \
    pool->_blocks = blocks;                                                                       \
    pool->_size   = pool->_blocks * pool->objectsPerBlock;                                        \
    if (NULL == pool->name)                                                                       \
      pool->name = #poolType;                                                                     \
                                                                                                  \
    if (pool->threadSafe) {                                                                       \
      /* Create spinlock variable */                                                              \
      err = pthread_spin_init(&(pool->_lock), PTHREAD_PROCESS_PRIVATE);                           \
      if (0 != err) {                                                                             \
        logMsg(LOG_ERROR, "Failed to create spin-lock: (%d) %ls", err, _wcserror(err));           \
        rtnVal = genError(SA_GENERIC_ERROR_THREAD, err);                                          \
        goto error;                                                                               \
      }                                                                                           \
    }                                                                                             \
                                                                                                  \
    /* Allocate an array for the block memory pointers */                                         \
    pool->_mem = calloc(pool->_blocks, sizeof(void*));                                            \
    if (NULL == pool->_mem) {                                                                     \
      rtnVal = SA_ERROR_MEMORY;                                                                   \
      goto error;                                                                                 \
    }                                                                                             \
    LOG_POOL_MEM(pool->_blocks * sizeof(void*));                                                  \
                                                                                                  \
    /* Allocate the object stack */                                                               \
    pool->_stack = malloc(pool->_size * sizeof(type*));                                           \
    if (NULL == pool->_stack) {                                                                   \
      rtnVal = SA_ERROR_MEMORY;                                                                   \
      goto error;                                                                                 \
    }                                                                                             \
    LOG_POOL_MEM(pool->_size * sizeof(type*));                                                    \
                                                                                                  \
    /* Allocate initial blocks */                                                                 \
    for (size_t i = 0; i < pool->_blocks; i++) {                                                  \
      if (pool->byteAlignment <= 0)                                                               \
        pool->_mem[i] = malloc(sizeof(type) * pool->objectsPerBlock);                             \
      else                                                                                        \
        pool->_mem[i] = _aligned_malloc(sizeof(type) * pool->objectsPerBlock,                     \
                                        pool->byteAlignment);                                     \
      if (NULL == pool->_mem[i]) {                                                                \
        rtnVal = SA_ERROR_MEMORY;                                                                 \
        goto error;                                                                               \
      }                                                                                           \
      LOG_POOL_MEM(sizeof(type) * pool->objectsPerBlock);                                         \
                                                                                                  \
      for (size_t j = 0; j < pool->objectsPerBlock; j++) {                                        \
        /* This bullshit gets the memory location of the object we want to create */              \
        type *obj = (type*) &(((uint8_t*) pool->_mem[i])[j * sizeof(type)]);                      \
                                                                                                  \
        /* Fill up the stack */                                                                   \
        pool->_stack[pool->_head] = obj;                                                          \
        pool->_head++;                                                                            \
                                                                                                  \
        if (pool->createFunc) {                                                                   \
          err = pool->createFunc(obj);                                                            \
          if (SA_SUCCESS != err) {                                                                \
            rtnVal = err;                                                                         \
            goto error;                                                                           \
          }                                                                                       \
        }                                                                                         \
      }                                                                                           \
    }                                                                                             \
                                                                                                  \
    return SA_SUCCESS;                                                                            \
                                                                                                  \
    error:                                                                                        \
    closeFunc(pool);                                                                              \
    return rtnVal;                                                                                \
  }                                                                                               \
                                                                                                  \
  storage int closeFunc(                                                                          \
    poolType *pool)                                                                               \
  {                                                                                               \
    if (NULL == pool)                                                                             \
      return SA_SUCCESS;                                                                          \
                                                                                                  \
    if (pool->_stack) {                                                                           \
      free(pool->_stack);                                                                         \
      LOG_POOL_MEM(-(pool->_size * sizeof(type*)));                                               \
    }                                                                                             \
                                                                                                  \
    /* Deallocate memory blocks */                                                                \
    if (pool->_mem) {                                                                             \
      for (size_t i = 0; i < pool->_blocks; i++) {                                                \
        if (pool->destroyFunc)                                                                    \
          for (size_t j = 0; j < pool->objectsPerBlock; j++) {                                    \
            /* This gets the memory location of the object we want to destroy, even if it has */  \
            /* been popped off or otherwise lost.                                             */  \
            type *obj = (type*) &(((uint8_t*) pool->_mem[i])[j * sizeof(type)]);                  \
            pool->destroyFunc(obj);                                                               \
          }                                                                                       \
                                                                                                  \
        if (pool->_mem[i]) {                                                                      \
          if (pool->byteAlignment <= 0)                                                           \
            free(pool->_mem[i]);                                                                  \
          else                                                                                    \
            _aligned_free(pool->_mem[i]);                                                         \
          LOG_POOL_MEM(-(sizeof(type) * pool->objectsPerBlock));                                  \
        }                                                                                         \
      }                                                                                           \
                                                                                                  \
      free(pool->_mem);                                                                           \
      LOG_POOL_MEM(-(pool->_blocks * sizeof(void*)));                                             \
    }                                                                                             \
                                                                                                  \
    pool->_stack = NULL;                                                                          \
    pool->_mem = NULL;                                                                            \
                                                                                                  \
    if (pool->_lock)                                                                              \
      pthread_spin_destroy(&(pool->_lock));                                                       \
    pool->_lock = (pthread_spinlock_t) 0;                                                         \
                                                                                                  \
    return SA_SUCCESS;                                                                            \
  }                                                                                               \
                                                                                                  \
  /* Returned object is not cleared - this is the responsibility of the caller */                 \
  storage type* popFunc(                                                                          \
    poolType *pool)                                                                               \
  {                                                                                               \
    if (NULL == pool)                                                                             \
      return NULL;                                                                                \
                                                                                                  \
    if (pool->threadSafe)                                                                         \
      pthread_spin_lock(&(pool->_lock));                                                          \
                                                                                                  \
    if (pool->_head <= 0) {                                                                       \
      /* Out of memory - allocate a new block! */                                                 \
      pool->_blocks++;                                                                            \
      pool->_size = pool->_blocks * pool->objectsPerBlock;                                        \
      size_t i = pool->_blocks - 1;                                                               \
                                                                                                  \
      /* Resize the block list */                                                                 \
      void **newMem = realloc(pool->_mem, pool->_blocks * sizeof(void*));                         \
      if (NULL == newMem)                                                                         \
        return NULL;                                                                              \
      pool->_mem = newMem;                                                                        \
      LOG_POOL_MEM(1 * sizeof(void*));                                                            \
                                                                                                  \
      /* Resize the object stack - safe, because the stack is empty right now */                  \
      type **newStack = realloc(pool->_stack, pool->_size * sizeof(type*));                       \
      if (NULL == newStack)                                                                       \
        return NULL;                                                                              \
      pool->_stack = newStack;                                                                    \
      LOG_POOL_MEM(pool->objectsPerBlock * sizeof(void*));                                        \
                                                                                                  \
      /* Allocate new block */                                                                    \
      if (pool->byteAlignment <= 0)                                                               \
        pool->_mem[i] = malloc(sizeof(type) * pool->objectsPerBlock);                             \
      else                                                                                        \
        pool->_mem[i] = _aligned_malloc(sizeof(type) * pool->objectsPerBlock,                     \
                                        pool->byteAlignment);                                     \
      if (NULL == pool->_mem[i])                                                                  \
        return NULL;                                                                              \
      LOG_POOL_MEM(sizeof(type) * pool->objectsPerBlock);                                         \
                                                                                                  \
      for (size_t j = 0; j < pool->objectsPerBlock; j++) {                                        \
        /* This gets the memory location of the object we want to create */                       \
        type *obj = (type*) &(((uint8_t*) pool->_mem[i])[j * sizeof(type)]);                      \
                                                                                                  \
        /* Add the new objects to the stack */                                                    \
        pool->_stack[pool->_head] = obj;                                                          \
        pool->_head++;                                                                            \
                                                                                                  \
        if (pool->createFunc) {                                                                   \
          int err = pool->createFunc(obj);                                                        \
          if (SA_SUCCESS != err)                                                                  \
            return NULL;                                                                          \
        }                                                                                         \
      }                                                                                           \
                                                                                                  \
      if (logPoolResize)                                                                          \
        logMsg(LOG_INFO, "%s pool \"%s\" allocated new %d-byte block",                            \
               #type, pool->name, sizeof(type) * pool->objectsPerBlock);                          \
    }                                                                                             \
                                                                                                  \
    type *obj = pool->_stack[--(pool->_head)];                                                    \
                                                                                                  \
    if (pool->threadSafe)                                                                         \
      pthread_spin_unlock(&(pool->_lock));                                                        \
                                                                                                  \
    return obj;                                                                                   \
  }                                                                                               \
                                                                                                  \
  storage int pushFunc(                                                                           \
    poolType *pool,                                                                               \
    type *obj)                                                                                    \
  {                                                                                               \
    if (NULL == pool)                                                                             \
      return SA_ERROR_ARGS;                                                                       \
    if (NULL == obj)                                                                              \
      return SA_ERROR_ARGS;                                                                       \
                                                                                                  \
    if (pool->threadSafe)                                                                         \
      pthread_spin_lock(&(pool->_lock));                                                          \
                                                                                                  \
    /* Sanity check - catch improper use of the object pool */                                    \
    if (pool->_head >= pool->_size)                                                               \
      return SA_ERROR_OP_INVALID;                                                                 \
                                                                                                  \
    pool->_stack[pool->_head++] = obj;                                                            \
                                                                                                  \
    if (pool->threadSafe)                                                                         \
      pthread_spin_unlock(&(pool->_lock));                                                        \
                                                                                                  \
    return SA_SUCCESS;                                                                            \
  }

#endif /* ___OBJECT_H___ */
