/**********************************************************************************************************************
 * main.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Built-in Functions:
 *   wmain              Primary entry point for the program
 *
 * Local Functions:
 *   initMainThread     Entry point for the main thread
 *   exitMainThread     Termination routine for the main thread
 *   runMainLoop        Run the main loop of the program
 *   runMainLoop        Main loop routine
 *   refreshMainConfig  Parse and save the main configuration file
 *   countSetBits       Helper function for getting CPU information
 *   getCPUInfo         Function for getting CPU information
 *********************************************************************************************************************/

#include <float.h>
#include <locale.h>
#include <math.h>
#include <pthread.h>
#include <sched.h>

/** SA Headers **/
#include "main.h"
#include "common.h"
#include "display/display.h"
#include "utils/test.h"

/**
 ** Definitions
 **/

#ifdef _DEBUG
#  define DEFAULT_LOG_SAFE             (SA_TRUE)
#  define DEFAULT_LOG_TO_CONSOLE       (SA_TRUE)
#  define DEFAULT_LOG_TO_FILE          (SA_TRUE)
#  define DEFAULT_LOG_POOL_RESIZE      (SA_TRUE)
#  define DEFAULT_LOG_TIMING           (SA_TRUE)
#  define DEFAULT_LOG_SUPPRESS_DEBUG   (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_INFO    (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_WARNING (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_ERROR   (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_FATAL   (SA_FALSE)
#  define DEFAULT_RUN_TESTS            (SA_FALSE)
#else
#  define DEFAULT_LOG_SAFE             (SA_FALSE)
#  define DEFAULT_LOG_TO_CONSOLE       (SA_TRUE)
#  define DEFAULT_LOG_TO_FILE          (SA_TRUE)
#  define DEFAULT_LOG_POOL_RESIZE      (SA_FALSE)
#  define DEFAULT_LOG_TIMING           (SA_TRUE)
#  define DEFAULT_LOG_SUPPRESS_DEBUG   (SA_TRUE)
#  define DEFAULT_LOG_SUPPRESS_INFO    (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_WARNING (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_ERROR   (SA_FALSE)
#  define DEFAULT_LOG_SUPPRESS_FATAL   (SA_FALSE)
#  define DEFAULT_RUN_TESTS            (SA_TRUE)
#endif

#define SA_CONFIG_NAME      "SAConfig"
#define SA_CONFIG_FILE_NAME W("config.xml")

/**
 ** Global Variables
 **/

int cpuCount = 1;
int maxCPU = 0;
int useHTMode = SA_FALSE;
int skipHTCores = SA_FALSE;
int64_t primaryThreads = 0;
uint64_t primaryAffinityMask = 0x0000000000000000;

int logSafe = DEFAULT_LOG_SAFE;
int logToConsole = DEFAULT_LOG_TO_CONSOLE;
int logToFile = DEFAULT_LOG_TO_FILE;
int logPoolResize = DEFAULT_LOG_POOL_RESIZE;
int logTiming = DEFAULT_LOG_TIMING;
int logSuppressDebug = DEFAULT_LOG_SUPPRESS_DEBUG;
int logSuppressInfo = DEFAULT_LOG_SUPPRESS_INFO;
int logSuppressWarning = DEFAULT_LOG_SUPPRESS_WARNING;
int logSuppressError = DEFAULT_LOG_SUPPRESS_ERROR;
int logSuppressFatal = DEFAULT_LOG_SUPPRESS_FATAL;

double frameRate = 60.00;
double frameTarget = 1.0 / 60.0;

uint64_t currFrame = 0;

/**
 ** Constants
 **/

static const size_t mainThreadStackSize = MBYTES_TO_BYTES(4);
static const int winTimerTargetRes = 1;
static double maxTimeStep = MSEC_TO_SEC(50.00);

/**
 ** Local Variables
 **/

static SA_Config_t SAConfig = { SA_CONFIG_NAME };

static pthread_t *mainThread = NULL;
static pthread_t *displayThread = NULL;
static pthread_t *schedulerThread = NULL;

static int physicalCPUCount = 0;
static int logicalCPUCount = 0;
static int numaNodeCount = 0;
static int processorPackageCount = 0;
static int processorL1CacheCount = 0;
static int processorL2CacheCount = 0;
static int processorL3CacheCount = 0;

static int winTimerRes = -1;

static int runTests = SA_TRUE;
static int cfgTestPool = DEFAULT_RUN_TESTS;
static int cfgTestScheduler = DEFAULT_RUN_TESTS;
static int cfgTestDDS = DEFAULT_RUN_TESTS;
static int cfgTestSIMD = DEFAULT_RUN_TESTS;

/**
 ** Prototypes
 **/

static void* initMainThread(
  void *arg);

static void exitMainThread(
  void);

static int runMainLoop(
  void);

static int mainLoop(
  double dt);

static int refreshMainConfig(
  void);

static int countSetBits(
  ULONG_PTR bitMask);

static int getCPUInfo(
  void);

/**
 ** Built-in Functions
 **/

int wmain(
  int argc,
  wchar_t **argv)
{
  void *rtnVal = D2V(SA_SUCCESS);
  int err;

  pthread_attr_t *mainThreadAttr = alloca(sizeof(pthread_attr_t));
  mainThread = a_malloc(sizeof(pthread_t));

  struct sched_param schedParam;
  schedParam.sched_priority = sched_get_priority_max(SCHED_OTHER);

  /* Set the locale before anything else */
  setlocale(LC_ALL, "");

  /* Initialize global structures */
  err = initCommon();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to initialize common structures: (%d) %ls", err, getError(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }

  /* Print version string and build info */
  _START_logMsg(LOG_INFO, "Stellar Ascendant Alpha - Version %s", STRINGIFY(SA_VERSION));
  logAppend("%s", SA_BUILD_TYPE);
  logAppend("Built %s %s", __DATE__, __TIME__);
  logAppend("");
  logAppend("Copyright (C) Magehand LLC 2016");
  logAppend("All rights reserved");
  logAppend("");
  _END_logMsg();

  /* Set main thread attributes */
  err = pthread_attr_init(mainThreadAttr);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to initialize main thread attribute: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  err = pthread_attr_setdetachstate(mainThreadAttr, PTHREAD_CREATE_JOINABLE);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set main thread attribute joinable: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  err = pthread_attr_setstacksize(mainThreadAttr, mainThreadStackSize);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set main thread stack size to %'d bytes: (%d) %ls",
        mainThreadStackSize, err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  else {
    logMsg(LOG_DEBUG, "Set main thread stack size to %'d bytes", mainThreadStackSize);
  }
  err = pthread_attr_setinheritsched(mainThreadAttr, PTHREAD_EXPLICIT_SCHED);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set main thread scheduling explicit: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  err = pthread_attr_setscope(mainThreadAttr, PTHREAD_SCOPE_PROCESS);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set main thread to process scope: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  /* Only SCHED_OTHER is supported on Windows */
  err = pthread_attr_setschedpolicy(mainThreadAttr, SCHED_OTHER);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set main thread scheduling policy to standard: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  err = pthread_attr_setschedparam(mainThreadAttr, &schedParam);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set main thread scheduling priority to %d: (%d) %ls",
        schedParam.sched_priority, err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  else {
    logMsg(LOG_DEBUG, "Set main thread scheduling priority to %d", schedParam.sched_priority);
  }

  /* This function is Windows-only */
  cpuCount = pthread_num_processors_np();
  logMsg(LOG_DEBUG, "Running on %d processors", cpuCount);

  /* Create main thread */
  err = pthread_create(mainThread, mainThreadAttr, initMainThread, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create main thread: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  pthread_attr_destroy(mainThreadAttr);
  mainThreadAttr = NULL;

  /* Wait for main thread to finish before exiting */
  err = pthread_join(*mainThread, &rtnVal);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to join main thread: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }

  exit:
  if (mainThreadAttr)
    pthread_attr_destroy(mainThreadAttr);
  err = (int) V2D(rtnVal);
  logMsg(LOG_ERROR, "Program exiting with code %d: %ls", err, getError(err));

  /* Close out the common structures at the very end */
  closeCommon();
  return err;
}

/**
 ** Local Functions
 **/

static void* initMainThread(
  void *arg)
{
  wchar_t *tempPath = NULL;
  void *rtnVal = D2V(SA_SUCCESS);
  int err;

  logMsg(LOG_DEBUG, "Main thread started");

  /* Create config directory, if necessary */
  tempPath = BUILD_PATH(baseDir, SA_CONFIG_DIR_PATH);
  err = _wmkdir(tempPath);
  if (tempPath)
    free(tempPath);
  if (0 > err) {
    if (errno != EEXIST) {
      logMsg(LOG_WARNING, "Failed to create config directory at \"%ls\": (%d) %ls",
          SA_CONFIG_DIR_PATH, errno, _wcserror(errno));
      rtnVal = D2V(genError(SA_GENERIC_ERROR_FILE, errno));
      goto exit;
    }
  }

  /* Load the main configuration file */
  SAConfig.path = BUILD_PATH(baseDir, SA_CONFIG_DIR_PATH, SA_CONFIG_FILE_NAME);
  err = refreshMainConfig();
  if (SA_SUCCESS != err) {
    logMsg(LOG_FATAL, "Configuration failure: (%d) %ls", err, getError(err));
    rtnVal = D2V(err);
    goto exit;
  }

  /* Set affinity (Windows-specific; TODO Linux version) */
  if (primaryThreads > 0) {
    HANDLE currThread = GetCurrentThread();
    if (0 == SetThreadAffinityMask(currThread, (DWORD_PTR) primaryAffinityMask)) {
      logMsg(LOG_WARNING, "Failed to set main thread affinity: (%d) %ls", GetLastError(), winError(GetLastError()));
    }
    else {
      if (sizeof(DWORD_PTR) > 4)
        logMsg(LOG_DEBUG, "Set primary affinity mask to 0x%016llX", primaryAffinityMask);
      else
        logMsg(LOG_DEBUG, "Set primary affinity mask to 0x%08X", primaryAffinityMask);
    }
  }

  /* If we're not logging to a file, kill the file we just made */
  if (!logToFile) {
    int logToConsolePrev = logToConsole;
    logToFile = SA_TRUE;
    logToConsole = SA_TRUE;
    logMsg(LOG_INFO, "Disabling log file...");
    err = removeLogFile();
    logToFile = SA_FALSE;
    if (SA_SUCCESS != err)
      logMsg(LOG_WARNING, "Failed to remove log file: (%d) %ls", err, getError(err));
    logToConsole = logToConsolePrev;
  }

  /* If we're not logging to the console, kill the toilet thread we just made */
  if (!logToConsole) {
    logToConsole = SA_TRUE;
    logMsg(LOG_INFO, "Disabling console logger...");
    err = closeToilet();
    logToConsole = SA_FALSE;
    if (SA_SUCCESS != err)
      logMsg(LOG_WARNING, "Failed to remove log thread: (%d) %ls", err, getError(err));
  }

  /* Start the display */
  displayThread = initDisplay();
  if (!displayThread) {
    logMsg(LOG_ERROR, "Display initialization failed!");
    exitMainThread();
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  else {
    logMsg(LOG_INFO, "Display initialized");
  }

  /* Load up the scheduler */
  schedulerThread = initScheduler();
  if (!schedulerThread) {
    logMsg(LOG_ERROR, "Failed to initialize scheduler!");
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  else {
    logMsg(LOG_INFO, "Scheduler initialized");
  }

  currFrame = 0;

  /* Run main loop (and block here for a long time) */
  err = runMainLoop();
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Main loop exited unexpectedly: (%d) %ls", err, getError(err));
    rtnVal = D2V(err);
    goto exit;
  }

#if 1
  /* Wait for display thread to finish before exiting */
  void *threadRtn = D2V(SA_SUCCESS);
  err = pthread_join(*displayThread, &threadRtn);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to join display thread: (%d) %ls", err, _wcserror(err));
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
#endif

  rtnVal = D2V(SA_SUCCESS);

  exit:
  exitMainThread();
  logMsg(LOG_DEBUG, "Main thread exiting");
  return rtnVal;
}

static void exitMainThread(
  void)
{
  logMsg(LOG_INFO, "Starting main thread exit");

  closeScheduler();
  logMsg(LOG_INFO, "Scheduler closed");

  closeDisplay();
  logMsg(LOG_INFO, "Display closed");
}

static int runMainLoop(
  void)
{
  const double catchUpRatio = 0.5;
  const double allowableOversleepRatio = 0.05;

  double currSpinMargin = MSEC_TO_SEC(1.0);
  int err;
  int rtnVal = SA_SUCCESS;

  TIMECAPS caps;

  /* Set timer resolution to something better */
  err = timeGetDevCaps(&caps, sizeof(TIMECAPS));
  if (TIMERR_NOERROR != err) {
    logMsg(LOG_WARNING, "Failed to get timing capabilities: (%d) %ls", err, winError(err));
  }
  else {
    winTimerRes = MIN(MAX(caps.wPeriodMin, winTimerTargetRes), caps.wPeriodMax);
    err = timeBeginPeriod((uint32_t) winTimerRes);
    if (TIMERR_NOERROR != err) {
      logMsg(LOG_WARNING, "Failed to set timing capabilities: (%d) %ls", err, winError(err));
    }
  }

  /* Call the main loop function repeatedly */
  double start;
  double elapsed = frameTarget;
  while (SA_TRUE) {
    start = getTime();

    /* Adjust for over-sleeping or frames that took too long - recovers from small hitches */
    double target = frameTarget - MIN(frameTarget * catchUpRatio, MAX(0.0, elapsed - frameTarget));

    err = mainLoop(MIN(elapsed, maxTimeStep));
    if (SA_SUCCESS != err) {
      if (SA_EXIT == err) {
        /* Unexceptional exit */
        logMsg(LOG_INFO, "Main loop signaled exit");
        goto exit;
      }
      else {
        logMsg(LOG_ERROR, "Main loop failure: (%d) %ls", err, getError(err));
        rtnVal = err;
        goto exit;
      }
    }

    /* If the last frame went over budget, continue to the next frame right away */
    elapsed = getTime() - start;
    if (elapsed >= target) {
      if (logTiming)
        logMsg(LOG_WARNING, "Main loop cycle overrun: took %.3f msec", SEC_TO_MSEC(elapsed));
      continue;
    }

    /* Wait until the next frame is supposed to start */
    while (elapsed < target) {
      /* Sleep if it's safe, to save power/performance; otherwise, spin */
      if (elapsed < (target - currSpinMargin)) {
        Sleep(1);
        elapsed = getTime() - start;
        if (elapsed > target) {
          currSpinMargin += MSEC_TO_SEC(0.1);
          if (logTiming && (((elapsed - target) / target) > allowableOversleepRatio))
            logMsg(LOG_DEBUG, "Overslept by %.3f msec: margin is now %.3f msec",
                SEC_TO_MSEC(elapsed - target), SEC_TO_MSEC(currSpinMargin));
        }
        else if (elapsed > (target - currSpinMargin)) {
          currSpinMargin -= MSEC_TO_SEC(0.001);
        }
      }
      elapsed = getTime() - start;
    }
  }

  exit:
  /* Restore timer resolution */
  if (0 <= winTimerRes) {
    timeEndPeriod((uint32_t) winTimerRes);
    winTimerRes = -1;
  }

  return rtnVal;
}

static int mainLoop(
  double dt)
{
  int err;

  if (!isDisplayInitialized())
    return SA_EXIT;

  /* Wait for display to finish */
  err = waitForDisplay();
  if (SA_SUCCESS != err) {
    if (SA_EXIT == err) {
      /* Unexceptional exit */
      logMsg(LOG_INFO, "Display signaled exit");
      return err;
    }
    else {
      logMsg(LOG_ERROR, "Display wait failure: (%d) %ls", err, getError(err));
      return err;
    }
  }

  /* Officially start the next frame */
  currFrame++;

  /* Step the display */
  err = signalDisplay();
  if (SA_SUCCESS != err) {
    if (SA_EXIT == err) {
      /* Unexceptional exit */
      logMsg(LOG_INFO, "Display signaled exit");
      return err;
    }
    else {
      logMsg(LOG_ERROR, "Display signal failure: (%d) %ls", err, getError(err));
      return err;
    }
  }

  /* Run test suites */
  if (runTests) {
    /* Flush console before the test */
    if (logToConsole) {
      flushToilet();
    }

    if (cfgTestPool) {
      if (isDisplayInitialized())
        testPool();
      else
        logMsg(LOG_INFO, "testPool() cancelled");
      cfgTestPool = SA_FALSE;
    }
    else if (cfgTestScheduler) {
      if (isDisplayInitialized())
        testScheduler();
      else
        logMsg(LOG_INFO, "testScheduler() cancelled");
      cfgTestScheduler = SA_FALSE;
    }
    else if (cfgTestDDS) {
      if (isDisplayInitialized())
        testDDS();
      else
        logMsg(LOG_INFO, "testDDS() cancelled");
      cfgTestDDS = SA_FALSE;
    }
    else if (cfgTestSIMD) {
      if (isDisplayInitialized())
        testSIMD();
      else
        logMsg(LOG_INFO, "testSIMD() cancelled");
      cfgTestSIMD = SA_FALSE;
    }
    else {
      logMsg(LOG_INFO, "Tests completed");
      runTests = SA_FALSE;
    }
  }
  else {
    /* Simulate gameplay work */
#if 1
    Sleep((uint32_t) round(SEC_TO_MSEC(getRandDouble(&random, frameTarget * 0.15, frameTarget * 0.35))));
#endif
  }

  if (logToFile) {
    /* Flush log file every frame, if we're not flushing after every individual print */
    if (!logSafe)
      fflush(logFile);
  }

  if (logToConsole) {
    /* Flush console every frame, so we can print everything safely */
    flushToilet();
  }

  return SA_SUCCESS;
}

static int refreshMainConfig(
  void)
{
#define OPT_CONFIG(optFunc, category, param, var, value)                  \
  ({                                                                      \
    int _err = optFunc(&SAConfig, (category), (param), &(var), (value));  \
    if (SA_SUCCESS != _err) {                                             \
      logMsg(LOG_WARNING, "Failed to parse main configuration: (%d) %ls", \
             _err, getError(_err));                                       \
      rtnVal = _err;                                                      \
      goto exit;                                                          \
    }                                                                     \
  })

#define SET_CONFIG(setFunc, category, param, var, value)                  \
  ({                                                                      \
    __typeof__ (var) *_varPtr = &(var);                                   \
    *_varPtr = (value);                                                   \
    int _err = setFunc(&SAConfig, (category), (param), *_varPtr);         \
    if (SA_SUCCESS != _err) {                                             \
      logMsg(LOG_WARNING, "Failed to parse main configuration: (%d) %ls", \
             _err, getError(_err));                                       \
      rtnVal = _err;                                                      \
      goto exit;                                                          \
    }                                                                     \
  })

#define GET_CONFIG(getFunc, category, param, var)                         \
  ({                                                                      \
    int _err = getFunc(&SAConfig, (category), (param), &(var));           \
    if ((SA_SUCCESS != _err) && (SA_ERROR_NOT_FOUND != _err)) {           \
      logMsg(LOG_WARNING, "Failed to parse main configuration: (%d) %ls", \
             _err, getError(_err));                                       \
      rtnVal = _err;                                                      \
      goto exit;                                                          \
    }                                                                     \
    _err;                                                                 \
  })

#define CLAMP_CONFIG(setFunc, category, param, var, min, max, rangeMacro)       \
  ({                                                                            \
    __typeof__ (min) _minVal  = (min);                                          \
    __typeof__ (max) _maxVal  = (max);                                          \
    __typeof__ (var) *_varPtr = &(var);                                         \
    int _clamped = SA_FALSE;                                                    \
    if (*_varPtr < _minVal) {                                                   \
      *_varPtr = _minVal;                                                       \
      _clamped = SA_TRUE;                                                       \
    }                                                                           \
    else if (*_varPtr > _maxVal) {                                              \
      *_varPtr = _maxVal;                                                       \
      _clamped = SA_TRUE;                                                       \
    }                                                                           \
    if (_clamped) {                                                             \
      wlogMsg(LOG_WARNING, W("Parameter \"%s\" in category \"%s\" clamped to ") \
                           rangeMacro(var),                                     \
             category, param, _minVal, _maxVal);                                \
      int _err = setFunc(&SAConfig, (category), (param), *_varPtr);             \
      if (SA_SUCCESS != _err) {                                                 \
        logMsg(LOG_WARNING, "Failed to parse main configuration: (%d) %ls",     \
               _err, getError(_err));                                           \
        rtnVal = _err;                                                          \
        goto exit;                                                              \
      }                                                                         \
    }                                                                           \
  })

#define PRINT_RANGE_INT(var)   W("[%d, %d]")
#define PRINT_RANGE_FLOAT(var) W("[%.6g, %.6g]")

  int err;
  int rtnVal = SA_SUCCESS;

  err = loadConfig(&SAConfig);
  if (SA_SUCCESS == err) {
    logMsg(LOG_DEBUG, "Main configuration loaded");
  }
  else if (SA_CREATED_CONFIG == err) {
    logMsg(LOG_INFO, "Main configuration not found; refreshing default configuration");
  }
  else if (SA_SUCCESS > err) {
    logMsg(LOG_ERROR, "Failed to load main configuration: (%d) %ls", err, getError(err));
    rtnVal = err;
    goto exit;
  }

  /* Reaching here, the configuration is ready to read/write */
  int overrideCPUConfig = SA_FALSE;
  OPT_CONFIG(configOptBool, "Core", "overrideCPUConfig",
      overrideCPUConfig,
      SA_FALSE);
  int overrideThreadConfig = SA_FALSE;
  OPT_CONFIG(configOptBool, "Core", "overrideThreadConfig",
      overrideThreadConfig,
      SA_FALSE);

  /* Get CPU configuration */
  err = getCPUInfo();
  if (SA_SUCCESS != err) {
    if (cpuCount > 1) {
      err = GET_CONFIG(configGetBool, "Core", "hasHyperThreading",
          useHTMode);
      if (err == SA_ERROR_NOT_FOUND) {
        SET_CONFIG(configSetBool, "Core", "hasHyperThreading",
            useHTMode,
            SA_FALSE);
        configSetComment(&SAConfig, "Core", "hasHyperThreading",
        W("This option appears for you because the program was unable to discover your CPU configuration.  ")
        W("Set this to \"true\" if you have a hyper-threaded CPU; otherwise, set this to \"false\"."));
        logMsg(LOG_WARNING, "CPU configuration unknown! Please check config.xml");
      }

      logicalCPUCount = cpuCount;
      physicalCPUCount = cpuCount / (useHTMode ? 2 : 1);
    }
    else if (cpuCount == 1) {
      useHTMode = SA_FALSE;
      logicalCPUCount = 1;
      physicalCPUCount = 1;
    }
    else {
      int64_t temp = cpuCount;
      err = GET_CONFIG(configGetInt, "Core", "logicalCores",
          temp);
      if (err == SA_ERROR_NOT_FOUND) {
        SET_CONFIG(configSetInt, "Core", "logicalCores",
            temp,
            1);
        configSetComment(&SAConfig, "Core", "logicalCores",
        W("This option appears for you because the program was unable to discover your CPU configuration.  ")
        W("Set this equal to the number of logical cores on your CPU."));
        logMsg(LOG_WARNING, "CPU configuration unknown! Please check config.xml");
      }
      CLAMP_CONFIG(configSetInt, "Core", "logicalCores",
          temp,
          1, INT_MAX,
          PRINT_RANGE_INT);
      cpuCount = temp;

      err = GET_CONFIG(configGetBool, "Core", "hasHyperThreading",
          useHTMode);
      if (err == SA_ERROR_NOT_FOUND) {
        SET_CONFIG(configSetBool, "Core", "hasHyperThreading",
            useHTMode,
            SA_FALSE);
        configSetComment(&SAConfig, "Core", "hasHyperThreading",
        W("This option appears for you because the program was unable to discover your CPU configuration.  ")
        W("Set this to \"true\" if you have a hyper-threaded CPU; otherwise, set this to \"false\"."));
      }

      logicalCPUCount = cpuCount;
      physicalCPUCount = cpuCount / (useHTMode ? 2 : 1);
    }
  }
  else if (!overrideCPUConfig) {
    configRemoveParam(&SAConfig, "Core", "logicalCores");
    configRemoveParam(&SAConfig, "Core", "hasHyperThreading");
    configRemoveParam(&SAConfig, "Core", "skipHyperThreadedCores");
    logMsg(LOG_INFO, "CPU configuration: %d physical, %d logical", physicalCPUCount, logicalCPUCount);
  }

  if (logicalCPUCount > physicalCPUCount)
    useHTMode = SA_TRUE;
  else
    useHTMode = SA_FALSE;
  skipHTCores = SA_FALSE;

  if (overrideCPUConfig) {
    int64_t temp = cpuCount;
    OPT_CONFIG(configOptInt, "Core", "logicalCores",
        temp,
        temp);
    CLAMP_CONFIG(configSetInt, "Core", "logicalCores",
        temp,
        1, INT_MAX,
        PRINT_RANGE_INT);
    cpuCount = temp;

    OPT_CONFIG(configOptBool, "Core", "hasHyperThreading",
        useHTMode,
        useHTMode);

    OPT_CONFIG(configOptBool, "Core", "skipHyperThreadedCores",
        skipHTCores,
        SA_FALSE);

    if (overrideThreadConfig) {
      configSetComment(&SAConfig, "Core", "logicalCores",
      W("Set this equal to the number of logical cores on your CPU.\n\n")
      W("WARNING: This setting has no effect because \"overrideThreadConfig\" is enabled!"));
      configSetComment(&SAConfig, "Core", "hasHyperThreading",
      W("Set this to \"true\" if you have a hyper-threaded CPU; otherwise, set this to \"false\".  ")
      W("In this context, hyper-threading applies to any similar constructs, such as Bulldozer-style CPU ")
      W("modules.\n\n")
      W("WARNING: This setting has no effect because \"overrideThreadConfig\" is enabled!"));
      configSetComment(&SAConfig, "Core", "skipHyperThreadedCores",
      W("Set this to \"true\" to prevent assigning affinities to virtual cores - the game will only use one thread ")
      W("associated with each physical core/module.\n\n")
      W("WARNING: This setting has no effect because \"overrideThreadConfig\" is enabled!"));
    }
    else {
      configSetComment(&SAConfig, "Core", "logicalCores",
          W("Set this equal to the number of logical cores on your CPU."));
      configSetComment(&SAConfig, "Core", "hasHyperThreading",
      W("Set this to \"true\" if you have a hyper-threaded CPU; otherwise, set this to \"false\".  ")
      W("In this context, hyper-threading applies to any similar constructs, such as Bulldozer-style CPU modules."));
      configSetComment(&SAConfig, "Core", "skipHyperThreadedCores",
      W("Set this to \"true\" to prevent assigning affinities to virtual cores - the game will only use one thread ")
      W("associated with each physical core/module."));
    }

    /* Pretend the logical cores don't exist */
    if (skipHTCores && useHTMode) {
      useHTMode = SA_FALSE;
      cpuCount = cpuCount / 2;
    }

    logicalCPUCount = cpuCount;
    if (useHTMode)
      physicalCPUCount = logicalCPUCount / 2;
    else
      physicalCPUCount = logicalCPUCount;
  }

  if (skipHTCores)
    maxCPU = (cpuCount * 2) - 1;
  else
    maxCPU = cpuCount - 1;

  int threadingPower = physicalCPUCount + ((logicalCPUCount - physicalCPUCount) / 2);
  if (threadingPower >= 2)
    primaryThreads = MIN(6, MAX(2, threadingPower / 2));
  else
    primaryThreads = 0;
  reservedThreads = MIN(6, (int64_t ) floor(threadingPower / 3));

  if (overrideThreadConfig) {
    OPT_CONFIG(configOptInt, "Core", "primaryThreads",
        primaryThreads,
        primaryThreads);
    CLAMP_CONFIG(configSetInt, "Core", "primaryThreads",
        primaryThreads,
        0, maxCPU,
        PRINT_RANGE_INT);
  }
  else {
    configRemoveParam(&SAConfig, "Core", "primaryThreads");
  }

  if (primaryThreads > 0) {
    primaryAffinityMask = 0x0000000000000000;
    for (size_t i = 0; i < primaryThreads; i++) {
      if (skipHTCores || useHTMode)
        primaryAffinityMask |= 1ULL << (i * 2);
      else
        primaryAffinityMask |= 1ULL << i;
    }
  }
  else {
    primaryAffinityMask = 0xFFFFFFFFFFFFFFFF;
  }

  /* Set default threading parameters */
  /* 1:     2 workers,  1 critical, 0 reserved, no primary (7 overlap)
   * 1+HT:  2 workers,  1 critical, 0 reserved, no primary (6 overlap)
   * 2:     2 workers,  1 critical, 0 reserved, no primary (6 overlap)
   * 3:     2 workers,  1 critical, 1 reserved, 2 primary  (5 overlap)
   * 2+HT:  3 workers,  1 critical, 1 reserved, 2 primary  (5 overlap)
   * 4:     3 workers,  1 critical, 1 reserved, 2 primary  (5 overlap)
   * 3+HT:  5 workers,  2 critical, 1 reserved, 2 primary  (5 overlap)
   * 6:     4 workers,  2 critical, 2 reserved, 3 primary  (4 overlap)
   * 4+HT:  6 workers,  2 critical, 2 reserved, 3 primary  (4 overlap)
   * 8:     6 workers,  2 critical, 2 reserved, 4 primary  (4 overlap)
   * 10:    7 workers,  3 critical, 3 reserved, 5 primary  (3 overlap)
   * 6+HT:  9 workers,  3 critical, 3 reserved, 4 primary  (3 overlap)
   * 12:    8 workers,  3 critical, 4 reserved, 6 primary  (2 overlap)
   * 8+HT:  12 workers, 4 critical, 4 reserved, 6 primary  (2 overlap)
   * 16:    11 workers, 4 critical, 5 reserved, 6 primary  (1 overlap)
   * 10+HT: 15 workers, 5 critical, 5 reserved, 6 primary  (1 overlap)
   */
  workerCount = MAX(2, cpuCount - reservedThreads);
  criticalCount = MAX(1, MIN(workerCount / 2, (int64_t) round(cpuCount / 4.0)));

  OPT_CONFIG(configOptFloat, "Core", "frameRate",
      frameRate,
      60.0);
  CLAMP_CONFIG(configSetFloat, "Core", "frameRate",
      frameRate,
      FLT_MIN, FLT_MAX,
      PRINT_RANGE_FLOAT);
  frameTarget = 1.0 / frameRate;

  /* TODO: Setting to change primary input type (automatic by default; main purpose is if you have a controller plugged
   * in but want to use K+M anyway).
   */
  if (overrideThreadConfig) {
    OPT_CONFIG(configOptInt, "Display", "threadPriority",
        displayThreadPri,
        sched_get_priority_max(SCHED_OTHER));
    CLAMP_CONFIG(configSetInt, "Display", "threadPriority",
        displayThreadPri,
        sched_get_priority_min(SCHED_OTHER), sched_get_priority_max(SCHED_OTHER),
        PRINT_RANGE_INT);
  }
  else {
    configRemoveParam(&SAConfig, "Display", "threadPriority");
  }

  OPT_CONFIG(configOptFloat, "Display", "inputRate",
      inputFrameRate,
      frameRate * 2.0);
  CLAMP_CONFIG(configSetFloat, "Display", "inputRate",
      inputFrameRate,
      frameRate, FLT_MAX,
      PRINT_RANGE_FLOAT);
  inputFrameTarget = 1.0 / inputFrameRate;

  /* Scheduler settings */
  if (overrideThreadConfig) {
    OPT_CONFIG(configOptInt, "Scheduler", "workerThreads",
        workerCount,
        workerCount);
    CLAMP_CONFIG(configSetInt, "Scheduler", "workerThreads",
        workerCount,
        2, INT_MAX,
        PRINT_RANGE_INT);

    OPT_CONFIG(configOptInt, "Scheduler", "criticalThreads",
        criticalCount,
        criticalCount);
    CLAMP_CONFIG(configSetInt, "Scheduler", "criticalThreads",
        criticalCount,
        1, INT_MAX,
        PRINT_RANGE_INT);

    OPT_CONFIG(configOptInt, "Scheduler", "reservedThreads",
        reservedThreads,
        reservedThreads);
    CLAMP_CONFIG(configSetInt, "Scheduler", "reservedThreads",
        reservedThreads,
        0, MAX(0, cpuCount - 2),
        PRINT_RANGE_INT);

    OPT_CONFIG(configOptInt, "Scheduler", "threadPriority",
        schedulerThreadPri,
        (sched_get_priority_max(SCHED_OTHER) + (sched_get_priority_min(SCHED_OTHER) * 3)) / 4);
    CLAMP_CONFIG(configSetInt, "Scheduler", "threadPriority",
        schedulerThreadPri,
        sched_get_priority_min(SCHED_OTHER), sched_get_priority_max(SCHED_OTHER),
        PRINT_RANGE_INT);

    OPT_CONFIG(configOptInt, "Scheduler", "workerThreadPriority",
        workerThreadPri,
        (sched_get_priority_max(SCHED_OTHER) + sched_get_priority_min(SCHED_OTHER)) / 2);
    CLAMP_CONFIG(configSetInt, "Scheduler", "workerThreadPriority",
        workerThreadPri,
        sched_get_priority_min(SCHED_OTHER), sched_get_priority_max(SCHED_OTHER),
        PRINT_RANGE_INT);

    OPT_CONFIG(configOptInt, "Scheduler", "criticalThreadPriority",
        criticalThreadPri,
        sched_get_priority_max(SCHED_OTHER));
    CLAMP_CONFIG(configSetInt, "Scheduler", "criticalThreadPriority",
        criticalThreadPri,
        sched_get_priority_min(SCHED_OTHER), sched_get_priority_max(SCHED_OTHER),
        PRINT_RANGE_INT);

    OPT_CONFIG(configOptBool, "Scheduler", "useWorkerAffinity",
        useWorkerAffinity,
        SA_TRUE);
  }
  else {
    configRemoveParam(&SAConfig, "Scheduler", "workerThreads");
    configRemoveParam(&SAConfig, "Scheduler", "criticalThreads");
    configRemoveParam(&SAConfig, "Scheduler", "reservedThreads");
    configRemoveParam(&SAConfig, "Scheduler", "threadPriority");
    configRemoveParam(&SAConfig, "Scheduler", "threadAffinity");
    configRemoveParam(&SAConfig, "Scheduler", "workerThreadPriority");
    configRemoveParam(&SAConfig, "Scheduler", "criticalThreadPriority");
    configRemoveParam(&SAConfig, "Scheduler", "useWorkerAffinity");
  }

  OPT_CONFIG(configOptFloat, "Scheduler", "lowPriorityRatio",
      lowPriRatio,
      0.5);
  CLAMP_CONFIG(configSetFloat, "Scheduler", "lowPriorityRatio",
      lowPriRatio,
      (1.0 / workerCount), 1.0,
      PRINT_RANGE_FLOAT);

  OPT_CONFIG(configOptBool, "Logger", "safeLog",
      logSafe,
      DEFAULT_LOG_SAFE);
  OPT_CONFIG(configOptBool, "Logger", "logToConsole",
      logToConsole,
      DEFAULT_LOG_TO_CONSOLE);
  OPT_CONFIG(configOptBool, "Logger", "logToFile",
      logToFile,
      DEFAULT_LOG_TO_FILE);
  OPT_CONFIG(configOptBool, "Logger", "infoPoolResize",
      logPoolResize,
      DEFAULT_LOG_POOL_RESIZE);
  OPT_CONFIG(configOptBool, "Logger", "warnTiming",
      logTiming,
      DEFAULT_LOG_TIMING);
  OPT_CONFIG(configOptBool, "Logger", "suppressDebug",
      logSuppressDebug,
      DEFAULT_LOG_SUPPRESS_DEBUG);
  OPT_CONFIG(configOptBool, "Logger", "suppressInfo",
      logSuppressInfo,
      DEFAULT_LOG_SUPPRESS_INFO);
  OPT_CONFIG(configOptBool, "Logger", "suppressWarning",
      logSuppressWarning,
      DEFAULT_LOG_SUPPRESS_WARNING);
  OPT_CONFIG(configOptBool, "Logger", "suppressError",
      logSuppressError,
      DEFAULT_LOG_SUPPRESS_ERROR);
  OPT_CONFIG(configOptBool, "Logger", "suppressFatal",
      logSuppressFatal,
      DEFAULT_LOG_SUPPRESS_FATAL);

  OPT_CONFIG(configOptBool, "Test", "poolTest",
      cfgTestPool,
      DEFAULT_RUN_TESTS);
  OPT_CONFIG(configOptBool, "Test", "schedulerTest",
      cfgTestScheduler,
      DEFAULT_RUN_TESTS);
  OPT_CONFIG(configOptBool, "Test", "ddsTest",
      cfgTestDDS,
      DEFAULT_RUN_TESTS);
  OPT_CONFIG(configOptBool, "Test", "simdTest",
      cfgTestSIMD,
      DEFAULT_RUN_TESTS);

  /* Add comments for complicated settings */
  configSetHeader(&SAConfig, "Core",
      W("To reset a parameter, simply delete the XML entry and the comment above it (if any)."));
  configSetComment(&SAConfig, "Core", "overrideCPUConfig",
  W("Set this to \"true\" if you want to change the number of cores or the hyper-threading mode that the game ")
  W("assumes your CPU has.  ")
  W("For instance, some users might find better performance with hyper-threading mode enabled on Bulldozer-family ")
  W("AMD processors.\n\n")
  W("Run the game once after setting this setting to \"true\" to generate the extra configuration settings."));
  configSetComment(&SAConfig, "Core", "overrideThreadConfig",
  W("Set this to \"true\" if you want to change the thread affinities, game loop settings, and other settings ")
  W("sensitive to your particular CPU configuration.\n\n")
  W("Run the game once after setting this setting to \"true\" to generate the extra configuration settings."));
  configSetComment(&SAConfig, "Core", "frameRate",
  W("This is the operating frequency that Stellar Ascendant will attempt to match.\n\n")
  W("External factors, like vsync, resource limitations, and random stalls may reduce the frame rate.\n\n")
  W("This setting only guarantees that the overall frame rate will not exceed the given value.  ")
  W("Other aspects of timing, such as phase and clock drift, are not guaranteed.\n\n")
  W("Stellar Ascendant is non-deterministic and prioritizes smoothness and frame consistency over correctness.  ")
  W("As such, Stellar Ascendant uses a variable time step; gameplay may be affected by frame rate.  ")
  W("For this reason, Stellar Ascendant enforces a maximum time step of 50 milliseconds (20 Hz).  ")
  W("In other words, any frame rate below 20 Hz will cause the game to operate in slow-motion.\n\n")
  W("Note that frame timing is based on the GLFW high-precision performance timer."));
  configSetComment(&SAConfig, "Core", "primaryThreads",
  W("Number of processors to allow for primary tasks.  Primary tasks will only execute on these processing cores.  ")
  W("Set to 0 to allow primary threads to run on any processing core.\n\n")
  W("The primary tasks include:\n")
  W("  1. Main loop\n")
  W("  2. GLFW (display + input)\n")
  W("  3. Scheduler\n")
  W("  4. OpenGL (graphics)\n")
  W("  5. OpenAL (audio)\n")
  W("  6. Controller"));

  configSetComment(&SAConfig, "Display", "threadPriority",
  W("This is the priority for the window and input handling thread.\n\n")
  W("The graphics thread is separate from the display thread, so this priority level largely affects input ")
  W("response, not rendering performance.\n\n")
  W("Note that the program's main thread always has the maximum priority allowed by the system."));
  configSetComment(&SAConfig, "Display", "inputRate",
  W("This is the frequency at which Stellar Ascendant will poll input events and controller state.\n\n")
  W("Stellar Ascendant supports sub-frame input timing, so generally speaking a higher input rate makes the game ")
  W("a bit more responsive.  ")
  W("Don't go overboard; past a certain point, this setting offers virtually no benefit and just eats up the CPU.  ")
  W("By default, this value is double the target frame rate.\n\n")
  W("Note that input is always polled right before a frame is calculated, regardless of the commanded input ")
  W("rate.\n\n")
  W("Furthermore, the input clock is reset every frame, so that it doesn't drift out of phase with gameplay.  ")
  W("Therefore, this setting should be a multiple of the target frame rate.\n\n")
  W("TAS users should set this value equal to the frame rate, in order to achieve deterministic behavior."));

  configSetComment(&SAConfig, "Scheduler", "workerThreads",
  W("Number of general-purpose threads for the scheduler to work with.\n\n")
  W("By default, this value is equal to three-fourths of the number of processors available to the system.  ")
  W("There is generally no benefit to increasing this value past the total processor count."));
  configSetComment(&SAConfig, "Scheduler", "reservedThreads",
      W("Number of threads that the scheduler should not set affinities for."));
  configSetComment(&SAConfig, "Scheduler", "criticalThreads",
  W("Number of special-purpose threads for the scheduler to use for time-critical tasks.\n\n")
  W("By default, this value is equal to one-fourth of the number of processors available to the system.  ")
  W("Increasing this value too much can actually degrade performance."));
  configSetComment(&SAConfig, "Scheduler", "lowPriorityRatio",
  W("Ratio of worker threads available for performing low-priority tasks.\n\n")
  W("By default, this value is 0.5 (50%).\n\n")
  W("Note that this value is always treated as 1.0 (100%) in menus and loading screens."));
  configSetComment(&SAConfig, "Scheduler", "threadPriority",
  W("Thread priority level for the scheduling module itself.\n\n")
  W("This should generally be low, as the actual work should have precedence over their scheduler."));
  configSetComment(&SAConfig, "Scheduler", "useWorkerAffinity",
  W("Whether worker threads should have affinities (automatically) set to specific processors.\n\n")
  W("Using affinities usually reduces peak performance but improve worst-case performance and make the game ")
  W("smoother."));

  configSetComment(&SAConfig, "Logger", "safeLog",
  W("Makes logging slower, but prevents several problems (such as mangling and lost data).\n\n")
  W("This is good to use while debugging."));
  configSetComment(&SAConfig, "Logger", "logToConsole",
  W("Logging to the console is useful for debugging and active development, but not much else.\n\n")
  W("Warning: Logging to cmd.exe (the Windows pre-10 default) is EXTREMELY SLOW."));
  configSetComment(&SAConfig, "Logger", "logToFile",
      W("Logging to a file is relatively cheap, and useful for reporting bugs."));
  configSetComment(&SAConfig, "Logger", "infoPoolResize",
      W("Produce a message in the log any time an object pool is dynamically resized because it ran out of space."));
  configSetComment(&SAConfig, "Logger", "warnTiming",
      W("Produce warnings on cycle overruns and other anomalies that indicate a lapse in performance."));

  /* Save what we've got */
  err = saveConfig(&SAConfig);
  if (SA_SUCCESS == err) {
    logMsg(LOG_DEBUG, "Main configuration saved");
  }
  else if (SA_SUCCESS > err) {
    logMsg(LOG_ERROR, "Failed to save main configuration: (%d) %ls", err, getError(err));
    rtnVal = err;
    goto exit;
  }

  exit:
  /* Close out the configuration file once we're done with it */
  closeConfig(&SAConfig);
  return rtnVal;

#undef OPT_CONFIG
#undef SET_CONFIG
#undef GET_CONFIG
#undef CLAMP_CONFIG
#undef PRINT_RANGE_INT
#undef PRINT_RANGE_FLOAT
}

/* Helper function to count set bits in the processor mask. */
static int countSetBits(
  ULONG_PTR bitMask)
{
  const int shift = sizeof(ULONG_PTR) * 8 - 1;

  int bitSetCount = 0;
  ULONG_PTR bitTest = (ULONG_PTR) 1 << shift;

  for (int i = 0; i <= shift; i++) {
    bitSetCount += ((bitMask & bitTest) ? 1 : 0);
    bitTest >>= 1;
  }

  return bitSetCount;
}

/* Stolen from Microsoft */
static int getCPUInfo(
  void)
{
  int done = SA_FALSE;
  PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
  PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
  DWORD returnLength = 0;
  int byteOffset = 0;
  PCACHE_DESCRIPTOR cache;

  /* Clear static variables */
  physicalCPUCount = 0;
  logicalCPUCount = 0;
  numaNodeCount = 0;
  processorPackageCount = 0;
  processorL1CacheCount = 0;
  processorL2CacheCount = 0;
  processorL3CacheCount = 0;

  while (!done) {
    int rtnVal = GetLogicalProcessorInformation(buffer, &returnLength);

    if (SA_FALSE == rtnVal) {
      uint32_t err = GetLastError();
      if (err == ERROR_INSUFFICIENT_BUFFER) {
        if (buffer)
          free(buffer);

        buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION) malloc(returnLength);

        if (NULL == buffer) {
          logMsg(LOG_ERROR, "Failed to allocate %'d bytes", returnLength);
          return SA_ERROR_MEMORY;
        }
      }
      else {
        logMsg(LOG_ERROR, "Error getting CPU info: (%d) %ls", err, winError(err));
        if (buffer)
          free(buffer);
        return genError(SA_GENERIC_ERROR_MS_SYSTEM, err);
      }
    }
    else {
      done = SA_TRUE;
    }
  }

  ptr = buffer;

  while ((byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION)) <= returnLength) {
    switch (ptr->Relationship) {
      case RelationNumaNode: {
        /* Non-NUMA systems report a single record of this type. */
        numaNodeCount++;
        break;
      }

      case RelationProcessorCore: {
        physicalCPUCount++;

        /* A hyper-threaded core supplies more than one logical processor. */
        logicalCPUCount += countSetBits(ptr->ProcessorMask);
        break;
      }

      case RelationCache: {
        /* Cache data is in ptr->Cache, one CACHE_DESCRIPTOR structure for each cache. */
        cache = &ptr->Cache;
        if (cache->Level == 1)
          processorL1CacheCount++;
        else if (cache->Level == 2)
          processorL2CacheCount++;
        else if (cache->Level == 3)
          processorL3CacheCount++;
        break;
      }

      case RelationProcessorPackage: {
        /* Logical processors share a physical package. */
        processorPackageCount++;
        break;
      }

      default:
      break;
    }

    byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
    ptr++;
  }

  free(buffer);

  return SA_SUCCESS;
}
