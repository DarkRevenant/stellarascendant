/**********************************************************************************************************************
 * main.h
 * Copyright (C) 2016 Magehand LLC
 *
 * NOTE: Never use variadic arguments for functions.  It is totally broken with LTO.  They kind of suck anyway.
 * NOTE: LTO does not work at all with debug builds.
 * NOTE: Don't use hyper-threaded workers for pure math.  It does NOT scale.
 * NOTE: Avoid using more workers than you have dedicated threads for, when performing the same kind of task.
 *********************************************************************************************************************/

#ifndef ___MAIN_H___
#define ___MAIN_H___

/**
 ** Global Variables
 **/

extern int cpuCount;
extern int maxCPU;
extern int useHTMode;
extern int skipHTCores;
extern int64_t primaryThreads;
extern uint64_t primaryAffinityMask;

extern int logSafe;
extern int logToConsole;
extern int logToFile;
extern int logPoolResize;
extern int logTiming;
extern int logSuppressDebug;
extern int logSuppressInfo;
extern int logSuppressWarning;
extern int logSuppressError;
extern int logSuppressFatal;

extern double frameRate;
extern double frameTarget;

extern uint64_t currFrame;

#endif /* ___MAIN_H___ */
