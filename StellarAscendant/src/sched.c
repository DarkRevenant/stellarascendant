/**********************************************************************************************************************
 * sched.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   initScheduler           Initializes the SA thread scheduler
 *   closeScheduler          Terminates the SA thread scheduler
 *   setSchedulerMode        Changes the scheduler mode
 *   queueTask               Schedules and ultimately runs the given task
 *   flushQueue              Sends queued tasks to run; necessary after calls to queueTask
 *   bindFuture              Blocks until the future's task is finished, and retrieves the result
 *   tryBindFuture           Retrieves the future's result, if available
 *
 * Local Functions:
 *   createSchedulerThread   Create the scheduler thread
 *   runScheduler            Entry point to run the scheduler thread
 *   freeScheduler           Free the memory allocated to the scheduler
 *   runWorker               Entry point to run a worker thread
 *   createWorker            Create a worker thread
 *   freeWorker              Free the memory allocated to a worker thread
 *   assignTask              Assign a scheduled task to a worker thread
 *
 * Utility Functions:
 *   createFuture            Creates a future object for later use
 *   destroyFuture           Destroys a future object
 *   initFuturePool          Initializes a future pool
 *   closeFuturePool         Closes a future pool
 *   popFuture               Retrieves an unused future from a future pool
 *   pushFuture              Returns a future to a future pool
 *   initTaskPool            Initializes a task pool
 *   closeTaskPool           Closes a task pool
 *   popTask                 Retrieves an unused task from a task pool
 *   pushTask                Returns a task to a task pool
 *********************************************************************************************************************/

#include <limits.h>
#include <math.h>

/** SA Headers **/
#include "sched.h"
#include "common.h"
#include "main.h"
#include "display/display.h"
#include "utils/object.h"

/**
 ** Typedefs and Definitions
 **/

typedef struct {
  void *result;
  int done;
  pthread_mutex_t lock;
  pthread_cond_t access;
} Future_Impl_t;

typedef struct _ScheduledTask {
  TaskFunc_t task;
  void *arg;
  Future_Impl_t *future;
  TaskPriority_t priority;

  struct _ScheduledTask *next;
} ScheduledTask_t;

typedef struct {
  ScheduledTask_t *first;
  ScheduledTask_t *last;

  size_t timesPassed; /* Number of times this priority level has been passed up in favor of a higher one */
} TaskQueue_t;

typedef struct {
  pthread_t thread;
  pthread_mutex_t lock;
  pthread_cond_t access;

  int workerID;
  int cmdClose;

  int affinity;
  int overlapAffinity;

  ScheduledTask_t *currentTask;
} WorkerThread_t;

typedef OBJECT_POOL(Future_Impl_t)
FuturePool_t;

typedef OBJECT_POOL(ScheduledTask_t)
TaskPool_t;

/**
 ** Global Variables
 **/

__thread int workerID = -1;

int64_t workerCount = 0;
int64_t criticalCount = 0;
int64_t reservedThreads = 0;
double lowPriRatio = 0.5;

int64_t schedulerThreadPri;
int64_t workerThreadPri;
int64_t criticalThreadPri;
int useWorkerAffinity = SA_TRUE;

/**
 ** Constants
 **/

static const size_t schedulerThreadStackSize = KBYTES_TO_BYTES(128);
static const size_t workerThreadStackSize = MBYTES_TO_BYTES(1);

/**
 ** Prototypes #1
 **/

static int createFuture(
  Future_Impl_t *future);

static int destroyFuture(
  Future_Impl_t *future);

/**
 ** Local Variables
 **/

static pthread_t schedulerThread = (pthread_t) 0;
static pthread_mutex_t schedulerLock = (pthread_mutex_t) 0;
static pthread_cond_t schedulerAccess = (pthread_cond_t) 0;

static WorkerThread_t **workerThreads = NULL;
static WorkerThread_t **criticalThreads = NULL;

/* FIFO queue */
static TaskQueue_t taskQueue[NUM_TASK_PRIORITY_LEVELS];
static int numCurrentTasks[NUM_TASK_PRIORITY_LEVELS];

/* Object pools */
FuturePool_t futurePool = { createFuture, destroyFuture, 1000, 0, SA_TRUE, "Futures" };
TaskPool_t taskPool = { NULL, NULL, 1000, 0, SA_TRUE, "Tasks" };

static SchedulerMode_t schedulerMode = MODE_OFFLINE;
static int loaded = SA_FALSE;
static int cmdClose = SA_FALSE;

/**
 ** Prototypes #2
 **/

static pthread_t* createSchedulerThread(
  void);

static void* runScheduler(
  void *arg);

static void freeScheduler(
  void);

static WorkerThread_t* createWorker(
  int workerID,
  int priority,
  int affinity,
  int overlapAffinity);

/* Expects WorkerThread_t argument */
static void* runWorker(
  void *arg);

static void freeWorker(
  WorkerThread_t *worker);

static void assignTask(
  WorkerThread_t *worker,
  ScheduledTask_t *task);

DECLARE_POOL_FUNCS(Future_Impl_t, FuturePool_t, static, initFuturePool, closeFuturePool, popFuture, pushFuture);

DECLARE_POOL_FUNCS(ScheduledTask_t, TaskPool_t, static, initTaskPool, closeTaskPool, popTask, pushTask);

/**
 ** Global Functions
 **/

pthread_t* initScheduler(
  void)
{
  pthread_t *thread = NULL;
  int err;

  cmdClose = SA_FALSE;

  if (workerCount <= 1)
    workerCount = MAX(2, cpuCount);
  if (criticalCount <= 0)
    criticalCount = MAX(1, (int64_t ) round(cpuCount / 4.0));

  workerThreads = a_malloc(workerCount * sizeof(void*));
  criticalThreads = a_malloc(criticalCount * sizeof(void*));

  /* Initialize object pools */
  err = initFuturePool(&futurePool, 10);
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to create future pool: (%d) %ls", err, getError(err));
    return NULL;
  }
  err = initTaskPool(&taskPool, 10);
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Failed to create task pool: (%d) %ls", err, getError(err));
    return NULL;
  }

  int *reservedCPUs = alloca((maxCPU + 1) * sizeof(int));
  for (size_t i = 0; i <= maxCPU; i++)
    reservedCPUs[i] = SA_FALSE;
  for (size_t i = 0; i < reservedThreads; i++) {
    if (useHTMode || skipHTCores)
      reservedCPUs[i * 2] = SA_TRUE;
    else
      reservedCPUs[i] = SA_TRUE;
  }

  /* Choose affinity for first worker */
  int affinity = maxCPU;
  if (1 == (affinity % 2)) {
    /* We force the affinity to land on an even-numbered processor (counting from 0).  Why?  All of the reserved
     * threads are on even-numbered processors as well.  This means that the first few worker threads (the ones most
     * likely to be doing work) will avoid conflicting with the reserved threads' cores, ensuring that those cores are
     * as free as possible!  The remaining worker threads fill in after that, and will therefore be slower than the
     * first few worker threads since, in theory, every physical core has been used up by then.
     *
     * To summarize, this lets the "bonus" threads from hyper-threading act as extra worker threads.  They'll be slower
     * than typical threads, but this lets us squeeze extra power out of the CPU.
     */
    affinity--;
  }
  while (reservedCPUs[affinity]) {
    affinity -= (useHTMode || skipHTCores) ? 2 : 1;
    if (affinity < 0) {
      if (useHTMode) {
        if (affinity == -1)
          affinity += maxCPU;
        else
          affinity += maxCPU + 2;
      }
      else {
        affinity += maxCPU + 1;
      }
    }
  }

  /* Create standard workers */
  int *workerAffinities = alloca(workerCount * sizeof(int));
  for (int i = 0; i < workerCount; i++) {
    WorkerThread_t *worker = createWorker(i, workerThreadPri, affinity, -1);
    if (!worker) {
      logMsg(LOG_ERROR, "Failed to create worker %d", i);
      goto error;
    }

    workerThreads[i] = worker;
    workerAffinities[i] = affinity;

    /* Select next affinity */
    do {
      affinity -= (useHTMode || skipHTCores) ? 2 : 1;
      if (affinity < 0) {
        if (useHTMode) {
          if (affinity == -1)
            affinity += maxCPU;
          else
            affinity += maxCPU + 2;
        }
        else {
          affinity += maxCPU + 1;
        }
      }
    }
    while (reservedCPUs[affinity]);
  }

  /* Create time-critical workers */
  for (int i = 0; i < criticalCount; i++) {
    /* Time-critical workers get an affinity set involving two threads:
     *
     * First, they get the threads they're normally supposed to get, according to the scheme used for the other worker
     * threads.
     *
     * Second, they get the threads that would otherwise belong to the last few worker threads.  The reason for this is
     * that those last few worker threads will usually be unused when a bunch of time-critical jobs have been queued;
     * the scheduler will try not to let more jobs be processing at the same time than the normal worker threads can
     * handle.  The only time that *every* worker is active simultaneously is when every standard worker thread is busy
     * at the moment that bunch of time-critical tasks are ordered.  In such a situation, the time-critical workers
     * kick in, finishing their jobs as soon as possible at the cost of slowing down the rest of the program.
     */
    int overlapAffinity = -1;
    if (i < workerCount) {
      int workerIdx = i + MAX(0, workerCount - criticalCount);
      overlapAffinity = workerAffinities[workerIdx];
    }

    WorkerThread_t *worker = createWorker(i + workerCount, criticalThreadPri, affinity, overlapAffinity);
    if (!worker) {
      logMsg(LOG_ERROR, "Failed to create time-critical worker %d", i + workerCount);
      goto error;
    }

    criticalThreads[i] = worker;

    /* Select next affinity */
    do {
      affinity -= (useHTMode || skipHTCores) ? 2 : 1;
      if (affinity < 0) {
        if (useHTMode) {
          if (affinity == -1)
            affinity += maxCPU;
          else
            affinity += maxCPU + 2;
        }
        else {
          affinity += maxCPU + 1;
        }
      }
    }
    while (reservedCPUs[affinity]);
  }

  /* Create scheduler thread */
  thread = createSchedulerThread();
  if (!thread) {
    logMsg(LOG_ERROR, "Failed to create scheduler thread");
    goto error;
  }

  schedulerMode = MODE_LOADING;
  goto exit;

  error:
  closeScheduler();
  return NULL;

  exit:
  return thread;
}

int closeScheduler(
  void)
{
  schedulerMode = MODE_OFFLINE;

  if (workerThreads) {
    /* Send close commands */
    for (int i = 0; i < workerCount; i++) {
      WorkerThread_t *worker = workerThreads[i];

      pthread_mutex_lock(&(worker->lock));
      worker->cmdClose = SA_TRUE;
      pthread_cond_signal(&(worker->access));
      pthread_mutex_unlock(&(worker->lock));
    }

    /* Wait for the worker threads to close, and free their resources */
    for (int i = 0; i < workerCount; i++) {
      WorkerThread_t *worker = workerThreads[i];

      void *threadRtn;
      pthread_join(worker->thread, &threadRtn);

      freeWorker(worker);
    }

    free(workerThreads);
  }

  if (criticalThreads) {
    /* Send close commands */
    for (int i = 0; i < criticalCount; i++) {
      WorkerThread_t *worker = criticalThreads[i];

      pthread_mutex_lock(&(worker->lock));
      worker->cmdClose = SA_TRUE;
      pthread_cond_signal(&(worker->access));
      pthread_mutex_unlock(&(worker->lock));
    }

    /* Wait for the worker threads to close, and free their resources */
    for (int i = 0; i < criticalCount; i++) {
      WorkerThread_t *worker = criticalThreads[i];

      void *threadRtn;
      pthread_join(worker->thread, &threadRtn);

      freeWorker(worker);
    }

    free(criticalThreads);
  }

  /* Early exit if there's nothing left to close */
  if (!loaded)
    goto exit;

  /* Once we get a lock, signal to close and wait until it has actually closed */
  pthread_mutex_lock(&schedulerLock);
  cmdClose = SA_TRUE;
  pthread_cond_signal(&schedulerAccess);
  pthread_mutex_unlock(&schedulerLock);

  /* Wait for main thread to finish before exiting */
  void *threadRtn;
  pthread_join(schedulerThread, &threadRtn);

  /* Clean up data */
  exit:
  freeScheduler();

  return SA_SUCCESS;
}

SchedulerMode_t setSchedulerMode(
  SchedulerMode_t mode)
{
  if ((schedulerMode > MODE_OFFLINE) &&
      (mode > MODE_OFFLINE) && (mode < NUM_SCHEDULER_MODES))
  {
    /* Obtain a lock so that the scheduler mode can be safely changed */
    pthread_mutex_lock(&schedulerLock);

    schedulerMode = mode;

    /* Wake up the scheduler so that the change may take effect, if possible */
    pthread_cond_signal(&schedulerAccess);
    pthread_mutex_unlock(&schedulerLock);
  }

  return schedulerMode;
}

/* Recursive tasks are possible but not recommended, as this can cause an total lock-up if we try blocking for task
 * completion in a worker thread.
 * Remember that *arg shall not be freed by the caller before the task is completed.
 * May return NULL if the operation failed, which is generally the case if the scheduler is offline.
 */
Future_t* queueTask(
  TaskFunc_t task,
  void *arg,
  TaskPriority_t priority)
{
  if (schedulerMode == MODE_OFFLINE)
    return NULL;

  ScheduledTask_t *schedTask;

  /* Initialize scheduled task structure */
  schedTask = popTask(&taskPool);
  if (!schedTask) {
    logMsg(LOG_ERROR, "Failed to allocate scheduled task");
    goto error;
  }
  schedTask->task = task;
  schedTask->arg = arg;
  schedTask->priority = priority;
  schedTask->next = NULL;

  /* Initialize future object */
  schedTask->future = popFuture(&futurePool);
  if (!(schedTask->future)) {
    logMsg(LOG_ERROR, "Failed to allocate future");
    goto error;
  }
  schedTask->future->done = SA_FALSE;
  schedTask->future->result = NULL;

  /* Lock the scheduler to avoid race conditions, but don't wake it up */
  pthread_mutex_lock(&schedulerLock);

  /* Add scheduled task to the queue */
  if (!(taskQueue[priority].first))
    taskQueue[priority].first = schedTask;
  if (taskQueue[priority].last)
    taskQueue[priority].last->next = schedTask;
  taskQueue[priority].last = schedTask;

  pthread_mutex_unlock(&schedulerLock);

  /* Export the future as public type */
  return (Future_t*) (schedTask->future);

  error:
  if (schedTask) {
    if (schedTask->future)
      pushFuture(&futurePool, schedTask->future);
    pushTask(&taskPool, schedTask);
  }
  return NULL;
}

/* The queue typically doesn't automatically flush - call this when you want them to run! */
void flushQueue(
  void)
{
  if (schedulerMode == MODE_OFFLINE)
    return;

  /* Wake up the scheduler */
  pthread_mutex_lock(&schedulerLock);
  pthread_cond_signal(&schedulerAccess);
  pthread_mutex_unlock(&schedulerLock);
}

/* Frees the future object if SA_SUCCESS is returned */
int bindFuture(
  Future_t *future,
  void* *result)
{
  if (schedulerMode == MODE_OFFLINE)
    return SA_ERROR_OFFLINE;
  if (future == NULL) {
    logMsg(LOG_ERROR, "Future is NULL");
    return SA_ERROR_ARGS;
  }
  if (result == NULL) {
    logMsg(LOG_ERROR, "Result pointer is NULL");
    return SA_ERROR_ARGS;
  }

  Future_Impl_t *f = (Future_Impl_t*) future;

  pthread_mutex_lock(&(f->lock));
  while (!(f->done))
    pthread_cond_wait(&(f->access), &(f->lock));
  pthread_mutex_unlock(&(f->lock));

  *result = f->result;

  /* Clean up the future */
  pushFuture(&futurePool, f);
  return SA_SUCCESS;
}

/* Frees the future object if SA_SUCCESS is returned */
int tryBindFuture(
  Future_t *future,
  void* *result)
{
  if (schedulerMode == MODE_OFFLINE)
    return SA_ERROR_OFFLINE;
  if (future == NULL) {
    logMsg(LOG_ERROR, "Future is NULL");
    return SA_ERROR_ARGS;
  }
  if (result == NULL) {
    logMsg(LOG_ERROR, "Result pointer is NULL");
    return SA_ERROR_ARGS;
  }

  Future_Impl_t *f = (Future_Impl_t*) future;

  if (!(f->done))
    return SA_FAILURE;

  *result = f->result;

  /* Clean up the future */
  pushFuture(&futurePool, f);
  return SA_SUCCESS;
}

/**
 ** Local Functions
 **/

static pthread_t* createSchedulerThread(
  void)
{
  pthread_attr_t *schedulerThreadAttr = alloca(sizeof(pthread_attr_t));

  int err;

  /* Somewhere between standard and critical worker threads */
  struct sched_param schedParam;
  schedParam.sched_priority = schedulerThreadPri;

  /* Set scheduler thread attributes */
  err = pthread_attr_init(schedulerThreadAttr);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to initialize scheduler thread attribute: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setdetachstate(schedulerThreadAttr, PTHREAD_CREATE_JOINABLE);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set scheduler thread attribute joinable: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setstacksize(schedulerThreadAttr, schedulerThreadStackSize);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set scheduler thread stack size to %'d bytes: (%d) %ls",
        schedulerThreadStackSize, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set scheduler thread stack size to %'d bytes", schedulerThreadStackSize);
  }
  err = pthread_attr_setinheritsched(schedulerThreadAttr, PTHREAD_EXPLICIT_SCHED);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set scheduler thread scheduling explicit: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setscope(schedulerThreadAttr, PTHREAD_SCOPE_PROCESS);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set scheduler thread to process scope: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  /* Only SCHED_OTHER is supported on Windows */
  err = pthread_attr_setschedpolicy(schedulerThreadAttr, SCHED_OTHER);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set scheduler thread scheduling policy to standard: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setschedparam(schedulerThreadAttr, &schedParam);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set scheduler thread scheduling priority to %d: (%d) %ls",
        schedParam.sched_priority, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set scheduler thread scheduling priority to %d", schedParam.sched_priority);
  }

  /* Create mutex object to synchronize scheduler to other threads */
  err = pthread_mutex_init(&schedulerLock, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create mutex: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Create condition object to synchronize scheduler to other threads */
  err = pthread_cond_init(&schedulerAccess, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create condition: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Create scheduler thread */
  err = pthread_create(&schedulerThread, schedulerThreadAttr, runScheduler, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create scheduler thread: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Wait for the scheduler to start up */
  pthread_mutex_lock(&schedulerLock);
  while (!loaded) {
    pthread_cond_wait(&schedulerAccess, &schedulerLock);
  }
  pthread_mutex_unlock(&schedulerLock);

  goto exit;

  error:
  freeScheduler();

  exit:
  if (schedulerThreadAttr)
    pthread_attr_destroy(schedulerThreadAttr);
  return &schedulerThread;
}

static void* runScheduler(
  void *arg)
{
  void *rtnVal = D2V(SA_SUCCESS);

  /* Set affinity (Windows-specific; TODO Linux version) */
  if (primaryThreads >= 0) {
    HANDLE currThread = GetCurrentThread();
    if (0 == SetThreadAffinityMask(currThread, (DWORD_PTR) primaryAffinityMask))
      logMsg(LOG_WARNING, "Failed to set scheduler thread affinity: (%d) %ls",
          GetLastError(), winError(GetLastError()));
  }

  /* Set up the task queue */
  for (TaskPriority_t p = 0; p < NUM_TASK_PRIORITY_LEVELS; p++) {
    taskQueue[p].first = NULL;
    taskQueue[p].last = NULL;
    taskQueue[p].timesPassed = 0;
  }

  /* Signal to the world that the scheduler is ready */
  pthread_mutex_lock(&schedulerLock);
  loaded = SA_TRUE;
  pthread_cond_broadcast(&schedulerAccess);
  pthread_mutex_unlock(&schedulerLock);

  pthread_mutex_lock(&schedulerLock);

  while (SA_TRUE) {
    /* Wait until something interesting happens. */
    if (!cmdClose)
      pthread_cond_wait(&schedulerAccess, &schedulerLock);

    /* See if we were asked to close */
    if (cmdClose) {
      logMsg(LOG_DEBUG, "Close command received");
      break;
    }

    /* Determine the number of worker threads we can use */
    int permittedWorkers;
    int permittedWorkersLowPri;
    switch (schedulerMode) {
      case MODE_LOADING: {
        permittedWorkers = MAX(2, workerCount);
        permittedWorkersLowPri = permittedWorkers;
        break;
      }
      case MODE_PLAYING_ADVANCE: {
        permittedWorkers = MAX(2, workerCount - 1);
        permittedWorkersLowPri = MAX(1, permittedWorkers * lowPriRatio);
        break;
      }
      case MODE_PLAYING_DRAW: {
        permittedWorkers = MAX(2, workerCount - 1);
        permittedWorkersLowPri = MAX(1, permittedWorkers * lowPriRatio);
        break;
      }
      case MODE_PLAYING_ADVANCE_DRAW: {
        permittedWorkers = MAX(2, workerCount - 2);
        permittedWorkersLowPri = MAX(1, permittedWorkers * lowPriRatio);
        break;
      }
      case MODE_PLAYING_SYNC: {
        permittedWorkers = MAX(2, workerCount);
        permittedWorkersLowPri = MAX(1, permittedWorkers * lowPriRatio);
        break;
      }

      default: {
        logMsg(LOG_ERROR, "Invalid scheduling mode: %d", schedulerMode);
        continue;
      }
    }

    /* Time-critical tasks can spill over into standard worker threads */
    int numStandardTasks = numCurrentTasks[TASK_TIME_CRITICAL] +
        numCurrentTasks[TASK_HIGH_PRIORITY] +
        numCurrentTasks[TASK_MEDIUM_PRIORITY] +
        numCurrentTasks[TASK_LOW_PRIORITY];
    int numCriticalTasks = MIN(criticalCount, numCurrentTasks[TASK_TIME_CRITICAL]);

    /* Check the queue */
    for (int p = TASK_HIGHEST_PRIORITY; p >= TASK_LOWEST_PRIORITY; p--) {
      TaskQueue_t *queue = &(taskQueue[p]);

      /* Exhaust the entire queue before moving on */
      while (NULL != queue->first) {
        ScheduledTask_t *nextTask = queue->first;

        switch (p) {
          case TASK_LOW_PRIORITY: {
            /* Stop searching if no workers are free */
            if (numStandardTasks >= permittedWorkersLowPri)
              goto exitLoop;

            /* Find a free worker thread */
            for (int i = 0; i < workerCount; i++) {
              WorkerThread_t *worker = workerThreads[i];
              if (!(worker->currentTask)) {
                assignTask(worker, nextTask);
                numCurrentTasks[p]++;
                numStandardTasks++;
                queue->timesPassed = 0;
                goto next;
              }
            }

            /* Control should never reach here */
            logMsg(LOG_FATAL, "Task count mismatch for priority %d!", p);
            goto fatal;
          }

          case TASK_MEDIUM_PRIORITY:
          case TASK_HIGH_PRIORITY: {
            /* Stop searching if no workers are free */
            if (numStandardTasks >= permittedWorkers) {
              /* Mark the lower priority queues as being passed up in favor of this one */
              for (int lowerPri = p - 1; lowerPri >= TASK_LOWEST_PRIORITY; lowerPri--) {
                if (taskQueue[lowerPri].first)
                  taskQueue[lowerPri].timesPassed++;
              }

              goto exitLoop;
            }

            /* Find a free worker thread */
            for (int i = 0; i < workerCount; i++) {
              WorkerThread_t *worker = workerThreads[i];
              if (!(worker->currentTask)) {
                assignTask(worker, nextTask);
                numCurrentTasks[p]++;
                numStandardTasks++;
                queue->timesPassed = 0;
                goto next;
              }
            }

            /* Control should never reach here */
            logMsg(LOG_FATAL, "Task count mismatch for priority %d!", p);
            goto fatal;
          }

          case TASK_TIME_CRITICAL: {
            /* Stop searching if no workers are free */
            if (numCriticalTasks >= criticalCount) {
              /* Enter backup behavior! */
              if (numStandardTasks >= workerCount)
                goto exitLoop;

              /* Find a free standard worker thread */
              for (int i = 0; i < workerCount; i++) {
                WorkerThread_t *worker = workerThreads[i];
                if (!(worker->currentTask)) {
                  assignTask(worker, nextTask);
                  numCurrentTasks[p]++;
                  numStandardTasks++;
                  queue->timesPassed = 0;
                  goto next;
                }
              }

              /* Control should never reach here */
              logMsg(LOG_FATAL, "Task count mismatch for priority %d!", p);
              goto fatal;
            }

            /* Find a free time-critical worker thread */
            for (int i = 0; i < criticalCount; i++) {
              WorkerThread_t *worker = criticalThreads[i];
              if (!(worker->currentTask)) {
                assignTask(worker, nextTask);
                numCurrentTasks[p]++;
                numCriticalTasks++;
                queue->timesPassed = 0;
                goto next;
              }
            }

            /* Control should never reach here */
            logMsg(LOG_FATAL, "Task count mismatch for priority %d!", p);
            goto fatal;
          }

          default: {
            logMsg(LOG_ERROR, "Invalid task priority level: %d", p);
            goto exitLoop;
          }
        }

        next:
        /* Pop the task from its queue */
        queue->first = nextTask->next;
        if (queue->first == NULL)
          queue->last = NULL;
        nextTask->next = NULL;
        continue;
      }

      exitLoop:
      continue;
    }

    /* Try to balance the queues by promoting tasks.  This avoids permanent blockage of a lower-priority task, at the
     * obvious cost of softening the priority levels.  In most cases, this will prove to be an acceptable trade-off.
     * The actual threshold depends on the system; more cores makes for a higher threshold because, in an effort to
     * create parallelism, tasks are split into more chunks.
     */
    for (int p = TASK_HIGHEST_PRIORITY - 1; p >= TASK_LOWEST_PRIORITY; p--) {
      if (!(taskQueue[p].first))
        continue;

      int threshold;
      switch (p) {
        case TASK_LOW_PRIORITY: {
          threshold = MAX(4, cpuCount);
          break;
        }
        case TASK_MEDIUM_PRIORITY: {
          threshold = MAX(2, cpuCount / 2);
          break;
        }
        default: {
          continue;
        }
      }

      if (taskQueue[p].timesPassed >= threshold) {
        ScheduledTask_t *task = taskQueue[p].first;

        /* Pop the task from its original queue */
        taskQueue[p].first = task->next;
        if (taskQueue[p].first == NULL)
          taskQueue[p].last = NULL;
        task->next = NULL;
        task->priority = p + 1;
        numCurrentTasks[p]--;

        /* Push the task into the destination queue */
        if (!(taskQueue[p + 1].first))
          taskQueue[p + 1].first = task;
        if (taskQueue[p + 1].last)
          taskQueue[p + 1].last->next = task;
        taskQueue[p + 1].last = task;
        numCurrentTasks[p + 1]++;
      }
    }

#if 0
    _START_logMsg(LOG_INFO, "Scheduler Iteration");
    logAppend("Low-Priority Tasks:    %2d / %2d", numCurrentTasks[TASK_LOW_PRIORITY],
        MAX(0, MIN(permittedWorkersLowPri,
                permittedWorkers - numCurrentTasks[TASK_MEDIUM_PRIORITY] - numCurrentTasks[TASK_HIGH_PRIORITY] -
                numCurrentTasks[TASK_TIME_CRITICAL])));
    logAppend("Medium-Priority Tasks: %2d / %2d", numCurrentTasks[TASK_MEDIUM_PRIORITY],
        MAX(0, permittedWorkers - numCurrentTasks[TASK_LOW_PRIORITY] - numCurrentTasks[TASK_HIGH_PRIORITY] -
            numCurrentTasks[TASK_TIME_CRITICAL]));
    logAppend("High-Priority Tasks:   %2d / %2d", numCurrentTasks[TASK_HIGH_PRIORITY],
        MAX(0, permittedWorkers - numCurrentTasks[TASK_LOW_PRIORITY] - numCurrentTasks[TASK_MEDIUM_PRIORITY] -
            numCurrentTasks[TASK_TIME_CRITICAL]));
    logAppend("Time-Critical Tasks:   %2d / %2d", numCurrentTasks[TASK_TIME_CRITICAL],
        MAX(4, numCurrentTasks[TASK_TIME_CRITICAL]));
    _END_logMsg();
#endif
  }

  fatal:
  /* This jump is for task count mismatches - fatal because the game is quite screwed if it happens */
  pthread_mutex_unlock(&schedulerLock);

  loaded = SA_FALSE;
  return rtnVal;
}

static void freeScheduler(
  void)
{
  /* Free scheduler thread data */
  if (schedulerLock)
    pthread_mutex_destroy(&schedulerLock);
  if (schedulerAccess)
    pthread_cond_destroy(&schedulerAccess);
  schedulerLock = (pthread_mutex_t) 0;
  schedulerAccess = (pthread_cond_t) 0;
  schedulerThread = (pthread_t) 0;

  /* Clear the task queue */
  for (TaskPriority_t p = 0; p < NUM_TASK_PRIORITY_LEVELS; p++) {
    while (NULL != taskQueue[p].first) {
      ScheduledTask_t *t = taskQueue[p].first;
      taskQueue[p].first = t->next;
      free(t->future);
      free(t);
    }
    taskQueue[p].last = NULL;
    taskQueue[p].timesPassed = 0;
  }

  /* Close object pools */
  closeFuturePool(&futurePool);
  closeTaskPool(&taskPool);
}

static WorkerThread_t* createWorker(
  int workerID,
  int priority,
  int affinity,
  int overlapAffinity)
{
  int err;
  pthread_attr_t *threadAttr = alloca(sizeof(pthread_attr_t));

  struct sched_param schedParam;
  schedParam.sched_priority = priority;

  WorkerThread_t *worker = a_calloc(1, sizeof(WorkerThread_t));
  worker->workerID = workerID;
  worker->currentTask = NULL;
  worker->affinity = affinity;
  worker->overlapAffinity = overlapAffinity;

  /* Set main thread attributes */
  err = pthread_attr_init(threadAttr);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to initialize worker %d thread attribute: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setdetachstate(threadAttr, PTHREAD_CREATE_JOINABLE);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set worker %d thread attribute joinable: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setstacksize(threadAttr, workerThreadStackSize);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set worker %d thread stack size to %'d bytes: (%d) %ls",
        workerID, workerThreadStackSize, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set worker %d thread stack size to %'d bytes", workerID, workerThreadStackSize);
  }
  err = pthread_attr_setinheritsched(threadAttr, PTHREAD_EXPLICIT_SCHED);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set worker %d thread scheduling explicit: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setscope(threadAttr, PTHREAD_SCOPE_PROCESS);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set worker %d thread to process scope: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }
  /* Only SCHED_OTHER is supported on Windows */
  err = pthread_attr_setschedpolicy(threadAttr, SCHED_OTHER);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set worker %d thread scheduling policy to standard: (%d) %ls",
        workerID, err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setschedparam(threadAttr, &schedParam);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set worker %d thread scheduling priority to %d: (%d) %ls",
        workerID, schedParam.sched_priority, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set worker %d thread scheduling priority to %d", workerID, schedParam.sched_priority);
  }

  /* Create mutex object to synchronize worker to other threads */
  err = pthread_mutex_init(&(worker->lock), NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create mutex for worker %d: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }

  /* Create condition object to synchronize worker to other threads */
  err = pthread_cond_init(&(worker->access), NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create condition for worker %d: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }

  /* Create worker thread */
  err = pthread_create(&(worker->thread), threadAttr, runWorker, worker);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create worker %d thread: (%d) %ls", workerID, err, _wcserror(err));
    goto error;
  }

  goto exit;

  error:
  if (worker)
    freeWorker(worker);
  worker = NULL;

  exit:
  if (threadAttr)
    pthread_attr_destroy(threadAttr);
  return worker;
}

/* Expects WorkerThread_t argument */
static void* runWorker(
  void *arg)
{
  void *rtnVal = D2V(SA_SUCCESS);

  /* Set up the worker thread */
  WorkerThread_t *worker = (WorkerThread_t*) arg;
  workerID = worker->workerID;

  /* Set affinity (Windows-specific; TODO Linux version) */
  if (useWorkerAffinity && (worker->affinity >= 0)) {
    HANDLE currThread = GetCurrentThread();
    DWORD_PTR affinityMask = ((DWORD_PTR) 1) << worker->affinity;
    if (worker->overlapAffinity >= 0)
      affinityMask |= ((DWORD_PTR) 1) << worker->overlapAffinity;
    if (0 == SetThreadAffinityMask(currThread, affinityMask)) {
      logMsg(LOG_WARNING, "Failed to set worker %d thread affinity: (%d) %ls",
          workerID, GetLastError(), winError(GetLastError()));
    }
    else {
      if (sizeof(DWORD_PTR) > 4)
        logMsg(LOG_DEBUG, "Set worker %d thread affinity to 0x%016llX", workerID, affinityMask);
      else
        logMsg(LOG_DEBUG, "Set worker %d thread affinity to 0x%08X", workerID, affinityMask);
    }
    if (worker->overlapAffinity >= 0) {
      if (0 > SetThreadIdealProcessor(currThread, worker->overlapAffinity)) {
        logMsg(LOG_WARNING, "Failed to set worker %d thread preferred processor: (%d) %ls",
            workerID, GetLastError(), winError(GetLastError()));
      }
    }
  }

  pthread_mutex_lock(&(worker->lock));

  while (SA_TRUE) {
    /* Sleep until there is something to do. */
    if (!(worker->cmdClose) && (!(worker->currentTask)))
      pthread_cond_wait(&(worker->access), &(worker->lock));

    /* See if we were asked to close - unless there are tasks pending! */
    if (worker->cmdClose && (!(worker->currentTask))) {
      logMsg(LOG_DEBUG, "{%d} Close command received", worker->workerID);
      break;
    }

    /* If there is a task, unlock the mutex so that worker->lock doesn't block for a long time */
    if (worker->currentTask) {
      pthread_mutex_unlock(&(worker->lock));

      ScheduledTask_t *task = worker->currentTask;
      Future_Impl_t *future = task->future;

      /* Run the assigned task and eventually store the result in the future object.  Note that the calling thread
       * (in theory) has access to a pointer referencing this same future object.
       */
      future->result = task->task(task->arg);

      /* We lock the scheduler so that it can be made aware that the worker has finished its task.  If the scheduler
       * is, in turn, attempting to erroneously task this worker thread, then we enter a deadlock!  Unless we want the
       * game to freeze, it's best to avoid that situation.
       */
      pthread_mutex_lock(&(worker->lock));
      pthread_mutex_lock(&(future->lock));
      pthread_mutex_lock(&schedulerLock);

      /* This notifies the scheduler that the worker is done with its task.  After this, the only thing that still
       * references the future object is the original queueTask caller.  However, while it would leak memory and waste
       * scheduling time, throwing futures away will not cause a program fault.
       * This also notifies all the future-binding functions that the task is finished.
       */
      numCurrentTasks[task->priority]--;
      pushTask(&taskPool, task);
      worker->currentTask = NULL;
      future->done = SA_TRUE;
      pthread_cond_broadcast(&(future->access));
      pthread_cond_signal(&schedulerAccess);

      pthread_mutex_unlock(&schedulerLock);
      pthread_mutex_unlock(&(future->lock));

      /* worker->lock remains locked */
    }
  }

  pthread_mutex_unlock(&(worker->lock));

  return rtnVal;
}

static void freeWorker(
  WorkerThread_t *worker)
{
  if (worker == NULL)
    return;

  if (worker->lock)
    pthread_mutex_destroy(&(worker->lock));
  if (worker->access)
    pthread_cond_destroy(&(worker->access));

  free(worker);
}

static void assignTask(
  WorkerThread_t *worker,
  ScheduledTask_t *task)
{
  if (NULL != worker->currentTask) {
    logMsg(LOG_ERROR, "Attempted to task an used worker thread!");
    return;
  }

  pthread_mutex_lock(&(worker->lock));

  /* Set the worker thread onto the new task */
  worker->currentTask = task;

  pthread_cond_signal(&(worker->access));
  pthread_mutex_unlock(&(worker->lock));
}

static int createFuture(
  Future_Impl_t *future)
{
  int rtnVal;
  int err;

  /* Initialize future mutex */
  err = pthread_mutex_init(&(future->lock), NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create mutex: (%d) %ls", err, _wcserror(err));
    rtnVal = genError(SA_GENERIC_ERROR_THREAD, err);
    goto error;
  }

  /* Initialize future condition variable */
  err = pthread_cond_init(&(future->access), NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create condition: (%d) %ls", err, _wcserror(err));
    rtnVal = genError(SA_GENERIC_ERROR_THREAD, err);
    goto error;
  }

  return SA_SUCCESS;

  error:
  return rtnVal;
}

static int destroyFuture(
  Future_Impl_t *future)
{
  pthread_mutex_destroy(&(future->lock));
  pthread_cond_destroy(&(future->access));
  return SA_SUCCESS;
}

DEFINE_POOL_FUNCS(Future_Impl_t, FuturePool_t, static, initFuturePool, closeFuturePool, popFuture, pushFuture);

DEFINE_POOL_FUNCS(ScheduledTask_t, TaskPool_t, static, initTaskPool, closeTaskPool, popTask, pushTask);
