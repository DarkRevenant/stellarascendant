/**********************************************************************************************************************
 * config.h
 * Copyright (C) 2016 Magehand LLC
 *
 * For configuration parameters, parameter names and categories are ASCII-encoded.  Actual parameter content is stored
 * as UTF-8 in the XML file, and is retrieved as wchar_t strings.
 *
 * Global Functions:
 *   loadConfig         Loads an XML config file
 *   forceLoadConfig    Loads an XML config file, overwriting invalid XML files
 *   closeConfig        Closes an XML config file
 *   saveConfig         Saves an XML config file to disk
 *   configSetHeader    Adds a header before the given category
 *   configSetComment   Adds a comment before the given parameter
 *   configRemoveParam  Removes the given parameter
 *   configGetInt       Retrieves an integer from a config file if it exists
 *   configOptInt       Retrieves an integer from a config file if it exists; otherwise, stores it
 *   configSetInt       Stores an integer into a config file
 *   configGetFloat     Retrieves a floating point value from a config file if it exists
 *   configOptFloat     Retrieves a floating point value from a config file if it exists; otherwise, stores it
 *   configSetFloat     Stores a floating point value into a config file
 *   configGetBool      Retrieves a Boolean value from a config file if it exists
 *   configOptBool      Retrieves a Boolean value from a config file if it exists; otherwise, stores it
 *   configSetBool      Stores a Boolean value into a config file
 *   configGetString    Retrieves a string value from a config file if it exists
 *   configOptString    Retrieves a string value from a config file if it exists; otherwise, stores it
 *   configSetString    Stores a string value into a config file
 *********************************************************************************************************************/

#ifndef ___CONFIG_H___
#define ___CONFIG_H___

#include <stdint.h>
#include <wchar.h>

/** Utility Headers **/
#include <iconv.h>
#include <libxml/tree.h>

/**
 ** Typedefs and Definitions
 **/

/* Version info */
#define SA_VERSION 0.0.0_Pre_0
#ifdef _DEBUG
#  ifdef _WIN64
#    ifdef _AVX
#      define SA_BUILD_TYPE "Windows Debug 64-bit with AVX"
#    else
#      define SA_BUILD_TYPE "Windows Debug 64-bit with SSE"
#    endif
#  else
#    ifdef _AVX
#      define SA_BUILD_TYPE "Windows Debug 32-bit with AVX"
#    else
#      define SA_BUILD_TYPE "Windows Debug 32-bit with SSE"
#    endif
#  endif
#else
#  ifdef _WIN64
#    ifdef _AVX
#      define SA_BUILD_TYPE "Windows Release 64-bit with AVX"
#    else
#      define SA_BUILD_TYPE "Windows Release 64-bit with SSE"
#    endif
#  else
#    ifdef _AVX
#      define SA_BUILD_TYPE "Windows Release 32-bit with AVX"
#    else
#      define SA_BUILD_TYPE "Windows Release 32-bit with SSE"
#    endif
#  endif
#endif

#define SA_CONFIG_DIR_PATH W("config")
#define SA_DATA_DIR_PATH   W("data")
#define SA_TEST_DIR_PATH   W("test")

#define SA_LOG_FILE_NAME    W("log")
#define SA_LOG_FILE_EXT     W(".txt")
#define SA_LOG_FILE_INDICES 4

/* Must be lowercase */
#define SA_64_BIT_DIR W("bin-x64")
#define SA_32_BIT_DIR W("bin-x86")

typedef struct {
  const char *name;
  wchar_t *path;

  int _fd;
  xmlDoc *_doc;
  iconv_t _wchar2xml;
  iconv_t _xml2wchar;
  iconv_t _wchar2parse;
  iconv_t _parse2wchar;
} SA_Config_t;

/**
 ** Prototypes
 **/

/* Returns 1 if config file was created, 0 if config file was found, less than 0 otherwise */
extern int loadConfig(
  SA_Config_t *cfg);

/* Returns 2 if the config file was overwritten, 1 if config file was created,
 * 0 if config file was found, and less than 0 otherwise.
 */
extern int forceLoadConfig(
  SA_Config_t *cfg);

/* Does not attempt to save the config */
extern void closeConfig(
  SA_Config_t *cfg);

extern int saveConfig(
  SA_Config_t *cfg);

extern int configSetHeader(
  SA_Config_t *cfg,
  const char *category,
  const wchar_t *header);

extern int configSetComment(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const wchar_t *comment);

extern int configRemoveParam(
  SA_Config_t *cfg,
  const char *category,
  const char *param);

extern int configGetInt(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int64_t *out);

extern int configOptInt(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int64_t *out,
  const int64_t in);

extern int configSetInt(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const int64_t in);

extern int configGetFloat(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  double *out);

extern int configOptFloat(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  double *out,
  const double in);

extern int configSetFloat(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const double in);

extern int configGetBool(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int *out);

extern int configOptBool(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int *out,
  const int in);

extern int configSetBool(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const int in);

/* Out should be freed when no longer needed */
extern int configGetString(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  wchar_t **out);

/* Out should be freed when no longer needed */
extern int configOptString(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  wchar_t **out,
  const wchar_t *in);

extern int configSetString(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const wchar_t *in);

#endif /* ___CONFIG_H___ */
