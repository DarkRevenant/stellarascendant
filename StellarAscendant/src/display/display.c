/**********************************************************************************************************************
 * display.c
 * Copyright (C) 2016 Magehand LLC
 *
 * The display is synonymous with GLFW; therefore, this module involves window handling and input handling, NOT
 * rendering.
 *
 * Global Functions:
 *   initDisplay                  Initializes the display and returns a thread ID
 *   closeDisplay                 Terminates the display
 *   waitForDisplay               Blocks until the display has finished processing a frame
 *   signalDisplay                Signals the display to process one frame - blocks if the previous frame isn't done
 *   isDisplayInitialized         Checks for whether the display is initialized or not
 *
 * Local Functions:
 *   createDisplayThread          Starts a thread for the display
 *   runDisplay                   Entry point to start GLFW and run the display loop
 *   displayLoop                  Performs the immediate work of a display step
 *   terminateDisplay             Does the work of terminating the display
 *   freeDisplay                  Frees memory allocated to the display
 *   errorCallbackGLFW            Callback hook for GLFW errors
 *********************************************************************************************************************/

#include <math.h>
#include <pthread.h>

/** SA Headers **/
#include "display.h"
#include "common.h"
#include "input/control.h"

/** Utility Headers **/
#include <iconv.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 ** Global Variables
 **/

double firstGLFWTime = 0.0;
int64_t displayThreadPri;

double inputFrameRate = 120.00;
double inputFrameTarget = 1.0 / 120.0;

/* It may seem like this would lose precision after a while, but even at 144 Hz gameplay, it would take over 100,000
 * years to reach precision worse than +/- 0.1.
 */
double currInputFrame = 0.0;

/**
 ** Constants
 **/

static const size_t displayThreadStackSize = KBYTES_TO_BYTES(512);

/**
 ** Local Variables
 **/

static pthread_t displayThread = (pthread_t) 0;
static pthread_mutex_t displayLock = (pthread_mutex_t) 0;
static pthread_cond_t displayAccess = (pthread_cond_t) 0;

static int threadActive = SA_FALSE;
static int initialized = SA_FALSE;
static int loaded = SA_FALSE;

static int inputCycle = 0;
static int cmdClose = SA_FALSE;
static int cmdRun = SA_FALSE;
static int cmdCapture = SA_FALSE;

static GLFWwindow *gameWindow = NULL;

/**
 ** Prototypes
 **/

static pthread_t* createDisplayThread(
  void);

static void* runDisplay(
  void *arg);

static int displayLoop(
  void);

static int terminateDisplay(
  void);

static void freeDisplay(
  void);

static void errorCallbackGLFW(
  int error,
  const char* description);

/**
 ** Global Functions
 **/

pthread_t* initDisplay(
  void)
{
  cmdClose = SA_FALSE;
  cmdRun = SA_FALSE;
  cmdCapture = SA_FALSE;

  pthread_t *thread = createDisplayThread();
  if (!thread) {
    logMsg(LOG_ERROR, "Failed to create display thread");
    return NULL;
  }

  return thread;
}

/* Returns SA_SUCCESS */
int closeDisplay(
  void)
{
  /* Early exit if there's nothing left to close */
  if (!isDisplayInitialized())
    goto exit;

  /* Once we get a lock, signal to close and wait until it has actually closed */
  pthread_mutex_lock(&displayLock);
  cmdClose = SA_TRUE;
  pthread_cond_broadcast(&displayAccess);
  pthread_mutex_unlock(&displayLock);

  /* Wait for thread to finish before exiting */
  void *threadRtn;
  pthread_join(displayThread, &threadRtn);

  /* Clean up data */
  exit:
  freeDisplay();

  return SA_SUCCESS;
}

int waitForDisplay(
  void)
{
  if (!isDisplayInitialized())
    return SA_EXIT;

  pthread_mutex_lock(&displayLock);
  while (cmdRun && isDisplayInitialized())
    pthread_cond_wait(&displayAccess, &displayLock);
  pthread_mutex_unlock(&displayLock);

  return SA_SUCCESS;
}

int signalDisplay(
  void)
{
  if (!isDisplayInitialized())
    return SA_EXIT;

  pthread_mutex_lock(&displayLock);

  cmdRun = SA_TRUE;
  cmdCapture = SA_TRUE;

  pthread_cond_signal(&displayAccess);
  while (cmdCapture && isDisplayInitialized())
    pthread_cond_wait(&displayAccess, &displayLock);

  pthread_mutex_unlock(&displayLock);

  return SA_SUCCESS;
}

int isDisplayInitialized(
  void)
{
  return initialized;
}

/**
 ** Local Functions
 **/

static pthread_t* createDisplayThread(
  void)
{
  pthread_attr_t *displayThreadAttr = alloca(sizeof(pthread_attr_t));

  int err;
  pthread_t *rtnVal = &displayThread;

  struct sched_param schedParam;
  schedParam.sched_priority = displayThreadPri;

  /* Set display thread attributes */
  err = pthread_attr_init(displayThreadAttr);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to initialize display thread attribute: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setdetachstate(displayThreadAttr, PTHREAD_CREATE_JOINABLE);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set display thread attribute joinable: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setstacksize(displayThreadAttr, displayThreadStackSize);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set display thread stack size to %'d bytes: (%d) %ls",
        displayThreadStackSize, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set display thread stack size to %'d bytes", displayThreadStackSize);
  }
  err = pthread_attr_setinheritsched(displayThreadAttr, PTHREAD_EXPLICIT_SCHED);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set display thread scheduling explicit: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setscope(displayThreadAttr, PTHREAD_SCOPE_PROCESS);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set display thread to process scope: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  /* Only SCHED_OTHER is supported on Windows */
  err = pthread_attr_setschedpolicy(displayThreadAttr, SCHED_OTHER);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set display thread scheduling policy to standard: (%d) %ls", err, _wcserror(err));
    goto error;
  }
  err = pthread_attr_setschedparam(displayThreadAttr, &schedParam);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to set display thread scheduling priority to %d: (%d) %ls",
        schedParam.sched_priority, err, _wcserror(err));
    goto error;
  }
  else {
    logMsg(LOG_DEBUG, "Set display thread scheduling priority to %d", schedParam.sched_priority);
  }

  /* Create mutex object to synchronize display to other threads */
  err = pthread_mutex_init(&displayLock, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create mutex: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Create condition object to synchronize display to other threads */
  err = pthread_cond_init(&displayAccess, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create condition: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Create display thread */
  threadActive = SA_TRUE;
  err = pthread_create(&displayThread, displayThreadAttr, runDisplay, NULL);
  if (0 != err) {
    logMsg(LOG_ERROR, "Failed to create display thread: (%d) %ls", err, _wcserror(err));
    goto error;
  }

  /* Wait for the display systems to start up */
  pthread_mutex_lock(&displayLock);
  while (!loaded && threadActive)
    pthread_cond_wait(&displayAccess, &displayLock);
  pthread_mutex_unlock(&displayLock);

  if (!threadActive)
    goto error;
  else
    goto exit;

  error:
  freeDisplay();
  rtnVal = NULL;

  exit:
  if (displayThreadAttr)
    pthread_attr_destroy(displayThreadAttr);
  return rtnVal;
}

static void* runDisplay(
  void *arg)
{
  int err;
  void *rtnVal = D2V(SA_SUCCESS);
  double currSpinMargin = MSEC_TO_SEC(1.0);

  /* Set affinity (Windows-specific; TODO Linux version) */
  if (primaryThreads >= 0) {
    HANDLE currThread = GetCurrentThread();
    if (0 == SetThreadAffinityMask(currThread, (DWORD_PTR) primaryAffinityMask))
      logMsg(LOG_WARNING, "Failed to set display thread affinity: (%d) %ls", GetLastError(), winError(GetLastError()));
  }

  /* Initialize GLFW */
  glfwSetErrorCallback(errorCallbackGLFW);

  if (GLFW_TRUE != glfwInit()) {
    logMsg(LOG_ERROR, "GLFW initialization failed!");
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  else {
    firstGLFWTime = USEC_TO_SEC((double ) getSysTime() - initSysTime);
    initialized = SA_TRUE;
    logMsg(LOG_INFO, "GLFW initialized");
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  gameWindow = glfwCreateWindow(640, 480, "Stellar Ascendant", NULL, NULL);
  if (!gameWindow) {
    logMsg(LOG_ERROR, "GLFW window failed to open!");
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }

  /* TODO: Move this to separate thread */
  /* TODO: Create actual graphics thread (and adjust priorities) for the rendering. */
  glfwMakeContextCurrent(gameWindow);

  /* Initialize controls subsystem */
  err = initControls(gameWindow);
  if (SA_SUCCESS != err) {
    logMsg(LOG_ERROR, "Controls failed to initialize: (%d) %ls", err, getError(err));
    rtnVal = D2V(err);
    goto exit;
  }

  /* Initialize glad */
  if (0 == gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
    logMsg(LOG_ERROR, "glad load failed!");
    rtnVal = D2V(SA_FAILURE);
    goto exit;
  }
  else {
    logMsg(LOG_INFO, "glad loaded");
  }

  /* Signal to the world that the display is good to go */
  pthread_mutex_lock(&displayLock);
  loaded = SA_TRUE;
  pthread_cond_broadcast(&displayAccess);
  pthread_mutex_unlock(&displayLock);

  /* Loop continuously */
  pthread_mutex_lock(&displayLock);

  double start = getTime();
  double elapsed = inputFrameTarget;
  while (SA_TRUE) {
    /* Come up with the number of self-serve inputs we want */
    int subdivide = MAX(1, (int ) round(frameTarget / inputFrameTarget));

    start = getTime();

    /* Adjust for variance in waits, signals, etc. */
    double target = inputFrameTarget - MAX(0.0, elapsed - inputFrameTarget);

    /* Start the timer fresh if the main loop wants to poll input right now */
    if (cmdRun) {
      target = inputFrameTarget;
      currInputFrame = (double) currFrame;
    }
    else {
      /* Advance by a predictable amount, rather than relying on the real-time clock. This still leaves the possibility
       * that an input frame will be skipped, so it's not *purely* deterministic if the computer can't keep up.  For
       * this reason, serious TAS users will need to set the input frequency equal to the frame rate.
       */
      currInputFrame += inputFrameTarget / frameTarget;
    }

    err = displayLoop();
    if (SA_SUCCESS != err) {
      if (SA_EXIT == err) {
        /* Unexceptional exit */
        logMsg(LOG_INFO, "Display loop signaled exit");
        break;
      }
      else {
        logMsg(LOG_ERROR, "Display loop failure: (%d) %ls", err, getError(err));
        rtnVal = D2V(err);
        break;
      }
    }

    /* Alert main thread of state changes */
    pthread_cond_signal(&displayAccess);

    /* If the last frame went over budget, continue to the next frame right away */
    elapsed = getTime() - start;
    if (elapsed >= target) {
      /* But first let other threads have a turn */
      pthread_mutex_unlock(&displayLock);
      pthread_mutex_lock(&displayLock);

      elapsed = getTime() - start;
      continue;
    }

    if (inputCycle < (subdivide - 1)) {
      /* Wait until the next input cycle is supposed to start, or wake up early if a signal comes in */
      //    struct timespec t;
      //    t.tv_sec = (time_t) floor(target - elapsed);
      //    t.tv_nsec = (long) SEC_TO_NSEC((target - elapsed) - t.tv_sec);
      //    pthread_cond_timedwait_relative_np(&displayAccess, &displayLock, &t);
      struct timespec t = { 0, 1 * (NSEC_PER_SEC / MSEC_PER_SEC) };
      int locked = SA_TRUE;
      while (!cmdRun && (elapsed < target)) {
        /* Relinquish if it's safe, to save power/performance; otherwise, spin */
        if (elapsed < (target - currSpinMargin)) {
          pthread_cond_timedwait_relative_np(&displayAccess, &displayLock, &t);
          elapsed = getTime() - start;
          if (elapsed > target)
            currSpinMargin += MSEC_TO_SEC(0.1);
          else if (elapsed > (target - currSpinMargin))
            currSpinMargin -= MSEC_TO_SEC(0.001);
        }
        else if (locked) {
          pthread_mutex_unlock(&displayLock);
          locked = SA_FALSE;
        }
        elapsed = getTime() - start;
      }

      if (!locked)
        pthread_mutex_lock(&displayLock);
    }
    else {
      /* Otherwise, just wait */
      pthread_cond_wait(&displayAccess, &displayLock);
    }

    elapsed = getTime() - start;
  }

  pthread_cond_broadcast(&displayAccess);
  pthread_mutex_unlock(&displayLock);

  exit:
  rtnVal = D2V(terminateDisplay());
  return rtnVal;
}

/* This entire function is assumed to run under the mutex displayLock */
static int displayLoop(
  void)
{
  /* Get the inputs! */
  pollInputs();
  execKeymap();
  cmdCapture = SA_FALSE;
  inputCycle++;

  /* See if we were asked to close */
  if (cmdClose || glfwWindowShouldClose(gameWindow)) {
    logMsg(LOG_DEBUG, "Close command received");
    return SA_EXIT;
  }

  /* Stop now if we don't have to run */
  if (!cmdRun)
    return SA_SUCCESS;

  /* Report that the display has finished running the frame */
  cmdRun = SA_FALSE;
  inputCycle = 0;
  return SA_SUCCESS;
}

static int terminateDisplay(
  void)
{
  /* glad does not need to be terminated */

  /* Close the controls subsystem */
  closeControls();

  /* Terminate GLFW */
  if (gameWindow)
    glfwDestroyWindow(gameWindow);

  glfwTerminate();
  logMsg(LOG_INFO, "GLFW terminated");

  /* Tell the world we're done! */
  pthread_mutex_lock(&displayLock);
  initialized = SA_FALSE;
  loaded = SA_FALSE;
  threadActive = SA_FALSE;
  pthread_cond_broadcast(&displayAccess);
  pthread_mutex_unlock(&displayLock);

  return SA_SUCCESS;
}

static void freeDisplay(
  void)
{
  if (displayLock)
    pthread_mutex_destroy(&displayLock);
  if (displayAccess)
    pthread_cond_destroy(&displayAccess);
  displayLock = (pthread_mutex_t) 0;
  displayAccess = (pthread_cond_t) 0;
  displayThread = (pthread_t) 0;
}

static void errorCallbackGLFW(
  int error,
  const char* description)
{
  logMsg(LOG_ERROR, "GLFW error %d: %s", error, description);
}
