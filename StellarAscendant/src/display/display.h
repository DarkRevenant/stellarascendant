/**********************************************************************************************************************
 * display.h
 * Copyright (C) 2016 Magehand LLC
 *
 * The display is synonymous with GLFW; therefore, this module involves window handling and input handling, NOT
 * rendering.
 *
 * Global Functions:
 *   initDisplay                  Initializes the display and returns a thread ID
 *   closeDisplay                 Terminates the display
 *   waitForDisplay               Blocks until the display has finished processing a frame
 *   signalDisplay                Signals the display to process one frame - blocks if the previous frame isn't done
 *   isDisplayInitialized         Checks for whether the display is initialized or not
 *********************************************************************************************************************/

#ifndef ___DISPLAY_H___
#define ___DISPLAY_H___

#include <pthread.h>
#include <stdint.h>

/** Utility Headers **/
#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 ** Global Variables
 **/

extern double firstGLFWTime;
extern int64_t displayThreadPri;

extern double inputFrameRate;
extern double inputFrameTarget;

extern double currInputFrame;

/**
 ** Prototypes
 **/

extern pthread_t* initDisplay(
  void);

/* Returns SA_SUCCESS */
extern int closeDisplay(
  void);

extern int waitForDisplay(
  void);

extern int signalDisplay(
  void);

extern int isDisplayInitialized(
  void);

#endif /* ___DISPLAY_H___ */
