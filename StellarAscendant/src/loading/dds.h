/**********************************************************************************************************************
 * dds.h
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   ddsLoad       Loads a DDS image from the given file path
 *   freeImage     Destroys a SA_Image_t struct
 *********************************************************************************************************************/

#ifndef ___DDS_H___
#define ___DDS_H___

/**
 ** Typedefs
 **/

typedef enum {
  /* Basic uncompressed texture formats */
  SA_TEXTURE_FORMAT_RGB,
  SA_TEXTURE_FORMAT_RGBA,

  /* DXT compressed texture formats */
  SA_TEXTURE_FORMAT_DXT1,
  SA_TEXTURE_FORMAT_DXT3,
  SA_TEXTURE_FORMAT_DXT5,

  NUM_SA_TEXTURE_FORMATS
} SA_TextureFormat_t;

typedef struct {
  SA_TextureFormat_t format;
  size_t width;
  size_t height;
  size_t levels;
  size_t *levelSizes;
  void **pixelData;
} SA_Image_t;

/**
 ** Prototypes
 **/

extern int ddsLoad(
  const wchar_t *filePath,
  SA_Image_t **image);

extern void freeImage(
  SA_Image_t *image);

#endif /* ___DDS_H___ */
