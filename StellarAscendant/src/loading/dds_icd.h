/**********************************************************************************************************************
 * dds_icd.h
 * Copyright (C) 2016 Magehand LLC
 * DDS structures and definitions are reproduced from Microsoft DirectX documentation.
 *********************************************************************************************************************/

#ifndef ___DDS_ICD_H___
#define ___DDS_ICD_H___

#include <stdint.h>

/**
 ** Typedefs and Definitions
 **/

#define DDPF_ALPHAPIXELS (0x00000001)
#define DDPF_ALPHA       (0x00000002)
#define DDPF_FOURCC      (0x00000004)
#define DDPF_RGB         (0x00000040)
#define DDPF_YUV         (0x00000200)
#define DDPF_LUMINANCE   (0x00020000)

#define DDSD_CAPS        (0x00000001)
#define DDSD_HEIGHT      (0x00000002)
#define DDSD_WIDTH       (0x00000004)
#define DDSD_PITCH       (0x00000008)
#define DDSD_PIXELFORMAT (0x00001000)
#define DDSD_MIPMAPCOUNT (0x00020000)
#define DDSD_LINEARSIZE  (0x00080000)
#define DDSD_DEPTH       (0x00800000)

#define DDSCAPS_COMPLEX  (0x00000008)
#define DDSCAPS_MIPMAP   (0x00400000)
#define DDSCAPS_TEXTURE  (0x00001000)

#define DDSCAPS2_CUBEMAP           (0x00000200)
#define DDSCAPS2_CUBEMAP_POSITIVEX (0x00000400)
#define DDSCAPS2_CUBEMAP_NEGATIVEX (0x00000800)
#define DDSCAPS2_CUBEMAP_POSITIVEY (0x00001000)
#define DDSCAPS2_CUBEMAP_NEGATIVEY (0x00002000)
#define DDSCAPS2_CUBEMAP_POSITIVEZ (0x00004000)
#define DDSCAPS2_CUBEMAP_NEGATIVEZ (0x00008000)
#define DDSCAPS2_VOLUME            (0x00200000)

typedef struct {
  uint32_t size;
  uint32_t flags;
  uint32_t fourCC;
  uint32_t rgbBitCount;
  uint32_t rBitMask;
  uint32_t gBitMask;
  uint32_t bBitMask;
  uint32_t aBitMask;
} DDS_PixelFormat_t;

typedef struct {
  uint32_t size;
  uint32_t flags;
  uint32_t height;
  uint32_t width;
  uint32_t pitchOrLinearSize;
  uint32_t depth;
  uint32_t mipMapCount;
  uint32_t reserved1[11];
  DDS_PixelFormat_t ddspf;
  uint32_t caps;
  uint32_t caps2;
  uint32_t caps3;
  uint32_t caps4;
  uint32_t reserved2;
} DDS_Header_t;

typedef struct {
  uint32_t magic;
  DDS_Header_t header;
  uint8_t bdata[];
} DDS_File_t;

#endif /* ___DDS_ICD_H___ */
