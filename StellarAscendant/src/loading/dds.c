/**********************************************************************************************************************
 * dds.c
 * Copyright (C) 2016 Magehand LLC
 *
 * Global Functions:
 *   ddsLoad       Loads a DDS image from the given file path
 *   freeImage     Destroys a SA_Image_t struct
 *********************************************************************************************************************/

#include <stdio.h>

/** SA Headers **/
#include "loading/dds.h"
#include "loading/dds_icd.h"
#include "common.h"

/**
 ** Typedefs
 **/

typedef union {
  char chars[4 + 1];
  uint32_t code;
} FourCC_t;

/**
 ** Constants
 **/

static const uint32_t ddsMagicNum = 0x20534444;

static const FourCC_t fourCCDXT1 = { { 'D', 'X', 'T', '1', '\0' } };
static const FourCC_t fourCCDXT2 = { { 'D', 'X', 'T', '2', '\0' } };
static const FourCC_t fourCCDXT3 = { { 'D', 'X', 'T', '3', '\0' } };
static const FourCC_t fourCCDXT4 = { { 'D', 'X', 'T', '4', '\0' } };
static const FourCC_t fourCCDXT5 = { { 'D', 'X', 'T', '5', '\0' } };
static const FourCC_t fourCCDX10 = { { 'D', 'X', '1', '0', '\0' } };

/**
 ** Global Functions
 **/

int ddsLoad(
  const wchar_t *filePath,
  SA_Image_t **image)
{
  /* Establish some basic assumptions about structure sizes */
  _Static_assert(sizeof(DDS_Header_t) == 124, W("DDS Header size mismatch"));
  _Static_assert(sizeof(DDS_PixelFormat_t) == 32, W("DDS PixelFormat size mismatch"));

  SA_Image_t *ourImage = NULL;
  FILE *ddsFile = NULL;
  int rtnVal = SA_SUCCESS;

  if (!filePath) {
    logMsg(LOG_WARNING, "No file path given");
    rtnVal = genError(SA_ERROR_ARGS, errno);
    goto error;
  }

  ddsFile = _wfopen(filePath, W("rb"));
  size_t itemsRead = 0;

  if (!ddsFile) {
    logMsg(LOG_DEBUG, "%ls: Could not open file: (%d) %ls", filePath, errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
    goto error;
  }

  /* First, read the magic number */
  uint32_t magic;
  itemsRead = fread(&magic, sizeof(magic), 1, ddsFile);
  if (1 > itemsRead) {
    if (feof(ddsFile)) {
      logMsg(LOG_DEBUG, "%ls: File is too short; size was under %d bytes", filePath, sizeof(magic));
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
    else {
      logMsg(LOG_WARNING, "%ls: Error reading file magic number", filePath);
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
  }
  else if (ddsMagicNum != magic) {
    logMsg(LOG_DEBUG, "%ls: Invalid magic number: %08X", filePath, magic);
    rtnVal = SA_ERROR_FILE_INVALID;
    goto error;
  }

  /* Next, read the standard header */
  DDS_Header_t header;
  itemsRead = fread(&header, sizeof(header), 1, ddsFile);
  if (1 > itemsRead) {
    if (feof(ddsFile)) {
      logMsg(LOG_DEBUG, "%ls: File is too short; header size was under %d bytes", filePath, sizeof(DDS_Header_t));
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
    else {
      logMsg(LOG_WARNING, "%ls: Error reading file header", filePath);
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
  }
  else if (sizeof(DDS_Header_t) != header.size) {
    logMsg(LOG_DEBUG, "%ls: Invalid header size field: %d", filePath, header.size);
    rtnVal = SA_ERROR_FILE_INVALID;
    goto error;
  }
  else if (sizeof(DDS_PixelFormat_t) != header.ddspf.size) {
    logMsg(LOG_DEBUG, "%ls: Invalid pixelformat size field: %d", filePath, header.ddspf.size);
    rtnVal = SA_ERROR_FILE_INVALID;
    goto error;
  }

  /* If we support the DX10 header, we'll read it here */
  if ((header.ddspf.flags & DDPF_FOURCC) && (fourCCDX10.code == header.ddspf.fourCC)) {
    logMsg(LOG_DEBUG, "%ls: DirectX 10 header is unsupported", filePath);
    rtnVal = SA_ERROR_FILE_UNSUPPORTED;
    goto error;
  }

  /* Additional validity checks */
  if (!((header.flags & DDSD_HEIGHT) && (header.flags & DDSD_WIDTH)) ||
      ((header.flags & DDSD_PITCH) && (header.flags & DDSD_LINEARSIZE)))
  {
    logMsg(LOG_DEBUG, "%ls: Invalid header flags: %08X", filePath, header.flags);
    rtnVal = SA_ERROR_FILE_INVALID;
    goto error;
  }
  if ((header.flags & DDSD_DEPTH) || (header.caps & DDSCAPS2_VOLUME)) {
    logMsg(LOG_DEBUG, "%ls: Depth/volume textures are unsupported", filePath);
    rtnVal = SA_ERROR_FILE_UNSUPPORTED;
    goto error;
  }
  if (header.caps & DDSCAPS2_CUBEMAP) {
    logMsg(LOG_DEBUG, "%ls: Cube maps are unsupported", filePath);
    rtnVal = SA_ERROR_FILE_UNSUPPORTED;
    goto error;
  }
  if (!(header.flags & DDSD_CAPS))
    logMsg(LOG_WARNING, "%ls: Header does not contain capabilities flag", filePath);
  if (!(header.flags & DDSD_PIXELFORMAT))
    logMsg(LOG_WARNING, "%ls: Header does not contain pixel format flag", filePath);
  if (!(header.caps & DDSCAPS_TEXTURE))
    logMsg(LOG_WARNING, "%ls: Header does not contain texture capabilities flag", filePath);

  /* Get basic information */
  size_t width = header.width;
  size_t height = header.height;
  size_t mipmapLevels = MAX(1, header.mipMapCount);

  /* More validity checks */
  if ((mipmapLevels > 1) && !(header.flags & DDSD_MIPMAPCOUNT))
    logMsg(LOG_WARNING, "%ls: Mipmapped but header does not contain mipmaps flag", filePath);
  if ((mipmapLevels > 1) && !(header.caps & DDSCAPS_COMPLEX))
    logMsg(LOG_WARNING, "%ls: Mipmapped but header does not contain complex capabilities flag", filePath);
  if ((mipmapLevels > 1) && !(header.caps & DDSCAPS_MIPMAP))
    logMsg(LOG_WARNING, "%ls: Mipmapped but header does not contain mipmap capabilities flag", filePath);
  if ((header.caps & DDSCAPS_MIPMAP) && (mipmapLevels <= 1))
    logMsg(LOG_WARNING, "%ls: Not mipmapped but header contains mipmap capabilities flag", filePath);

  /* Get texture format */
  SA_TextureFormat_t format;
  uint32_t redMask = 0;
  uint32_t greenMask = 0;
  uint32_t blueMask = 0;
  uint32_t alphaMask = 0;
  size_t blockSize = 0;
  if (header.ddspf.flags & DDPF_FOURCC) {
    /* Compressed */
    if (fourCCDXT1.code == header.ddspf.fourCC) {
      /* DXT1 format */
      format = SA_TEXTURE_FORMAT_DXT1;
      blockSize = 8;
    }
    else if (fourCCDXT3.code == header.ddspf.fourCC) {
      /* DXT3 format */
      format = SA_TEXTURE_FORMAT_DXT3;
      blockSize = 16;
    }
    else if (fourCCDXT5.code == header.ddspf.fourCC) {
      /* DXT5 format */
      format = SA_TEXTURE_FORMAT_DXT5;
      blockSize = 16;
    }
    else if ((fourCCDXT2.code == header.ddspf.fourCC) || (fourCCDXT4.code == header.ddspf.fourCC)) {
      FourCC_t fourCode;
      fourCode.code = header.ddspf.fourCC;
      logMsg(LOG_DEBUG, "%ls: Unsupported compression format: %s", filePath, fourCode.chars);
      rtnVal = SA_ERROR_FILE_UNSUPPORTED;
      goto error;
    }
    else {
      FourCC_t fourCode;
      fourCode.code = header.ddspf.fourCC;
      logMsg(LOG_DEBUG, "%ls: Unrecognized compression format: %s", filePath, fourCode.chars);
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
  }
  else {
    /* Uncompressed */
    if (header.ddspf.flags & DDPF_ALPHA) {
      logMsg(LOG_DEBUG, "%ls: Alpha-only textures are unsupported", filePath);
      rtnVal = SA_ERROR_FILE_UNSUPPORTED;
      goto error;
    }
    else if ((header.ddspf.flags & DDPF_LUMINANCE) && (header.ddspf.flags & DDPF_ALPHAPIXELS)) {
      logMsg(LOG_DEBUG, "%ls: Two-channel textures are unsupported", filePath);
      rtnVal = SA_ERROR_FILE_UNSUPPORTED;
      goto error;
    }
    else if (header.ddspf.flags & DDPF_LUMINANCE) {
      logMsg(LOG_DEBUG, "%ls: Single-channel textures are unsupported", filePath);
      rtnVal = SA_ERROR_FILE_UNSUPPORTED;
      goto error;
    }
    else if ((header.ddspf.flags & DDPF_YUV)) {
      logMsg(LOG_DEBUG, "%ls: YUV textures are unsupported", filePath);
      rtnVal = SA_ERROR_FILE_UNSUPPORTED;
      goto error;
    }
    else if (header.ddspf.flags & DDPF_RGB) {
      if (header.ddspf.flags & DDPF_ALPHAPIXELS) {
        /* RGBA format */
        format = SA_TEXTURE_FORMAT_RGBA;
        redMask = header.ddspf.rBitMask;
        greenMask = header.ddspf.gBitMask;
        blueMask = header.ddspf.bBitMask;
        alphaMask = header.ddspf.aBitMask;

        if ((redMask != 0xff000000) || (greenMask != 0x00ff0000) ||
            (blueMask != 0x0000ff00) || (alphaMask != 0x000000ff))
        {
          logMsg(LOG_DEBUG, "%ls: RGBA layout unsupported: %08X, %08X, %08X, %08X",
              filePath, redMask, greenMask, blueMask, alphaMask);
          rtnVal = SA_ERROR_FILE_UNSUPPORTED;
          goto error;
        }
      }
      else {
        /* RGB format */
        format = SA_TEXTURE_FORMAT_RGB;
        redMask = header.ddspf.rBitMask;
        greenMask = header.ddspf.gBitMask;
        blueMask = header.ddspf.bBitMask;

        if ((redMask != 0xff0000) || (greenMask != 0x00ff00) ||
            (blueMask != 0x0000ff))
        {
          logMsg(LOG_DEBUG, "%ls: RGB layout unsupported: %06X, %06X, %06X",
              filePath, redMask, greenMask, blueMask);
          rtnVal = SA_ERROR_FILE_UNSUPPORTED;
          goto error;
        }
      }
    }
    else {
      logMsg(LOG_DEBUG, "%ls: Does not have a pixel format", filePath);
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
  }

  /* Create the image struct we intend to output */
  ourImage = calloc(1, sizeof(SA_Image_t));
  if (!ourImage) {
    logMsg(LOG_ERROR, "%ls: Failed to allocate %'d bytes", filePath, sizeof(SA_Image_t));
    rtnVal = SA_ERROR_MEMORY;
    goto error;
  }
  LOG_IMAGE_MEM(sizeof(SA_Image_t));
  ourImage->format = format;
  ourImage->levelSizes = a_calloc(mipmapLevels, sizeof(size_t));
  ourImage->width = width;
  ourImage->height = height;
  LOG_IMAGE_MEM(mipmapLevels * sizeof(size_t));

  /* Get file layout */
  size_t pitch = 0; /* We don't actually use this */
  size_t bitsPerPixel = 0;
  switch (format) {
    case SA_TEXTURE_FORMAT_RGB:
    case SA_TEXTURE_FORMAT_RGBA: {
      /* Basic uncompressed */
      if (!(header.flags & DDSD_PITCH)) {
        logMsg(LOG_DEBUG, "%ls: Uncompressed texture is missing pitch flag", filePath);
        rtnVal = SA_ERROR_FILE_INVALID;
        goto error;
      }

      pitch = header.pitchOrLinearSize;
      bitsPerPixel = header.ddspf.rgbBitCount;
      size_t realPitch = (width * bitsPerPixel + 7) / 8;
      if (pitch != realPitch) {
        logMsg(LOG_WARNING, "%ls: File specifies incorrect pitch: %d", filePath, pitch);
        pitch = realPitch;
      }

      size_t levelWidth = width;
      size_t levelHeight = height;
      size_t level;
      for (level = 0; (level < mipmapLevels) && (levelWidth > 0) && (levelHeight > 0); level++) {
        ourImage->levelSizes[level] = ((levelWidth * bitsPerPixel + 7) / 8) * levelHeight;
        levelWidth /= 2;
        levelHeight /= 2;
      }

      if (level != mipmapLevels) {
        logMsg(LOG_WARNING, "%ls: File specifies %d mipmap levels, contains %d", filePath, mipmapLevels, level);
        mipmapLevels = level;
      }

      break;
    }

    case SA_TEXTURE_FORMAT_DXT1:
    case SA_TEXTURE_FORMAT_DXT3:
    case SA_TEXTURE_FORMAT_DXT5: {
      /* DXT compressed */
      if (!(header.flags & DDSD_LINEARSIZE)) {
        logMsg(LOG_DEBUG, "%ls: Compressed texture is missing linear size flag", filePath);
        rtnVal = SA_ERROR_FILE_INVALID;
        goto error;
      }

      /* linearSize = header.pitchOrLinearSize; */
      pitch = MAX(1, ((width + 3) / 4)) * blockSize;

      size_t levelWidth = width;
      size_t levelHeight = height;
      size_t level;
      for (level = 0; (level < mipmapLevels) && (levelWidth > 0) && (levelHeight > 0); level++) {
        ourImage->levelSizes[level] = MAX(1, ((levelWidth + 3) / 4)) * MAX(1, ((levelHeight + 3) / 4)) * blockSize;
        levelWidth /= 2;
        levelHeight /= 2;
      }

      if (level != mipmapLevels) {
        logMsg(LOG_WARNING, "%ls: File specifies %d mipmap levels, contains %d", filePath, mipmapLevels, level);
        mipmapLevels = level;
      }

      break;
    }

    default: {
      /* We screwed up somewhere */
      logMsg(LOG_WARNING, "%ls: Invalid format: %d", filePath, format);
      rtnVal = SA_ERROR_FILE_INVALID;
      goto error;
    }
  }

  /* Commit to # of mipmap levels */
  ourImage->levels = mipmapLevels;
  ourImage->pixelData = calloc(mipmapLevels, sizeof(void*));
  if (!(ourImage->pixelData)) {
    logMsg(LOG_ERROR, "%ls: Failed to allocate %'d bytes", filePath, mipmapLevels * sizeof(void*));
    rtnVal = SA_ERROR_MEMORY;
    goto error;
  }
  LOG_IMAGE_MEM(mipmapLevels * sizeof(void*));

  /* Read data */
  for (size_t i = 0; i < mipmapLevels; i++) {
    /* Allocate memory for this level's texture data */
    ourImage->pixelData[i] = malloc(ourImage->levelSizes[i]);
    if (!(ourImage->pixelData[i])) {
      logMsg(LOG_ERROR, "%ls: Failed to allocate %'d bytes", filePath, ourImage->levelSizes[i]);
      rtnVal = SA_ERROR_MEMORY;
      goto error;
    }
    LOG_IMAGE_MEM(ourImage->levelSizes[i]);

    /* Finally read the file to fill the newly-allocated buffer */
    itemsRead = fread(ourImage->pixelData[i], ourImage->levelSizes[i], 1, ddsFile);
    if (1 > itemsRead) {
      if (feof(ddsFile)) {
        logMsg(LOG_DEBUG, "%ls: File is too short; mipmap %d size was under %'d bytes",
            filePath, i, ourImage->levelSizes[i]);
        rtnVal = SA_ERROR_FILE_INVALID;
        goto error;
      }
      else {
        logMsg(LOG_WARNING, "%ls: Error reading file image data", filePath);
        rtnVal = SA_ERROR_FILE_INVALID;
        goto error;
      }
    }
  }

#if 0
  logMsg(LOG_INFO, "%ls: Texture information", filePath);
  logAppend(LOG_INFO, "Format:     %d", format);
  logAppend(LOG_INFO, "Dimensions: %d x %d pixels", width, height);
  if (mipmapLevels > 1)
  logAppend(LOG_INFO, "Mipmap:     %d levels", mipmapLevels);
  if (pitch > 0)
  logAppend(LOG_INFO, "Pitch:      %d bytes", pitch);
  size_t totalBytes = 0;
  for (size_t i = 0; i < ourImage->mipmaps; i++) {
    totalBytes += levelBytes[i];
  }
  logAppend(LOG_INFO, "Data Size:  %'d bytes", totalBytes);
#endif

  /* Successful completion */
  *image = ourImage;
  goto exit;

  error:
  if (ourImage)
    freeImage(ourImage);

  exit:
  if (ddsFile)
    fclose(ddsFile);

  return rtnVal;
}

void freeImage(
  SA_Image_t *image)
{
  if (!image) {
    logMsg(LOG_WARNING, "Argument is NULL");
    return;
  }

  if (image->pixelData) {
    for (size_t i = 0; i < image->levels; i++) {
      if (image->pixelData[i]) {
        free(image->pixelData[i]);
        LOG_IMAGE_MEM(-(image->levelSizes[i]));
      }
    }
    free(image->pixelData);
    LOG_IMAGE_MEM(-(image->levels * sizeof(void*)));
  }

  if (image->levelSizes) {
    free(image->levelSizes);
    LOG_IMAGE_MEM(-(image->levels * sizeof(size_t)));
  }

  free(image);
  LOG_IMAGE_MEM(-(sizeof(SA_Image_t)));
}
