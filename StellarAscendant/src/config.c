/**********************************************************************************************************************
 * config.c
 * Copyright (C) 2016 Magehand LLC
 *
 * For configuration parameters, parameter names and categories are ASCII-encoded.  Actual parameter content is stored
 * as UTF-8 in the XML file, and is retrieved as wchar_t strings.
 *
 * Global Functions:
 *   loadConfig         Loads an XML config file
 *   forceLoadConfig    Loads an XML config file, overwriting invalid XML files
 *   closeConfig        Closes an XML config file
 *   saveConfig         Saves an XML config file to disk
 *   configSetHeader    Adds a header before the given category
 *   configSetComment   Adds a comment before the given parameter
 *   configGetInt       Retrieves an integer from a config file if it exists
 *   configOptInt       Retrieves an integer from a config file if it exists; otherwise, stores it
 *   configSetInt       Stores an integer into a config file
 *   configGetFloat     Retrieves a floating point value from a config file if it exists
 *   configOptFloat     Retrieves a floating point value from a config file if it exists; otherwise, stores it
 *   configSetFloat     Stores a floating point value into a config file
 *   configGetBool      Retrieves a Boolean value from a config file if it exists
 *   configOptBool      Retrieves a Boolean value from a config file if it exists; otherwise, stores it
 *   configSetBool      Stores a Boolean value into a config file
 *   configGetString    Retrieves a string value from a config file if it exists
 *   configOptString    Retrieves a string value from a config file if it exists; otherwise, stores it
 *   configSetString    Stores a string value into a config file
 *
 * Local Functions:
 *   getNodeContent     Retrieves the content of an XML node
 *   setNodeContent     Replaces the content of an XML node
 *   getNode            Retrieves an XML node from a configuration file
 *   isParamType        Determines if parameter's XML node has a particular type
 *   findChild          Searches an XML node's children by name
 *   formatComment      Formats a config comment
 *********************************************************************************************************************/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

/** SA Headers **/
#include "config.h"
#include "common.h"

/** Utility Headers **/
#include <iconv.h>
#include <libxml/encoding.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlsave.h>

/**
 ** Definitions
 **/

#define CONFIG_FILE_XML_VERSION "1.0"
#define CONFIG_FILE_ENCODING    "UTF-8"
#define COMMENT_PARSE_ENCODING  "UTF-32LE"

#define PARAMETER_TYPE         "type"
#define PARAMETER_TYPE_INTEGER "int"
#define PARAMETER_TYPE_FLOAT   "float"
#define PARAMETER_TYPE_BOOLEAN "bool"
#define PARAMETER_TYPE_STRING  "string"

#define PARAMETER_BOOLEAN_TRUE  W("true")
#define PARAMETER_BOOLEAN_FALSE W("false")

#define XML_STRING_BUFFER_LEN (2048)
#define COMMENT_BUFFER_LEN    (16384)
#define COMMENT_LINE_LEN      (114)

/**
 ** Local Variables
 **/

static const wchar_t *booleanStrings[][2] = {
  { W("false"), W("true") },
  { W("f"), W("t") },
  { W("no"), W("yes") },
  { W("n"), W("y") },
  { W("off"), W("on") },
  { W("0"), W("1") }
};

/**
 ** Prototypes
 **/

static int getNodeContent(
  iconv_t conv,
  const xmlNode *node,
  const char *category,
  const char *param,
  wchar_t *out);

static int setNodeContent(
  iconv_t conv,
  xmlNode *node,
  const char *category,
  const char *param,
  wchar_t *in);

static int getNode(
  xmlDoc *doc,
  const xmlChar *category,
  const xmlChar *param,
  const xmlChar *type,
  int createMissing,
  xmlNode **node);

static int isParamType(
  const xmlNode *node,
  const xmlChar *type);

static xmlNode* findChild(
  const xmlNode *node,
  const xmlChar *name);

/* Return value must be freed */
static wchar_t* formatComment(
  iconv_t convTo,
  iconv_t convFrom,
  const wchar_t *comment);

/**
 ** Global Functions
 **/

/* Returns 1 if config file was created, 0 if config file was found, less than 0 otherwise */
int loadConfig(
  SA_Config_t *cfg)
{
  int rtnVal = SA_SUCCESS;

  if (!cfg || !(cfg->path)) {
    rtnVal = SA_ERROR_ARGS;
    goto error;
  }

  xmlKeepBlanksDefault(0);

  /* Open the config file */
  cfg->_fd = _wopen(cfg->path, O_RDWR);
  if (0 > cfg->_fd) {
    if (errno == ENOENT) {
      /* Config file was not found; create blank file */
      rtnVal = SA_CREATED_CONFIG;

      cfg->_fd = _wopen(cfg->path, O_RDWR | O_CREAT, DEFAULT_PERM);
      if (0 > cfg->_fd) {
        logMsg(LOG_ERROR, "Failed to create config file at \"%ls\": (%d) %ls", cfg->path, errno, _wcserror(errno));
        rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
        goto error;
      }

      /* Create new XML doc */
      cfg->_doc = xmlNewDoc((xmlChar*) CONFIG_FILE_XML_VERSION);
      if (!(cfg->_doc)) {
        xmlError *xmlError = xmlGetLastError();
        logMsg(LOG_ERROR, "Failed to create config XML: (%d) %s", xmlError->code, xmlError->message);
        rtnVal = genError(SA_GENERIC_ERROR_XML_CREATE, xmlError->code);
        goto error;
      }

      xmlNewTextChild((xmlNode*) cfg->_doc, NULL, (xmlChar*) cfg->name, NULL);
    }
    else {
      logMsg(LOG_ERROR, "Failed to open config file at \"%ls\": (%d) %ls", cfg->path, errno, _wcserror(errno));
      rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
      goto error;
    }
  }
  else {
    /* Parse existing XML doc */
    cfg->_doc = xmlReadFd(cfg->_fd, "", CONFIG_FILE_ENCODING, 0);
    if (!(cfg->_doc)) {
      xmlError *xmlError = xmlGetLastError();
      logMsg(LOG_ERROR, "Failed to read config XML at \"%ls\": (%d) %s", cfg->path, xmlError->code, xmlError->message);
      rtnVal = genError(SA_GENERIC_ERROR_XML_PARSE, xmlError->code);
      goto error;
    }
  }

  /* Create encoding descriptors */
  cfg->_wchar2xml = iconv_open(CONFIG_FILE_ENCODING, "WCHAR_T");
  if ((iconv_t) -1 == cfg->_wchar2xml) {
    rtnVal = genError(SA_GENERIC_ERROR_ENCODING, errno);
    cfg->_wchar2xml = 0;
    goto error;
  }

  cfg->_xml2wchar = iconv_open("WCHAR_T", CONFIG_FILE_ENCODING);
  if ((iconv_t) -1 == cfg->_xml2wchar) {
    rtnVal = genError(SA_GENERIC_ERROR_ENCODING, errno);
    cfg->_xml2wchar = 0;
    goto error;
  }

  cfg->_wchar2parse = iconv_open(COMMENT_PARSE_ENCODING, "WCHAR_T");
  if ((iconv_t) -1 == cfg->_wchar2parse) {
    rtnVal = genError(SA_GENERIC_ERROR_ENCODING, errno);
    cfg->_wchar2parse = 0;
    goto error;
  }

  cfg->_parse2wchar = iconv_open("WCHAR_T", COMMENT_PARSE_ENCODING);
  if ((iconv_t) -1 == cfg->_parse2wchar) {
    rtnVal = genError(SA_GENERIC_ERROR_ENCODING, errno);
    cfg->_parse2wchar = 0;
    goto error;
  }

  /* Successfully loaded config file */
  return rtnVal;

  error:
  closeConfig(cfg);
  return rtnVal;
}

/* Returns 2 if the config file was overwritten, 1 if config file was created,
 * 0 if config file was found, and less than 0 otherwise.
 */
int forceLoadConfig(
  SA_Config_t *cfg)
{
  int err;
  int rtnVal = SA_SUCCESS;

  rtnVal = err = loadConfig(cfg);
  if (SA_SUCCESS > err) {
    if (IS_GENERIC_ERROR(err) && (SA_GENERIC_ERROR_XML_PARSE == GENERIC_ERROR(err))) {
      err = _wremove(cfg->path);
      if (0 > err) {
        rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
        goto exit;
      }

      err = loadConfig(cfg);
      if (SA_SUCCESS > err) {
        rtnVal = err;
        goto exit;
      }

      rtnVal = SA_OVERWRITTEN_CONFIG;
    }
    else {
      rtnVal = err;
      goto exit;
    }
  }

  exit:
  return rtnVal;
}

/* Does not attempt to save the config */
void closeConfig(
  SA_Config_t *cfg)
{
  if (!cfg)
    return;

  if (0 <= cfg->_fd) {
    close(cfg->_fd);
    cfg->_fd = 0;
  }
  if (cfg->_doc) {
    xmlFreeDoc(cfg->_doc);
    cfg->_doc = NULL;
  }
  if (0 != cfg->_wchar2xml) {
    iconv_close(cfg->_wchar2xml);
    cfg->_wchar2xml = 0;
  }
  if (0 != cfg->_xml2wchar) {
    iconv_close(cfg->_xml2wchar);
    cfg->_xml2wchar = 0;
  }
  if (0 != cfg->_wchar2parse) {
    iconv_close(cfg->_wchar2parse);
    cfg->_wchar2parse = 0;
  }
  if (0 != cfg->_parse2wchar) {
    iconv_close(cfg->_parse2wchar);
    cfg->_parse2wchar = 0;
  }
}

int saveConfig(
  SA_Config_t *cfg)
{
  size_t err;
  int rtnVal = SA_SUCCESS;

  xmlSaveCtxt *saveCtxt = NULL;

  if (!cfg) {
    rtnVal = SA_ERROR_ARGS;
    goto exit;
  }
  if (!(cfg->_doc)) {
    rtnVal = SA_ERROR_OP_INVALID;
    goto exit;
  }

  /* Create saving context */
  close(cfg->_fd);
  cfg->_fd = _wopen(cfg->path, O_RDWR | O_TRUNC);
  if (0 > cfg->_fd) {
    logMsg(LOG_ERROR, "Failed to open config file at \"%ls\": (%d) %ls", cfg->path, errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
    goto exit;
  }

  err = lseek(cfg->_fd, 0, SEEK_SET);
  if (0 > err) {
    logMsg(LOG_ERROR, "Failed to seek to start of config file \"%ls\": (%d) %ls", cfg->path, errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
    goto exit;
  }

  saveCtxt = xmlSaveToFd(cfg->_fd, CONFIG_FILE_ENCODING, XML_SAVE_FORMAT | XML_SAVE_NO_EMPTY);
  if (!saveCtxt) {
    xmlError *xmlError = xmlGetLastError();
    logMsg(LOG_ERROR, "Failed to create config save context: (%d) %s", xmlError->code, xmlError->message);
    rtnVal = genError(SA_GENERIC_ERROR_XML_SAVE, xmlError->code);
    goto exit;
  }

  /* Save the document */
  err = xmlSaveDoc(saveCtxt, cfg->_doc);
  if (err < 0) {
    xmlError *xmlError = xmlGetLastError();
    logMsg(LOG_ERROR, "Failed to save config XML at \"%ls\": (%d) %s", cfg->path, xmlError->code, xmlError->message);
    rtnVal = genError(SA_GENERIC_ERROR_XML_SAVE, xmlError->code);
    goto exit;
  }

  xmlSaveFlush(saveCtxt);
  close(cfg->_fd);
  cfg->_fd = _wopen(cfg->path, O_RDWR);
  if (0 > cfg->_fd) {
    logMsg(LOG_ERROR, "Failed to create config file at \"%ls\": (%d) %ls", cfg->path, errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_FILE, errno);
    goto exit;
  }

  exit:
  if (saveCtxt)
    xmlSaveClose(saveCtxt);
  return rtnVal;
}

int configSetHeader(
  SA_Config_t *cfg,
  const char *category,
  const wchar_t *header)
{
  xmlChar buf[XML_STRING_BUFFER_LEN];
  xmlNode *categoryNode = NULL;
  int result;

  if (!cfg || !category || !header)
    return SA_ERROR_ARGS;

  /* Find the correct category node */
  result = getNode(cfg->_doc, (xmlChar*) category, NULL, NULL, SA_FALSE, &categoryNode);
  if (SA_SUCCESS != result)
    return result;

  /* Get the presentable row split-up */
  wchar_t *formattedComment = formatComment(cfg->_wchar2parse, cfg->_parse2wchar, header);
  if (NULL == formattedComment)
    return SA_ERROR_SYNTAX;

  /* Remove the previous comment, if any */
  xmlNode *commentNode = categoryNode->prev;
  while (commentNode && (XML_COMMENT_NODE == commentNode->type)) {
    xmlNode *tempNode = commentNode->prev;
    xmlUnlinkNode(commentNode);
    xmlFreeNode(commentNode);
    commentNode = tempNode;
  }

  /* Reset converter */
  iconv(cfg->_wchar2xml, NULL, NULL, NULL, NULL);

  /* Convert the input to UTF-8, as needed by libxml2 */
  size_t inLen = wcslen(formattedComment) * sizeof(wchar_t);
  size_t outLen = XML_STRING_BUFFER_LEN * sizeof(xmlChar);
  char *inPtr = (char*) formattedComment;
  char *outPtr = (char*) buf;
  size_t iconvResult = iconv(cfg->_wchar2xml, &inPtr, &inLen, &outPtr, &outLen);
  if (0 > iconvResult) {
    logMsg(LOG_ERROR, "Failed to encode comment for category \"%s\": (%d) %ls",
        category, errno, _wcserror(errno));
    return genError(SA_GENERIC_ERROR_ENCODING, errno);
  }

  /* Add the null term */
  *((wchar_t *) outPtr) = W('\0');

  /* Create the comment */
  commentNode = xmlNewComment(buf);
  xmlAddPrevSibling(categoryNode, commentNode);

  free(formattedComment);

  return SA_SUCCESS;
}

int configSetComment(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const wchar_t *comment)
{
  xmlChar buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !comment)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param, NULL, SA_FALSE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Get the presentable row split-up */
  wchar_t *formattedComment = formatComment(cfg->_wchar2parse, cfg->_parse2wchar, comment);
  if (NULL == formattedComment)
    return SA_ERROR_SYNTAX;

  /* Remove the previous comment, if any */
  xmlNode *commentNode = paramNode->prev;
  while (commentNode && (XML_COMMENT_NODE == commentNode->type)) {
    xmlNode *tempNode = commentNode->prev;
    xmlUnlinkNode(commentNode);
    xmlFreeNode(commentNode);
    commentNode = tempNode;
  }

  /* Reset converter */
  iconv(cfg->_wchar2xml, NULL, NULL, NULL, NULL);

  /* Convert the input to UTF-8, as needed by libxml2 */
  size_t inLen = wcslen(formattedComment) * sizeof(wchar_t);
  size_t outLen = XML_STRING_BUFFER_LEN * sizeof(xmlChar);
  char *inPtr = (char*) formattedComment;
  char *outPtr = (char*) buf;
  size_t iconvResult = iconv(cfg->_wchar2xml, &inPtr, &inLen, &outPtr, &outLen);
  if (0 > iconvResult) {
    logMsg(LOG_ERROR, "Failed to encode comment for parameter \"%s\" in category \"%s\": (%d) %ls",
        param, category, errno, _wcserror(errno));
    return genError(SA_GENERIC_ERROR_ENCODING, errno);
  }

  /* Add the null term */
  *((wchar_t *) outPtr) = W('\0');

  /* Create the comment */
  commentNode = xmlNewComment(buf);
  xmlAddPrevSibling(paramNode, commentNode);

  free(formattedComment);

  return SA_SUCCESS;
}

int configRemoveParam(
  SA_Config_t *cfg,
  const char *category,
  const char *param)
{
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param, NULL, SA_FALSE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Remove the comment, if any */
  xmlNode *commentNode = paramNode->prev;
  while (commentNode && (XML_COMMENT_NODE == commentNode->type)) {
    xmlNode *tempNode = commentNode->prev;
    xmlUnlinkNode(commentNode);
    xmlFreeNode(commentNode);
    commentNode = tempNode;
  }

  /* Remove the whitespace, if any */
  xmlNode *wsNode = paramNode->prev;
  while (wsNode && (XML_TEXT_NODE == wsNode->type)) {
    xmlNode *tempNode = wsNode->prev;
    xmlUnlinkNode(wsNode);
    xmlFreeNode(wsNode);
    wsNode = tempNode;
  }

  /* Remove the node */
  xmlUnlinkNode(paramNode);
  xmlFreeNode(paramNode);

  return SA_SUCCESS;
}

int configGetInt(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int64_t *out)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_INTEGER, SA_FALSE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Check to see if the buffer has any content */
  if (W('\0') == buf[0])
    return SA_ERROR_NOT_FOUND;

  /* Parse the content in the buffer */
  wchar_t *tailPtr = &(buf[0]);
  int64_t val = wcstoll(buf, &tailPtr, 0);

  /* Check for syntactical validity */
  if (tailPtr == &(buf[0])) {
    logMsg(LOG_WARNING, "Invalid syntax for parameter \"%s\" in category \"%s\"", param, category);
    return SA_ERROR_SYNTAX;
  }

  /* Finally return the retrieved value */
  *out = val;
  return SA_SUCCESS;
}

int configOptInt(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int64_t *out,
  const int64_t in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_INTEGER, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Go to the setVal routine if the buffer is empty */
  if (W('\0') == buf[0]) {
    goto setVal;
  }
  else {
    /* The buffer has content; parse it */
    wchar_t *tailPtr = &(buf[0]);
    int64_t val = wcstoll(buf, &tailPtr, 0);

    /* Check for syntactical validity */
    if (tailPtr == &(buf[0])) {
      logMsg(LOG_WARNING, "Invalid syntax for parameter \"%s\" in category \"%s\"; resetting value", param, category);
      goto setVal;
    }

    /* Finally return the retrieved value */
    *out = val;
    return SA_SUCCESS;
  }

  setVal:
  /* The buffer has no content or had invalid syntax; fill it in with the requested value */
  snwprintf(buf, XML_STRING_BUFFER_LEN, W("%lld"), in);
  buf[XML_STRING_BUFFER_LEN - 1] = W('\0');
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Lastly, set the output equal to the input we just assigned */
  *out = in;
  return SA_SUCCESS;
}

int configSetInt(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const int64_t in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_INTEGER, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Fill in the buffer with the requested value */
  snwprintf(buf, XML_STRING_BUFFER_LEN, W("%lld"), in);
  buf[XML_STRING_BUFFER_LEN - 1] = W('\0');
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  return SA_SUCCESS;
}

int configGetFloat(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  double *out)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_FLOAT, SA_FALSE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Check to see if the buffer has any content */
  if (W('\0') == buf[0])
    return SA_ERROR_NOT_FOUND;

  /* Parse the content in the buffer */
  wchar_t *tailPtr = &(buf[0]);
  double val = wcstod(buf, &tailPtr);

  /* Check for syntactical validity */
  if (tailPtr == &(buf[0])) {
    logMsg(LOG_WARNING, "Invalid syntax for parameter \"%s\" in category \"%s\"", param, category);
    return SA_ERROR_SYNTAX;
  }

  /* Finally return the retrieved value */
  *out = val;
  return SA_SUCCESS;
}

int configOptFloat(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  double *out,
  const double in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_FLOAT, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Go to the setVal routine if the buffer is empty */
  if (W('\0') == buf[0]) {
    goto setVal;
  }
  else {
    /* The buffer has content; parse it */
    wchar_t *tailPtr = &(buf[0]);
    double val = wcstod(buf, &tailPtr);

    /* Check for syntactical validity */
    if (tailPtr == &(buf[0])) {
      logMsg(LOG_WARNING, "Invalid syntax for parameter \"%s\" in category \"%s\"; resetting value", param, category);
      goto setVal;
    }

    /* Finally return the retrieved value */
    *out = val;
    return SA_SUCCESS;
  }

  setVal:
  /* The buffer has no content or had invalid syntax; fill it in with the requested value */
  snwprintf(buf, XML_STRING_BUFFER_LEN, W("%.6g"), in);
  buf[XML_STRING_BUFFER_LEN - 1] = W('\0');
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Lastly, set the output equal to the input we just assigned */
  *out = in;
  return SA_SUCCESS;
}

int configSetFloat(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const double in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_FLOAT, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Fill in the buffer with the requested value */
  snwprintf(buf, XML_STRING_BUFFER_LEN, W("%.6g"), in);
  buf[XML_STRING_BUFFER_LEN - 1] = W('\0');
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  return SA_SUCCESS;
}

int configGetBool(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int *out)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_BOOLEAN, SA_FALSE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Check to see if the buffer has any content */
  if (W('\0') == buf[0])
    return SA_ERROR_NOT_FOUND;

  /* Parse the content in the buffer */
  for (long i = 0; i < ARRAY_LEN(booleanStrings); i++) {
    if (0 == wcsicmp(buf, booleanStrings[i][SA_FALSE])) {
      *out = SA_FALSE;
      return SA_SUCCESS;
    }
    else if (0 == wcsicmp(buf, booleanStrings[i][SA_TRUE])) {
      *out = SA_TRUE;
      return SA_SUCCESS;
    }
  }

  /* Reaching here means the syntax was invalid */
  logMsg(LOG_WARNING, "Invalid syntax for parameter \"%s\" in category \"%s\"", param, category);
  return SA_ERROR_SYNTAX;
}

int configOptBool(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  int *out,
  const int in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  if ((SA_FALSE != in) && (SA_TRUE != in))
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_BOOLEAN, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Go to the setVal routine if the buffer is empty */
  if (W('\0') == buf[0]) {
    goto setVal;
  }
  else {
    /* The buffer has content; parse it */
    for (long i = 0; i < ARRAY_LEN(booleanStrings); i++) {
      if (0 == wcsicmp(buf, booleanStrings[i][SA_FALSE])) {
        *out = SA_FALSE;
        return SA_SUCCESS;
      }
      else if (0 == wcsicmp(buf, booleanStrings[i][SA_TRUE])) {
        *out = SA_TRUE;
        return SA_SUCCESS;
      }
    }

    /* Reaching here means the syntax was invalid */
    logMsg(LOG_WARNING, "Invalid syntax for parameter \"%s\" in category \"%s\"; resetting value", param, category);
  }

  setVal:
  /* The buffer has no content or had invalid syntax; fill it in with the requested value */
  wcscpy(buf, booleanStrings[0][in]);
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Lastly, set the output equal to the input we just assigned */
  *out = in;
  return SA_SUCCESS;
}

int configSetBool(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const int in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param)
    return SA_ERROR_ARGS;

  if ((SA_FALSE != in) && (SA_TRUE != in))
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_BOOLEAN, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Fill in the buffer with the requested value */
  wcscpy(buf, booleanStrings[0][in]);
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  return SA_SUCCESS;
}

/* Out should be freed when no longer needed */
int configGetString(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  wchar_t **out)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_STRING, SA_FALSE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Return the buffer */
  *out = wcsdup(buf);
  return SA_SUCCESS;
}

/* Out should be freed when no longer needed */
int configOptString(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  wchar_t **out,
  const wchar_t *in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !out || !in)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_STRING, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Retrieve the node content */
  result = getNodeContent(cfg->_xml2wchar, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Go to the setVal routine if the buffer is empty */
  if (W('\0') == buf[0]) {
    goto setVal;
  }
  else {
    /* Return the buffer */
    *out = wcsdup(buf);
    return SA_SUCCESS;
  }

  setVal:
  /* The buffer has no content; fill it in with the requested string */
  wcscpy(buf, in);
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  /* Lastly, set the output equal to the input we just assigned */
  *out = wcsdup(buf);
  return SA_SUCCESS;
}

int configSetString(
  SA_Config_t *cfg,
  const char *category,
  const char *param,
  const wchar_t *in)
{
  wchar_t buf[XML_STRING_BUFFER_LEN];
  xmlNode *paramNode = NULL;
  int result;

  if (!cfg || !category || !param || !in)
    return SA_ERROR_ARGS;

  /* Find the correct parameter node */
  result = getNode(cfg->_doc, (xmlChar*) category, (xmlChar*) param,
      (xmlChar*) PARAMETER_TYPE_STRING, SA_TRUE, &paramNode);
  if (SA_SUCCESS != result)
    return result;

  /* Fill in the buffer with the requested string */
  wcscpy(buf, in);
  result = setNodeContent(cfg->_wchar2xml, paramNode, category, param, buf);
  if (SA_SUCCESS != result)
    return result;

  return SA_SUCCESS;
}

/**
 ** Local Functions
 **/

static int getNodeContent(
  iconv_t conv,
  const xmlNode *node,
  const char *category,
  const char *param,
  wchar_t *out)
{
  xmlChar *content = NULL;
  int rtnVal = SA_SUCCESS;

  if (!node || !category || !param || !out) {
    rtnVal = SA_ERROR_ARGS;
    goto exit;
  }

  /* Get the node's content string, if it exists */
  content = xmlNodeGetContent(node);

  size_t inLen;
  if (content && (0 != (inLen = xmlUTF8Strsize(content, xmlUTF8Strlen(content))))) {
    /* Reset converter */
    iconv(conv, NULL, NULL, NULL, NULL);

    /* Convert the content to something more workable, if possible */
    size_t outLen = XML_STRING_BUFFER_LEN * sizeof(wchar_t);
    char *inPtr = (char*) content;
    char *outPtr = (char*) out;
    size_t result = iconv(conv, &inPtr, &inLen, &outPtr, &outLen);
    if (0 > result) {
      logMsg(LOG_ERROR, "Failed to decode parameter \"%s\" in category \"%s\": (%d) %ls",
          param, category, errno, _wcserror(errno));
      rtnVal = genError(SA_GENERIC_ERROR_ENCODING, errno);
      goto exit;
    }

    /* Add the null term */
    *((wchar_t*) outPtr) = W('\0');
  }
  else {
    /* There is no content to speak of, so output an empty string */
    out[0] = W('\0');
  }

  exit:
  if (content)
    xmlFree(content);
  return rtnVal;
}

static int setNodeContent(
  iconv_t conv,
  xmlNode *node,
  const char *category,
  const char *param,
  wchar_t *in)
{
  xmlChar content[XML_STRING_BUFFER_LEN];
  int rtnVal = SA_SUCCESS;

  if (!node || !category || !param || !in) {
    rtnVal = SA_ERROR_ARGS;
    goto exit;
  }

  /* Reset converter */
  iconv(conv, NULL, NULL, NULL, NULL);

  /* Convert the input to UTF-8, as needed by libxml2 */
  size_t inLen = wcslen(in) * sizeof(wchar_t);
  size_t outLen = XML_STRING_BUFFER_LEN * sizeof(xmlChar);
  char *inPtr = (char*) in;
  char *outPtr = (char*) content;
  size_t result = iconv(conv, &inPtr, &inLen, &outPtr, &outLen);
  if (0 > result) {
    logMsg(LOG_ERROR, "Failed to encode parameter \"%s\" in category \"%s\": (%d) %ls",
        param, category, errno, _wcserror(errno));
    rtnVal = genError(SA_GENERIC_ERROR_ENCODING, errno);
    goto exit;
  }

  /* Add the null term */
  *((wchar_t*) outPtr) = W('\0');

  /* Set the node's content */
  xmlNodeSetContent(node, content);

  exit:
  return rtnVal;
}

static int getNode(
  xmlDoc *doc,
  const xmlChar *category,
  const xmlChar *param,
  const xmlChar *type,
  int createMissing,
  xmlNode **node)
{
  if (!doc || !category || !node)
    return SA_ERROR_ARGS;

  /* Find the category */
  xmlNode *categoryNode = findChild(doc->children, category);
  if (!categoryNode) {
    if (createMissing)
      categoryNode = xmlNewTextChild(doc->children, NULL, category, NULL);
    else
      return SA_ERROR_NOT_FOUND;
  }

  if (param) {
    /* Find the parameter within that category */
    xmlNode *paramNode = findChild(categoryNode, param);
    if (!paramNode) {
      if (createMissing) {
        paramNode = xmlNewTextChild(categoryNode, NULL, param, NULL);
        if (type)
          xmlSetProp(paramNode, (xmlChar*) PARAMETER_TYPE, type);
      }
      else {
        return SA_ERROR_NOT_FOUND;
      }
    }

    /* Check and coerce type, if necessary */
    if (type) {
      if (!xmlHasProp(paramNode, (xmlChar*) PARAMETER_TYPE)) {
        xmlSetProp(paramNode, (xmlChar*) PARAMETER_TYPE, type);
      }
      else if (!isParamType(paramNode, type)) {
        return SA_ERROR_TYPE_MISMATCH;
      }
    }

    *node = paramNode;
    return SA_SUCCESS;
  }
  else {
    /* Just return the category */
    *node = categoryNode;
    return SA_SUCCESS;
  }
}

static int isParamType(
  const xmlNode *node,
  const xmlChar *type)
{
  if (!node || !type)
    return SA_FALSE;

  xmlChar *prop = xmlGetProp(node, (xmlChar*) PARAMETER_TYPE);
  if (prop) {
    int rtnVal;

    if (0 == xmlStrcasecmp(prop, type))
      rtnVal = SA_TRUE;
    else
      rtnVal = SA_FALSE;

    xmlFree(prop);
    return rtnVal;
  }

  return SA_FALSE;
}

static xmlNode* findChild(
  const xmlNode *node,
  const xmlChar *name)
{
  if (!node)
    return NULL;

  xmlNode *child = node->children;

  /* Iterate through all the children to find a match */
  while (child) {
    if (0 == xmlStrcasecmp(child->name, name))
      return child;
    child = child->next;
  }

  /* No match found */
  return NULL;
}

/* Return value must be freed */
static wchar_t* formatComment(
  iconv_t convTo,
  iconv_t convFrom,
  const wchar_t *comment)
{
  union utf32 {
    unsigned int code;
    char buf[4];
  };

  const union utf32 newlines[] = {
    { 0x0000000A }, //* \n *//
    { 0x0000000D }  //* \r *//
  };

  const union utf32 whitespace[] = {
    { 0x00000020 }
  };

  union utf32 inBuf[COMMENT_BUFFER_LEN];
  wchar_t outBuf[COMMENT_BUFFER_LEN];
  wchar_t tmpBuf[COMMENT_BUFFER_LEN];

  /* Reset converter */
  iconv(convTo, NULL, NULL, NULL, NULL);

  /* Convert from wchar_t to UTF-32, so we can freely manipulate each row */
  size_t inLen = wcslen(comment) * sizeof(wchar_t);
  size_t outLen = COMMENT_BUFFER_LEN * sizeof(union utf32);
  char *inPtr = (char*) comment;
  char *outPtr = (char*) inBuf;
  size_t result = iconv(convTo, &inPtr, &inLen, &outPtr, &outLen);
  if (0 > result) {
    logMsg(LOG_WARNING, "Failed to convert comment to usable array: (%d) %ls", errno, _wcserror(errno));
    return NULL;
  }

  /* Split all the lines */
  size_t commentLen = ((COMMENT_BUFFER_LEN * sizeof(union utf32)) - outLen) / sizeof(union utf32);
  size_t lineCount = 1;
  union utf32 **lineArray = a_calloc(lineCount, sizeof(union utf32*));
  size_t *lineLength = a_calloc(lineCount, sizeof(size_t));

  for (int i = 0, j = 1, start = 0; i < commentLen; i++, j++) {
    int isNewLine = SA_FALSE;
    for (size_t k = 0; k < ARRAY_LEN(newlines); k++) {
      if (newlines[k].code == inBuf[i].code)
        isNewLine = SA_TRUE;
    }

    if (isNewLine ||
        (j > COMMENT_LINE_LEN) ||
        (i == (commentLen - 1)))
    {
      if (!isNewLine && (i < (commentLen - 1))) {
        /* Work backwards to a word boundary */
        while (j > 0) {
          for (size_t k = 0; k < ARRAY_LEN(whitespace); k++) {
            if (whitespace[k].code == inBuf[i].code)
              goto doneWalkback;
          }
          j--;
          i--;
        }
      }
      doneWalkback: ;

      /* Strip whitespace */
      int idx = i;
      while (j > 0) {
        for (size_t k = 0; k < ARRAY_LEN(whitespace); k++) {
          if (whitespace[k].code != inBuf[idx].code)
            goto doneStrip;
        }
        j--;
        idx--;
      }
      doneStrip: ;

      if (isNewLine)
        j--;

      lineArray[lineCount - 1] = &(inBuf[start]);
      lineLength[lineCount - 1] = j;

      if (i == (commentLen - 1))
        break;

      lineCount++;
      lineArray = a_realloc(lineArray, lineCount * sizeof(union utf32*));
      lineLength = a_realloc(lineLength, lineCount * sizeof(size_t));
      lineArray[lineCount - 1] = NULL;
      lineLength[lineCount - 1] = 0;
      j = 0;
      start = i + 1;
    }
  }

  /* Convert lines back to wide strings */
  wchar_t *rtnBuf = a_calloc(COMMENT_BUFFER_LEN, sizeof(wchar_t*));
  _snwprintf(rtnBuf, COMMENT_BUFFER_LEN, W("\n    "));

  for (size_t i = 0; i < lineCount; i++) {
    /* Reset converter */
    iconv(convFrom, NULL, NULL, NULL, NULL);

    /* Convert from UTF-32 to wchar_t, for output */
    inLen = lineLength[i] * sizeof(union utf32);
    outLen = COMMENT_BUFFER_LEN * sizeof(wchar_t);
    inPtr = (char*) lineArray[i];
    outPtr = (char*) outBuf;
    result = iconv(convFrom, &inPtr, &inLen, &outPtr, &outLen);
    if (0 > result) {
      logMsg(LOG_WARNING, "Failed to convert array to comment line: (%d) %ls", errno, _wcserror(errno));
      free(lineArray);
      free(lineLength);
      return NULL;
    }

    /* Add the null term */
    *((wchar_t*) outPtr) = W('\0');

    /* Put converted string into return array */
    wcscpy(tmpBuf, rtnBuf);
    _snwprintf(rtnBuf, COMMENT_BUFFER_LEN, W("%ls %ls\n    "), tmpBuf, outBuf);
  }

  /* Free the memory we don't need anymore */
  free(lineArray);
  free(lineLength);

  /* Return */
  return rtnBuf;
}
