/**********************************************************************************************************************
 * mem.h
 * Copyright (C) 2016 Magehand LLC
 *********************************************************************************************************************/

#ifndef ___MEM_H___
#define ___MEM_H___

#include <stdatomic.h>

/**
 ** Macros
 **/

#define LOG_POOL_MEM(bytes)     ({ _pool_mem     += bytes; })
#define LOG_IMAGE_MEM(bytes)    ({ _image_mem    += bytes; })
#define LOG_TEXTURE_VMEM(bytes) ({ _texture_vmem += bytes; })

#define POOL_MEM     ({ _pool_mem;     })
#define IMAGE_MEM    ({ _image_mem;    })
#define TEXTURE_VMEM ({ _texture_vmem; })

/**
 ** Global Variables
 **/

extern atomic_ullong _pool_mem;
extern atomic_ullong _image_mem;
extern atomic_ullong _texture_vmem;

#endif /* ___MEM_H___ */
